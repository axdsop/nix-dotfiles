# AxdSop *nix Dotfiles For i3

Personal *inux application configurations, aka [dotfiles](https://wiki.archlinux.org/index.php/Dotfiles) for window manager [i3][i3].

To make [i3][i3] desktop environment works more powerfully. I choose these utilities.

## TOC

1. [Tool Stack](#tool-stack)  
2. [Deployment](#deployment)  
3. [Custom Script](#custom-script)  
4. [Change Log](#change-log)  


## Tool Stack

Every application has a *README.md* file which contains its simple introductions, deployment procedure.

Essential applications

Item|Type|Wiki|Note
---|---|---|---
[i3][i3]|tiling window manager|[User's Guide](https://i3wm.org/docs/userguide.html)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/I3)<br/>[Gentoo](https://wiki.gentoo.org/wiki/I3)|[note](/Configs/i3)
[picom][picom]|compositor|[Wiki](https://github.com/yshui/picom/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Picom)|[note](/Configs/picom)
[Polybar][polybar]|status bar|[Wiki](https://github.com/polybar/polybar/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Polybar)|[note](/Configs/polybar)
[Rofi][rofi]|application launcher|[Wiki](https://github.com/davatorium/rofi/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Rofi)|[note](/Configs/rofi)
[Dunst][dunst]|notification daemon|[Wiki](https://github.com/dunst-project/dunst/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Dunst)|[note](/Configs/dunst)
[Ranger][ranger]|text-based file manager|[Wiki](https://github.com/ranger/ranger/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Ranger)<br/>[Gentoo](https://wiki.gentoo.org/wiki/Ranger)|[note](/Configs/ranger)
[Alacritty][alacritty]|terminal emulator|[Wiki](https://github.com/alacritty/alacritty/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Alacritty)<br/>[Gentoo](https://wiki.gentoo.org/wiki/Alacritty)|[note](/Configs/alacritty)
[tmux][tmux]|terminal multiplexer|[Wiki](https://github.com/tmux/tmux/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Tmux)<br/>[Gentoo](https://wiki.gentoo.org/wiki/Tmux)|[note](/Configs/tmux)
[Vim][vim]/[Neovim][neovim]|terminal text editor|[vim](https://www.vim.org)<br/>[neovim](https://neovim.io)|[note](/Configs/vim)


Optional applications

Item|Type|Wiki|Note
---|---|---|---
[Breeze GTK](https://github.com/KDE/breeze-gtk)|GTK Theme||[note](/Configs/Components.md)
[Breeze Curosr](https://github.com/KDE/breeze/tree/master/cursors)|cursor theme||[note](/Configs/Components.md)
[Papirus][papirus]|icon theme|[Wiki](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)|[note](/Configs/Components.md)
[MuPDF][mupdf]|PDF viewer|[Wiki](https://mupdf.com/docs/index.html)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/MuPDF)<br/>[Gentoo](https://wiki.gentoo.org/wiki/MuPDF)|
[SCReenshOT][scrot]|screen capturer|[Arch Linux](https://wiki.archlinux.org/index.php/Screen_capture#scrot)|
[feh][feh]|image viewer|[Wiki](https://github.com/derf/feh/wiki)<br/>[Arch Linux](https://wiki.archlinux.org/index.php/Feh)<br/>[Gentoo](https://wiki.gentoo.org/wiki/Feh)|


## Deployment

Deployment seperates into two parts: *Packages* deployment and *Configs* deployment.

Here I use script [deploy.sh](./deploy.sh) as a deploy tool. Configs save path is `~/.config/axdsop/Configs`.

```bash
# - just configs
bash deploy.sh
# bash deploy.sh -a # normal mode (vim without plugins, tmux not config)

# - packages + configs
bash deploy.sh -i
```

If you wanna use my custom bashrc, please use script [axdsop_boost_tool.sh][boost_tool].

```bash
# method 1
bash axdsop_boost_tool.sh -i

# method 2
curl -fsL https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/axdsop_boost_tool.sh | bash -s -- -i

# method 3
curl -fsL https://axdlog.com/script/boost.sh | bash -s -- -i
```


## Custom Script

I write some Shell scripts to power [i3][i3] desktop environment.

* [Scripts](./Scripts)
  * [usb-automount](./Scripts/usb-automount)
  * [miscellaneous script collections](./Scripts/miscellaneous)
* [Polybar/scripts](./Configs/polybar/scripts)
  * [mpris_player](./Configs/polybar/scripts/mpris_player)
* [Axdsop/scripts](./Configs/axdsop)
  * [axdsop_boost_tool.sh][boost_tool]


## Bibliography

* [GitHub does dotfiles](https://dotfiles.github.io/)


## Change Log

* Apr 27, 2020 Sun 09:45 ET
  * first draft
* Oct 08, 2020 Thu 14:12 ET
  * Add configs deploy script


[i3]:https://i3wm.org/ "i3 - improved tiling wm"
[i3-gaps]:https://github.com/Airblader/i3
[polybar]:https://polybar.github.io/ "Polybar - A fast and easy-to-use tool for creating status bars"
[rofi]: https://github.com/davatorium/rofi "Rofi: A window switcher, application launcher and dmenu replacement"
[dunst]:https://dunst-project.org/ "Dunst is a lightweight replacement for the notification daemons provided by most desktop environments."
[alacritty]:https://github.com/alacritty/alacritty "A cross-platform, GPU-accelerated terminal emulator"
[ranger]:https://ranger.github.io/ "Ranger is a console file manager with VI key bindings."
[picom]:https://github.com/yshui/picom "A lightweight compositor for X11"
[tmux]:https://github.com/tmux/tmux "tmux is a terminal multiplexer"
[vim]:https://www.vim.org "Vim - the ubiquitous text editor"
[neovim]:https://neovim.io "hyperextensible Vim-based text editor"
[mupdf]:https://mupdf.com "MuPDF is a lightweight PDF, XPS, and E-book viewer."
[scrot]:https://github.com/resurrecting-open-source-projects/scrot "SCReenshOT - command line screen capture utility"
[feh]:https://feh.finalrewind.org "feh – a fast and light image viewer"
[papirus]:https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
[boost_tool]:./Configs/axdsop/axdsop_boost_tool.sh

<!-- End -->