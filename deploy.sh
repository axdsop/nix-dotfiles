#!/usr/bin/env bash
# set -u  #Detect undefined variable
# set -o pipefail #Return return code in pipeline fails

# Project https://gitlab.com/axdsop/nix-dotfiles
# Target: Deploying i3 stack packages and configs on *nix system.
# Developer: MaxdSre

# Change Log:
# - Nov 23, 2020 14:58 Mon ET - fix vim/nvim plugin intall make the left while loop item not execute any more
# - Nov 19, 2020 19:26 Thu ET - add file manager, archive manager, add login user into group 'video'
# - Oct 30, 2020 13:45 Fri ET - add i3 stack packages deployment, vim plug manager install, xinitrc&Xresources deploy if i3 exists
# - Oct 24, 2020 10:25 Sat ET - optimize soft link operation with normal action
# - Oct 17, 2020 08:23 Sat ET - add gtk-* packages if existed check
# - Oct 12, 2020 10:28 Mon ET - add nvim config init.vim deployment
# - Oct 08, 2020 10:15 Thu ET - first draft


#########  0-1. Variables Setting  #########
is_normal_action=${is_normal_action:-0}
is_install_packs=${is_install_packs:-0}


#########  0-2 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Project https://gitlab.com/axdsop/nix-dotfiles

Deploying i3 stack packages and configs on *nix system.

[available option]
    -a    --specify action mode to 'normal' (e.g. vim just use general configuration, tmux doesn't config), default is 'professional'
    -i    --install i3 stack packages (just support Ubuntu/Arch Linux family)
    -h    --help, show help info
\e[0m"
}

while getopts "hai" option "$@"; do
    case "$option" in
        # m ) target_mount_device="$OPTARG" ;;
        a ) is_normal_action=1 ;;
        i ) is_install_packs=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-1 Preparation Operation  #########
fn_CommandCheck(){ local l_name="${1:-}"; local l_output=${l_output:-1}; [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0; return "${l_output}"; }  # $? -- 0 is find, 1 is not find


fn_Variables_Initialization(){
    # download method detection
    download_method=${download_method:-'wget -qO-'}
    fn_CommandCheck 'curl' && download_method='curl -fsL'

    # $EUID is 0 if run as root, default is user id, use command `who` to detect login user name
    # $USER exist && $SUDO_USER not exist, then use $USER
    [[ -n "${USER:-}" && -z "${SUDO_USER:-}" ]] && login_user="${USER:-}" || login_user="${SUDO_USER:-}"
    [[ -z "${login_user}" ]] && login_user=$(logname 2> /dev/null)
    [[ -z "${login_user}" ]] && login_user=$(id -u -n 2> /dev/null)
    # [[ -z "${login_user}" ]] && login_user=$(ps -eo 'uname,comm,cmd' | sed -r -n '/xinit[[:space:]]+\/(root|home)\//{s@^([^[:space:]]+).*@\1@g;p;q}')

    [[ "${login_user}" == 'root' ]] && login_user_home='/root' || login_user_home="/home/${login_user}"
    [[ -d "${login_user_home}" ]] || login_user_home=$(sed -r -n '/^'"${login_user}"':/{p}' /etc/passwd | cut -d: -f6)

    user_config_dir=${user_config_dir:-"${login_user_home}/.config"} # XDG_CONFIG_HOME
    user_codebase_dir=${user_codebase_dir:-"${user_config_dir}/axdsop"}
}


#########  2-1 Packages Deployment  #########
fn_GTK_Components_Deployment(){
    local l_item=${l_item:-"$1"}

    case "${l_item,,}" in
        i|icon)
            # Icon Theme - Papirus
            # https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
            echo -e -n 'Icon Theme (Papirus) deploying ... '

            project_name='PapirusDevelopmentTeam/papirus-icon-theme'
            tag_name=$(${download_method} "https://api.github.com/repos/${project_name}/releases/latest" | sed -r -n '/tag_name/{s@[^:]*:[[:space:]]*"([^"]+).*@\1@g;p}')
            # https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/archive/20201001.tar.gz
            pack_download_link="https://github.com/${project_name}/archive/${tag_name}.tar.gz"

            save_dir=$(mktemp -d -t)
            pack_save_path="${save_dir}/${pack_download_link##*/}"
            # /usr/share/icons/
            ${download_method} "${pack_download_link}" > "${pack_save_path}"

            tar xf "${pack_save_path}" -C "${save_dir}"  --strip-components=1
            [[ -d /usr/share/icons/ ]] || sudo mkdir -p /usr/share/icons/
            sudo cp -R "${save_dir}"/Papirus* /usr/share/icons/
            # Papirus / Papirus-Dark / Papirus-Light
            [[ -d "${save_dir}" ]] && rm -rf "${save_dir}"
            echo -e -n '[ok]\n'
        ;;
        c|curosr)
            # Cursor theme - Breeze from KDE
            # https://github.com/KDE/breeze
            # https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=xcursor-breeze
            echo -e -n 'Cursor theme (Breeze from KDE) deploying ... '

            site_url='https://download.kde.org/stable/plasma/'
            latest_release_version=$(${download_method} "${site_url}" | sed -r -n '/<td>.*href=/{s@.*href="([^"]+)".*@\1@;p}' |  sed '$!d;s@\/$@@g')  # 5.20.1
            pack_download_link="${site_url}/${latest_release_version}/breeze-${latest_release_version}.tar.xz"
            # https://download.kde.org/stable/plasma/5.20.1/breeze-5.20.1.tar.xz

            save_dir=$(mktemp -d -t)
            pack_save_path="${save_dir}/${pack_download_link##*/}"
            # /usr/share/icons/
            ${download_method} "${pack_download_link}" > "${pack_save_path}"

            tar xf "${pack_save_path}" -C "${save_dir}"  --strip-components=1
            [[ -d /usr/share/icons/ ]] || sudo mkdir -p /usr/share/icons/
            sudo cp -R "${save_dir}"/cursors/Breeze/Breeze/ /usr/share/icons/
            # sudo cp -R "${save_dir}"/cursors/Breeze_Snow/Breeze_Snow/ /usr/share/icons/
            [[ -d "${save_dir}" ]] && rm -rf "${save_dir}"
            echo -e -n '[ok]\n'
        ;;
        f|font)
            # Font - Nerd Font
            # https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/SourceCodePro
            echo -e -n 'Font (SauceCodePro Nerd Font) deploying ... '

            # - SauceCodePro Nerd Font 28.3 MB
            # https://aur.archlinux.org/packages/nerd-fonts-source-code-pro/
            # https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=nerd-fonts-source-code-pro

            download_link=$(${download_method} https://api.github.com/repos/ryanoasis/nerd-fonts/releases/latest | sed -r -n '/browser_download_url.*SourceCodePro/{s@[^:]*:[[:space:]]*"([^"]+).*@\1@g;p}')
            # https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/SourceCodePro.zip

            save_dir=$(mktemp -d -t)
            save_path="${save_dir}/${download_link##*/}"
            ${download_method} "${download_link}" > "${save_path}"
            [[ -d /usr/share/fonts/TTF ]] || sudo mkdir -p /usr/share/fonts/TTF

            #  exclude windows compatible fonts
            # find $PWD -type f -name '*.ttf' ! -iname '*windows*'
            sudo unzip -oq "${save_path}" -x *Windows* -d /usr/share/fonts/TTF
            [[ -d "${save_dir}" ]] && rm -rf "${save_dir}"

            # build font cache
            fc-cache -f # -v
            echo -e -n '[ok]\n'
        ;;
    esac
}

fn_Ubuntu_Packages(){
    # - i3 (Ubuntu repository has no i3-gaps)
    # sudo apt purge -y i3-wm
    sudo apt purge -y i3status

    sudo apt install -y i3 i3lock
    # - i3 essential packages
    sudo apt install -y light pulseaudio pulseaudio-utils
    # - i3 optional packages
    sudo apt install -y scrot feh mupdf

    # - polybar
    sudo apt install -y polybar
    # - polybar optional packages
    sudo apt install -y libnotify-bin
    # need SauceCodePro Nerd Font

    # - rofi
    sudo apt install -y rofi
    # need SauceCodePro Nerd Font
    # need Papirus icon theme

    # - ranger
    sudo apt install -y ranger
    # - ranger optional packages
    sudo apt install -y w3m atool mediainfo libimage-exiftool-perl mupdf
    # sudo apt install -y ffmpegthumbnailer
    [[ -f /usr/lib/w3m/w3mimgdisplay ]] && sudo ln -fs /usr/lib/w3m/w3mimgdisplay /usr/local/bin
    # need SauceCodePro Nerd Font

    # - dunst
    sudo apt install -y dunst
    # need Papirus icon theme

    # - picom (optional)
    sudo apt install -y picom
    sudo pacman -S --noconfirm picom

    # - alacritty
    sudo apt install -y alacritty

    # - tmux
    sudo apt install -y tmux

    # - vim
    # sudo apt install -y vim
    sudo apt install -y neovim

    # notify-send
    sudo apt install -y libnotify-bin


    # - GTK Theme (Breeze)
    sudo apt install -y breeze-gtk-theme

    # - Icon Theme (Papirus)
    # sudo apt install -y papirus-icon-theme # not the latest release version
    # manual install

    # - Cursor Theme (Breeze)
    # manual install

    # - Font (SauceCodePro Nerd Font)
    # manual install

    # auto remove
    sudo apt purge --auto-remove -y
}

fn_ArchLinux_Packages(){
    # - i3 (Ubuntu repository has no i3-gaps)
    # sudo pacman -R --noconfirm i3-wm 
    sudo pacman -R --noconfirm i3status dmenu 2> /dev/null
    sudo pacman -S --noconfirm i3-gaps i3lock
    # - i3 essential packages
    sudo pacman -S --noconfirm light pulseaudio pulseaudio-alsa
    # - i3 optional packages
    sudo pacman -S --noconfirm scrot feh mupdf
    # - i3 optional packages file manager, archive manager
    # https://gitlab.com/axdsop/nixnotes/blob/master/GNULinux/Distros/ArchLinux/Notes/FileManagerComparison.md
    sudo pacman -S --noconfirm thunar
    sudo pacman -S --noconfirm tumbler libgsf libopenraw poppler-glib
    sudo pacman -S --noconfirm thunar-archive-plugin xarchiver  # xarchiver is archive manager
    # The Media Transfer Protocol (MTP) can be used to transfer media files to and from many mobile phones.  # https://wiki.archlinux.org/index.php/Thunar#Thunar_Volume_Manager
    # sudo pacman -S --noconfirm gvfs-mtp  # install 20+1 packages, download size 7.21MiB, installed size: 39.92MiB

    applications_dir=${applications_dir:-'/usr/share/applications'}
    sudo find "${applications_dir}" -type f -name 'exo-*.desktop' -exec mv {}{,.hide} \;
    sudo mv "${applications_dir}"/thunar-settings.desktop{,.hide} 2> /dev/null
    sudo mv "${applications_dir}"/xfce4-about.desktop{,.hide} 2> /dev/null

    # https://wiki.archlinux.org/index.php/Xdg-utils
    # https://bbs.archlinux.org/viewtopic.php?id=157033
    # xdg-mime query default inode/directory
    xdg-mime default thunar.desktop inode/directory 2> /dev/null # for thunar

    # - polybar
    fn_CommandCheck 'polybar' || yay -S --noconfirm polybar
    # need SauceCodePro Nerd Font

    # - rofi
    sudo pacman -S --noconfirm rofi
    # need SauceCodePro Nerd Font
    # need Papirus icon theme

    # - ranger
    sudo pacman -S --noconfirm ranger
    # - ranger optional packages
    sudo pacman -S --noconfirm w3m atool mediainfo perl-image-exiftool mupdf
    # sudo pacman -S --noconfirm ffmpegthumbnailer # need 38+1 packages
    [[ -f /usr/lib/w3m/w3mimgdisplay ]] && sudo ln /usr/lib/w3m/w3mimgdisplay /usr/bin
    # need SauceCodePro Nerd Font

    # - dunst
    sudo pacman -S --noconfirm dunst
    # need Papirus icon theme

    # - picom (optional)
    sudo pacman -S --noconfirm picom

    # - alacritty
    sudo pacman -S --noconfirm alacritty

    # - tmux
    sudo pacman -S --noconfirm tmux

    # - vim
    # sudo pacman -S --noconfirm vim
    sudo pacman -S --noconfirm neovim
    sudo pacman -S --noconfirm python-pynvim

    # notify-send
    sudo pacman -S --noconfirm libnotify

    # - GTK Theme (Breeze)
    sudo pacman -S --noconfirm breeze-gtk

    # - Icon Theme (Papirus)
    sudo pacman -S --noconfirm papirus-icon-theme

    # - Cursor Theme (Breeze)
    # manual install

    sudo pacman -S --noconfirm xorg-mkfontscale
    # Updating font cache... /tmp/alpm_cNr36P/.INSTALL: line 4: mkfontscale: command not found
    # /tmp/alpm_cNr36P/.INSTALL: line 5: mkfontdir: command not found

    # - Font (SauceCodePro Nerd Font)
    # yay -S --noconfirm nerd-fonts-source-code-pro
    sudo pacman -S --noconfirm ttf-sourcecodepro-nerd
}

fn_PackagesDeployment(){
    if [[ -s '/etc/os-release' ]]; then
        local l_distro_release_info=${l_distro_release_info:-}
        l_distro_release_info=$(sed -r -n 's@=@|@g;s@"@@g;/^$/d;p' /etc/os-release)
        #distro name，eg: centos/rhel/fedora,debian/ubuntu,opensuse/sles, arch
        distro_name=$(sed -r -n '/^ID\|/{s@^.*?\|(.*)$@\L\1@g;p}' <<< "${l_distro_release_info}")

        distro_family_own=$(sed -r -n '/^ID_LIKE\|/{s@^.*?\|(.*)$@\L\1@g;p}' <<< "${l_distro_release_info}" )

        if [[ "${distro_name}" == 'arch' || "${distro_name}" =~ ^manjaro ]]; then
            distro_family_own='arch'
        elif [[ "${distro_family_own,,}" =~ ^(debian|ubuntu) ]]; then
            distro_family_own='debian'
        fi

        case "${distro_family_own}" in
            arch)
                fn_ArchLinux_Packages
                fn_GTK_Components_Deployment 'c'
            ;;
            debian)
                fn_Ubuntu_Packages
                fn_GTK_Components_Deployment 'i'
                fn_GTK_Components_Deployment 'c'
                fn_GTK_Components_Deployment 'f'
            ;;
        esac

        # tmux bash completion
        # sudo curl -fsSL -o /usr/share/bash-completion/completions/tmux https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux
        bash_completion_dir='/usr/share/bash-completion/completions'
        bash_completion_tmux_path="${bash_completion_dir}/tmux"
        if [[ -d "${bash_completion_dir}" ]]; then
            [[ -f "${bash_completion_tmux_path}" ]] || ${download_method} https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux | sudo tee 1> /dev/null "${bash_completion_tmux_path}"
        fi

        # Add normal user to group 'video'
        # https://github.com/haikarainen/light It changes file /sys/class/backlight/intel_backlight/brightness which file group is 'video', login user need to add into group 'video' to make command light effect.
        if [[ $(groups "${login_user}" 2> /dev/null | sed -r -n '/[[:space:]]*video[[:space:]]*/{p}' | wc -l) -eq 0 ]]; then
            sudo usermod -a -G video "${login_user}"
            sudo light -S 15  # Set brightness to value
        fi

    fi
}


#########  3-1 Config Deployment  #########
fn_GnuPG_Configs_Deployment(){
    if fn_CommandCheck 'gpg2'; then
        local l_gnugp_home_dir=${l_gnugp_home_dir:-"${login_user_home}/.gnupg"}
        # gpg.conf, gpg-agent.conf, dirmngr.conf
        if [[ ! -f "${l_gnugp_home_dir}/gpg-agent.conf" ]]; then
            ${download_method} https://gitlab.com/axdsop/nixnotes/raw/master/CyberSecurity/GnuPG/GnuPG_Configs.sh | bash -s --
        fi
    fi
}

fn_ConfigDeployment(){
    # - download project if not existed in ~/.config
    if [[ ! -d "${user_codebase_dir}" ]]; then
        local l_project_namespace=${l_project_namespace:-'axdsop/nix-dotfiles'}
        mkdir -p "${user_codebase_dir}"

        if fn_CommandCheck 'git'; then
            git clone -q "https://gitlab.com/${l_project_namespace}.git" "${user_codebase_dir}"
        elif fn_CommandCheck 'tar'; then
            #  -z, --gzip, --gunzip, --ungzip  Filter the archive through gzip(1).
            #  -j, --bzip2  Filter the archive through bzip2(1).
            # -J, --xz  Filter the archive through xz(1).
            if fn_CommandCheck 'gzip'; then
                ${download_method} "https://gitlab.com/${l_project_namespace}/-/archive/master/${l_project_namespace##*/}-master.tar.gz" | tar xzf - -C "${user_codebase_dir}" --strip-components=1
            elif fn_CommandCheck 'bzip2'; then
                ${download_method} "https://gitlab.com/${l_project_namespace}/-/archive/master/${l_project_namespace##*/}-master.tar.bz2" | tar xjf - -C "${user_codebase_dir}" --strip-components=1
            else
                ${download_method} "https://gitlab.com/${l_project_namespace}/-/archive/master/${l_project_namespace##*/}-master.tar" | tar xf - -C "${user_codebase_dir}" --strip-components=1
            fi
        fi

        [[ -d "${user_codebase_dir}/.git" ]] && rm -rf "${user_codebase_dir}/.git"
    fi

    target_configs_dir=${target_configs_dir:-"${user_codebase_dir}/Configs"}
    # https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/axdsop_boost_tool.sh
    [[ -d "${target_configs_dir}" ]] || target_configs_dir=$(find "${user_config_dir}" -type f -name 'axdsop_boost_tool.sh' -exec dirname {} \; | xargs dirname | head -n 1)  # ~/.config/axdsop/Configs/

    # Enable Touchpad Tap To Click
    i3_config_path=${i3_config_path:-"${target_configs_dir}/i3/config"}
    if [[ -f "${i3_config_path}" ]]; then
        local l_touchpad_device=${l_touchpad_device:-}
        l_touchpad_device=$(xinput 2> /dev/null | sed -r -n '/touchpad/I{s@^[^[:upper:]]*(.*?)id=.*$@\1@g;s@[[:space:]]+$@@g;p}')
        # exec --no-startup-id xinput set-prop "SYNA1D31:00 06CB:CD48 Touchpad" "libinput Tapping Enabled" 1
        [[ -n "${l_touchpad_device}" ]] && sed -r -i '/xinput set-prop/{s@(.*set-prop[[:space:]]*")[^"]+(.*$)@\1'"${l_touchpad_device}"'\2@g;}' "${i3_config_path}"
    fi

    print_format="%-10s %s %-20s to %s\n"

    echo -e '\nConfiguration deployment begin ... \n'

    find "${target_configs_dir}" -maxdepth 1 -type d ! -path "${target_configs_dir}" | while read -r line; do
        if [[ -n "${line}" && -d "${line}" ]]; then
            line="${line%/}" # remove suffix '/' if exists
            application_name="${line##*/}"
            # i3/alacritty/dunst/picom/polybar/ranger/rofi/tmux/vim/gtk-3.0/

            case "${application_name}" in
                gtk*)
                    # For Arch Linux, 'apropos' is provided by package 'man-db', but it doesn't work like 'apropos gtk- 2> /dev/null | wc -l'
                    local l_is_gtk_existed=${l_is_gtk_existed:-0}
                    fn_CommandCheck 'gtk-launch' && l_is_gtk_existed=1
                    if [[ "${l_is_gtk_existed}" -eq 0 ]]; then
                        [[ $(find /usr/bin/ -name 'gtk*' -print 2> /dev/null | wc -l) -gt 0 ]] && l_is_gtk_existed=1
                    fi

                    if fn_CommandCheck 'i3' && [[ "${l_is_gtk_existed}" -eq 1 ]]; then
                        gtk_config_dir=${gtk_config_dir:-"${user_config_dir}/gtk-3.0"}
                        [[ -d "${gtk_config_dir}" ]] || mkdir -p "${gtk_config_dir}"
                        find "${line}" -type f -name 'settings.ini' -exec cp -f {} "${gtk_config_dir}" \;

                        printf "${print_format}" "${application_name}" 'copy' 'file settings.ini' "${gtk_config_dir}"
                    fi
                ;;
                vim|nvim)
                    # vim/nvim plugin install will make the left while loop item ($line) not execute any more
                    continue
                ;;
                * )
                    # axdsop/ do nothing
                    if fn_CommandCheck "${application_name}"; then
                        local l_is_continue=${l_is_continue:-1}

                        case "${application_name}" in
                            tmux) [[ "${is_normal_action}" -eq 1 ]] && l_is_continue=0 ;;
                        esac

                        if [[ "${l_is_continue}" -eq 1 && -d "${target_configs_dir}/${application_name}" ]]; then
                            ln -fs "${target_configs_dir}/${application_name}/" "${user_config_dir}/"

                            printf "${print_format}" "${application_name}" 'link' "dir  ${application_name}/" "${user_config_dir}/${application_name}"
                        else
                            [[ -L "${user_config_dir}/${application_name}" ]] && rm -rf "${user_config_dir}/${application_name}"
                        fi
                    fi
                ;;
            esac

        fi
    done

    # vim/nvim configuration
    find "${target_configs_dir}" -maxdepth 1 -type d ! -path "${target_configs_dir}" -name '*vim' | while read -r line; do
        if [[ -n "${line}" && -d "${line}" ]]; then
            line="${line%/}" # remove suffix '/' if exists
            application_name="${line##*/}"
            # i3/alacritty/dunst/picom/polybar/ranger/rofi/tmux/vim/gtk-3.0/

            case "${application_name}" in
                vim|nvim)
                    vim_config_dir=${vim_config_dir:-"${login_user_home}/.vim"} # ~/.vim
                    vim_config_path=${vim_config_path:-"${vim_config_dir}/vimrc"}
                    [[ -d "${vim_config_dir}" ]] || mkdir -p "${vim_config_dir}"

                    # find "${line}" -type f -name 'vimrc' -exec ln -fs {} "${vim_config_path}" \;
                    # printf "${print_format}" "${application_name}" 'link' 'file vimrc' "${vim_config_dir}"

                    [[ -L "${vim_config_path}" ]] && rm -f "${vim_config_path}"
                    find "${line}" -type f -name 'vimrc' -exec cp -f {} "${vim_config_path}" \;
                    # [[ "${is_normal_action}" -eq 1 ]] && sed -r -i '/General Config Start/,/General Config End/!d; /colorscheme/{s@^[^[:space:]]*[[:space:]]*@@g}' "${vim_config_path}"

                    if [[ "${is_normal_action}" -eq 1 ]]; then
                        sed -r -i '/General Config Start/,/General Config End/!d; /colorscheme/{s@^[^[:space:]]*[[:space:]]*@@g}' "${vim_config_path}"

                        # https://unix.stackexchange.com/questions/318824/vim-cutpaste-not-working-in-stretch-debian-9
                        [[ -n $(find /usr/share/vim/ -type f -name 'default.vim' -print 2> /dev/null) && -f "${vim_config_path}" ]] && ln -fs "${vim_config_path}" "${login_user_home}/.vimrc"  # For Debian
                    else
                        # vim plugin manager https://github.com/junegunn/vim-plug
                        # curl -fsLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
                        local l_plug_vim_path=${l_plug_vim_path:-"${vim_config_dir}/autoload/plug.vim"}
                        if [[ ! -f "${l_plug_vim_path}" ]]; then
                            mkdir -p "${l_plug_vim_path%/*}"
                            ${download_method} https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim > "${l_plug_vim_path}"
                        fi

                        # plugin install via command line
                        # https://github.com/junegunn/vim-plug/issues/225
                        echo -n -e 'vim        plugins   install    ...  '
                        vim +PlugInstall +visual +qall &> /dev/null
                        echo -n -e '[ok]\n'
                    fi

                    printf "${print_format}" "${application_name}" 'copy' 'file vimrc' "${vim_config_dir}"

                    if fn_CommandCheck 'nvim'; then
                        # ~/.config/nvim
                        nvim_config_dir=${nvim_config_dir:-"${user_config_dir}/nvim"}
                        nvim_config_path=${nvim_config_path:-"${nvim_config_dir}/init.vim"}
                        [[ -d "${nvim_config_dir}" ]] || mkdir -p "${nvim_config_dir}"
                        find "${line}" -type f -name 'init.vim' -exec ln -fs {} "${nvim_config_path}" \;

                        if [[ "${is_normal_action}" -eq 0 ]]; then
                            echo -n -e 'nvim       plugins   install    ...  '
                            nvim +PlugInstall +visual +qall &> /dev/null
                            echo -n -e '[ok]\n'
                        fi

                        printf "${print_format}" 'nvim' 'link' 'file init.vim' "${nvim_config_dir}"
                    fi
                ;;
            esac

        fi
    done

    if fn_CommandCheck 'i3'; then
        xinitrc_save_path=${xinitrc_save_path:-"${login_user_home}/.xinitrc"}
        if [[ ! -f "${xinitrc_save_path}" ]]; then
            find "${target_configs_dir}" -type f -name "${xinitrc_save_path##*/}" -exec cp {} "${xinitrc_save_path}" \;
            printf "${print_format}" 'Xorg' 'copy' 'file .xinitrc' "${login_user_home}/"
        fi

        # input method enable
        if fn_CommandCheck 'fcitx'; then
            sed -r -i '/Fcitx Start/,/Fcitx End/{/export/{s@^#*[[:space:]]*@@g;}}' "${xinitrc_save_path}" 2> /dev/null
        elif fn_CommandCheck 'ibus'; then
            sed -r -i '/IBus Start/,/IBus End/{/export/{s@^#*[[:space:]]*@@g;}}' "${xinitrc_save_path}" 2> /dev/null
        fi

        xresources_save_path=${xresources_save_path:-"${login_user_home}/.Xresources"}
        if [[ ! -f "${xresources_save_path}" ]]; then
            find "${target_configs_dir}" -type f -name "${xresources_save_path##*/}" -exec cp {} "${xresources_save_path}" \;
            printf "${print_format}" 'Xorg' 'copy' 'file .Xresources' "${login_user_home}/"
        fi
    fi

    fn_GnuPG_Configs_Deployment
}


#########  4. Executing Process  #########

fn_Main(){
    fn_Variables_Initialization
    [[ "${is_install_packs}" -eq 1 ]] && fn_PackagesDeployment
    fn_ConfigDeployment
}

fn_Main

# Script End