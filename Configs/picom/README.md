# picom

[picom][picom] is a standalone lightweight compositor for X11, suitable for use with window managers that do not provide compositing.

> The Composite extension for X causes an entire sub-tree of the window hierarchy to be rendered to an off-screen buffer. -- [Xorg#Composite](https://wiki.archlinux.org/index.php/Xorg#Composite)


## Official Wiki

Item|Link
---|---
Project (GitHub)|https://github.com/yshui/picom
Project (GitHub) wiki|https://github.com/yshui/picom/wiki
Arch Linux|https://wiki.archlinux.org/index.php/Picom


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```

### Installation

For Arch Linux

```bash
sudo pacman -S --noconfirm picom
```

For Ubuntu

```bash
# From 20.10 (groovy)
sudo apt-get install -y picom
```

For Ubuntu 20.04 (focal), it doesn't provide prebuilt binary package. But it can be downloaded from [Ubuntu 20.10 (groovy)](https://packages.ubuntu.com/groovy/picom). To install it manually, see note [Components.md](/Configs/Components.md) section *Package Manually Install*.

```bash
# fn_UbuntuPackManualInstall 'picom' 'amd64' 'groovy'
fn_UbuntuPackManualInstall 'picom'
```

### Initialization

To initialize configuration, just save configuration files to directory *~/.config/picom/*.

Enable it in `i3`

```ini
# Composite Manager - Picom
exec_always --no-startup-id picom --config ~/.config/picom/picom.conf
```


## Config Explanation

Specific configuration see file [picom.conf][picom_conf].

Configuration directory tree architecture

```txt
.
├── picom.conf
└── README.md

0 directories, 2 files
```

### Default Config

From `man picom`, [picom][picom] could read from a configuration file if `libconfig` support is compiled in. If `--config` is not used, [picom][picom] will seek for a configuration file in order:

```bash
$XDG_CONFIG_HOME/picom.conf  # ~/.config/picom.conf
$XDG_CONFIG_HOME/picom/picom.conf
$XDG_CONFIG_DIRS/picom.conf  # /etc/xdg/picom.conf
$XDG_CONFIG_DIRS/picom/picom.conf
```

Here choose *$HOME/.config/picom/picom.conf*.

[picom][picom] uses general libconfig configuration file format.

## Usage

### i3

If you wanna enable it in `i3`, add setting in i3 config

```ini
# Composite Manager - Picom
exec_always --no-startup-id picom --config ~/.config/picom/picom.conf
```

### dunst

[Dunst][dunst] support set the transparency of the window via parameter [transparency](/Configs/dunst/dunstrc). This option will only work if a compositing window manager is present (e.g. xcompmgr, compiz, etc.)


## Change Log

* Jun 24, 2020 Wed 20:26 ET
  * first draft
* Sep 21, 2020 Mon 14:57 ET
  * readme rewrite


[picom]:https://github.com/yshui/picom "A lightweight compositor for X11"
[dunst]:https://dunst-project.org/ "Dunst is a lightweight replacement for the notification daemons provided by most desktop environments."
[picom_conf]:./picom.conf

<!-- End -->