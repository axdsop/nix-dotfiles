# Application Configs

Default config directory is `~/.config/` (or `$XDG_CONFIG_HOME`).

Every application has a *README.md* file which note its simple introductions, deployment procedure.

Just click the last colume of the following table.

## Applications

* [i3](/Configs/i3) is tiling window manager
* [picom](/Configs/picom) is compositor for X11
* [Polybar](/Configs/polybar) is status bar
* [Rofi](/Configs/rofi) is application launcher
* [Dunst](/Configs/dunst) is notification daemon
* [Ranger](/Configs/ranger) is text-based file manager
* [Alacritty](/Configs/alacritty) is terminal emulator
* [tmux](/Configs/tmux) is terminal multiplexer
* [vim/nvim](/Configs/vim) is terminal text editor


## Components

Note [Components.md](/Configs/Components.md) lists optional applications about GTK theme, icon theme, cursor theme, font.

You can also manually install package on Ubuntu via function `fn_UbuntuPackManualInstall`.


## Change Log

* Apr 27, 2020 Sun 09:35 ET
  * first draft

<!-- End -->