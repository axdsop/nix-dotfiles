# Rofi Configuration & Usage Note

[Rofi][rofi] is a window switcher, application launcher and dmenu replacement.


## Official Wiki

Item|Link
---|---
Project (GitHub)|https://github.com/davatorium/rofi
Project (GitHub) wiki|https://github.com/davatorium/rofi/wiki
Arch Linux|https://wiki.archlinux.org/index.php/Rofi

Others

- https://github.com/Murzchnvok/rofi-collection
- https://github.com/newmanls/rofi-themes-collection

## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```


### Installation

You can install it via package management tool.

```bash
# - For Arch Linux
sudo pacman -S --noconfirm rofi

# - For Ubuntu
sudo apt-get install -y rofi
```

### Initialization

To initialize configuration, just save configuration files to directory *~/.config/rofi/*.


## Config Explanation

Configuration directory tree architecture

```txt
.
├── config.rasi
├── config_v1.6.0.rasi
└── README.md

0 directories, 3 files
```

### Default Config

[Rofi][rofi]'s configurations, custom themes live in *${XDG_CONFIG_HOME}/rofi/*, on most systems this is *~/.config/rofi/*.

The name of the main configuration file is *config.rasi*. (*~/.config/rofi/config.rasi*).

Here choose *~/.config/rofi/config.rasi*.

If you wanna generate default configuration

```bash
mkdir -pv ~/.config/rofi
rofi -dump-config > ~/.config/rofi/config.rasi # default configuration
```

### Custom Config

My current config [config.rasi][config_rasi] is based on user [Murzchnvok](https://github.com/Murzchnvok)'s project:[polybar-nord](https://github.com/Murzchnvok/polybar-nord), [nord-rofi-theme](https://github.com/Murzchnvok/nord-rofi-theme).

I choose [Papirus icon theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) as icon theme, save directory is */usr/share/icons/*. [config.rasi][config_rasi] config parameter is `icon-theme`.

I also chooses glyphs/icons from font [Sauce Code Pro Nerd Font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/SourceCodePro).

Note: [Ranger](/Configs/ranger/README.md) also choose this font to map file extensions to glyphs/icons.


#### Papirus icon theme

Icon theme dir */usr/share/icons/Papirus*.

To install Papirus icon theme manually, see note [Components.md](/Configs/Components.md) section *Papirus*.


#### Sauce Code Pro Nerd Font

To install SauceCodePro Nerd Font manually, see note [Components.md](/Configs/Components.md) section *Sauce Code Pro*.


### rofi theme selector

Binary file `/usr/bin/rofi-theme-selector` can be used to choose theme fro directory */usr/share/rofi/themes*.


Tree architecture

```txt
/usr/share/rofi/themes
├── Adapta-Nokto.rasi
├── android_notification.rasi
├── Arc-Dark.rasi
├── Arc.rasi
├── arthur.rasi
├── blue.rasi
├── c64.rasi
├── DarkBlue.rasi
├── dmenu.rasi
├── fancy.rasi
├── glue_pro_blue.rasi
├── gruvbox-common.rasi
├── gruvbox-dark-hard.rasi
├── gruvbox-dark.rasi
├── gruvbox-dark-soft.rasi
├── gruvbox-light-hard.rasi
├── gruvbox-light.rasi
├── gruvbox-light-soft.rasi
├── Indego.rasi
├── lb.rasi
├── Monokai.rasi
├── paper-float.rasi
├── Paper.rasi
├── Pop-Dark.rasi
├── purple.rasi
├── sidebar.rasi
├── solarized_alternate.rasi
└── solarized.rasi

0 directories, 28 files
```


## Change Log

- May 08, 2020 Fri 09:05 ET
  - first draft
- Sep 21, 2020 Mon 18:27 ET
  - readme rewrite


[rofi]: https://github.com/davatorium/rofi "Rofi: A window switcher, application launcher and dmenu replacement"
[config_rasi]:./config.rasi

<!-- End -->