# Essential Components Installation

This page lists essential components used in i3 desktop environment.


## TOC

1. [Theme](#theme)  
1.1 [GTK Theme](#gtk-theme)  
1.1.1 [Breeze GTK](#breeze-gtk)  
1.2 [Icon Theme](#icon-theme)  
1.2.1 [Papirus](#papirus)  
1.3 [Cursor Theme](#cursor-theme)  
1.3.1 [Breeze](#breeze)  
2. [Font](#font)  
2.1 [Nerd Font](#nerd-font)  
2.1.1 [Sauce Code Pro](#sauce-code-pro)  
3. [Ubuntu](#ubuntu)  
3.1 [Package Manually Install](#package-manually-install)  
4. [Change Log](#change-log)  


Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```

## Theme

>GTK, or the GIMP Toolkit, is a multi-platform toolkit for creating graphical user interfaces. Offering a complete set of widgets, GTK is suitable for projects ranging from small one-off tools to complete application suites.  -- https://wiki.archlinux.org/index.php/GTK


Rich GTK theme in i3, config file *~/.config/gtk-3.0/settings.ini*, comment via `#`.

```ini
[Settings]
gtk-application-prefer-dark-theme=0

#Dark theme (Breeze-Dark) makes firefox have white space both sides.
#Change about:config widget.content.gtk-theme-override not work.
gtk-theme-name = Breeze

#gtk-fallback-icon-theme = gnome
gtk-icon-theme-name = Papirus

#gtk-font-name = Terminus 11
#gtk-cursor-theme-name="Breeze"
#gtk-cursor-theme-size=64
```

### GTK Theme

To choose GTK themes, see Arch Linux wiki [Uniform look for Qt and GTK applications](https://wiki.archlinux.org/index.php/Uniform_look_for_Qt_and_GTK_applications).

#### Breeze GTK

[Breeze GTK](https://github.com/KDE/breeze-gtk) is a GTK theme built to match KDE's Breeze.

For Arch Linux

```bash
sudo pacman -S --noconfirm breeze-gtk
```

For Ubuntu

```bash
# breeze-gtk-theme - GTK theme built to match KDE's Breeze
sudo apt-get install -y breeze-gtk-theme
```


### Icon Theme

#### Papirus

[Papirus][papirus] is a free and open source SVG icon theme for Linux.

![Papirus Icon Theme](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/preview.png)

Its project [README](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme#installation) list install methods.

Icon theme dir */usr/share/icons/Papirus*.

For Arch Linux

```bash
sudo pacman -S --noconfirm papirus-icon-theme
```

For Ubuntu

```bash
sudo apt-get install -y papirus-icon-theme
```

To install it manually

```bash
# https://github.com/PapirusDevelopmentTeam/papirus-icon-theme

project_name='PapirusDevelopmentTeam/papirus-icon-theme'
tag_name=$($download_method "https://api.github.com/repos/${project_name}/releases/latest" | sed -r -n '/tag_name/{s@[^:]*:[[:space:]]*"([^"]+).*@\1@g;p}')
# https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/archive/20201001.tar.gz
pack_download_link="https://github.com/${project_name}/archive/${tag_name}.tar.gz"

save_dir=$(mktemp -d -t)
pack_save_path="${save_dir}/${pack_download_link##*/}"
# /usr/share/icons/
$download_method "${pack_download_link}" > "${pack_save_path}"

tar xf "${pack_save_path}" -C "${save_dir}"  --strip-components=1
[[ -d /usr/share/icons/ ]] || sudo mkdir -p /usr/share/icons/
sudo cp -R "${save_dir}"/Papirus* /usr/share/icons/
# Papirus / Papirus-Dark / Papirus-Light
[[ -d "${save_dir}" ]] && rm -rf "${save_dir}"
```


### Cursor Theme

To use cursor theme in i3, add configurations into file *~/.Xresources*

```txt
Xcursor.theme: Breeze
Xcursor.size: 64
```

#### Breeze

Cursor theme [Breeze](https://github.com/KDE/breeze/tree/master/cursors) comes from [KDE](https://kde.org).

To install it manually

```bash
# Cursor theme - Breeze from KDE
# https://github.com/KDE/breeze
# https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=xcursor-breeze

site_url='https://download.kde.org/stable/plasma/'
latest_release_version=$($download_method "${site_url}" | sed -r -n '/<td>.*href=/{s@.*href="([^"]+)".*@\1@;p}' |  sed '$!d;s@\/$@@g')  # 5.20.1
pack_download_link="${site_url}/${latest_release_version}/breeze-${latest_release_version}.tar.xz"
# https://download.kde.org/stable/plasma/5.20.1/breeze-5.20.1.tar.xz

save_dir=$(mktemp -d -t)
pack_save_path="${save_dir}/${pack_download_link##*/}"
# /usr/share/icons/
$download_method "${pack_download_link}" > "${pack_save_path}"

tar xf "${pack_save_path}" -C "${save_dir}"  --strip-components=1
[[ -d /usr/share/icons/ ]] || sudo mkdir -p /usr/share/icons/
sudo cp -R "${save_dir}"/cursors/Breeze/Breeze/ /usr/share/icons/
# sudo cp -R "${save_dir}"/cursors/Breeze_Snow/Breeze_Snow/ /usr/share/icons/
[[ -d "${save_dir}" ]] && rm -rf "${save_dir}"
```

## Font

### Nerd Font

[Nerd Fonts][nerd-fonts] is a project that patches developer targeted fonts with a high number of glyphs (icons). Specifically to add a high number of extra glyphs from popular 'iconic fonts' such as [Font Awesome](https://github.com/FortAwesome/Font-Awesome), [Devicons](https://vorillaz.github.io/devicons/), [Octicons](https://github.com/primer/octicons), and [others](https://github.com/ryanoasis/nerd-fonts/#glyph-sets).

Here choose [SourceCodePro](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/SourceCodePro) which is devised by [Adobe Fonts](https://github.com/adobe-fonts/source-code-pro).


#### Sauce Code Pro

To install SauceCodePro Nerd Font

For Arch Linux

>Replace nerd-fonts-source-code-pro with community/ttf-sourcecodepro-nerd? [Y/n]

```bash
# SauceCodePro Nerd Font 28.3 MB
# https://aur.archlinux.org/packages/nerd-fonts-source-code-pro/
# yay -S --noconfirm nerd-fonts-source-code-pro

# https://archlinux.org/packages/community/any/ttf-sourcecodepro-nerd/
sudo pacman -S --noconfirm ttf-sourcecodepro-nerd
```

If your *nix distribution doesn't provide packages, you can also install it manually

```bash
# - install SauceCodePro Nerd Font 28.3 MB
# https://aur.archlinux.org/packages/nerd-fonts-source-code-pro/
# https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=nerd-fonts-source-code-pro

download_link=$($download_method https://api.github.com/repos/ryanoasis/nerd-fonts/releases/latest | sed -r -n '/browser_download_url.*SourceCodePro/{s@[^:]*:[[:space:]]*"([^"]+).*@\1@g;p}')
# https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/SourceCodePro.zip

save_dir=$(mktemp -d -t)
save_path="${save_dir}/${download_link##*/}"
$download_method "${download_link}" > "${save_path}"
[[ -d /usr/share/fonts/TTF ]] || sudo mkdir -p /usr/share/fonts/TTF

#  exclude windows compatible fonts
# find $PWD -type f -name '*.ttf' ! -iname '*windows*'
sudo unzip -oq "${save_path}" -x *Windows* -d /usr/share/fonts/TTF
[[ -d "${save_dir}" ]] && rm -rf "${save_dir}"

# build font cache
fc-cache -f -v
```

## Ubuntu

### Package Manually Install

polybar, tmux, ranger

For Ubuntu 20.04 LTS (focal), some packages are outdated (e.g. `tmux`, `ranger`) or not provide (e.g. `picom`, `polybar`). But Ubuntu 20.10 (groovy) may provdiess prebuilt binary. You can search specific package on its [package page](https://packages.ubuntu.com).

To install package manually, I write a Shell script

```bash
#!/usr/bin/env bash
# Manually install packages from Ubuntu 20.10 (groovy) repository.

fn_UbuntuPackManualInstall(){
    local l_pname="${1:-}"
    local l_arch="${2:-amd64}"
    local l_distro="${3:-groovy}"

    if [[ -n "${l_pname}" ]]; then
        l_pname="${l_pname,,}"
        echo "Target package: ${l_pname}"

        local l_download_method='wget -qO-'
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_method='curl -fsSL'

        local l_ubuntu_pack_site='https://packages.ubuntu.com'
        # Available architecture: amd64 / arm64 / armhf / ppc64el / s390x / all
        case "${l_arch}" in
            all) l_arch='all' ;;
            *) [[ $(uname -m) == 'x86_64' || $(uname -p) == 'x86_64' ]] && l_arch='amd64' ;;
        esac

        local l_pack_detail_page=${l_pack_detail_page:-}
        l_pack_detail_page=$($l_download_method "${l_ubuntu_pack_site}/${l_distro}/${l_pname}" 2> /dev/null | sed -r -n '/href=.*download.*'"${l_arch}"'/{s@.*href="([^"]+)".*@\1@g;p}')
        # https://packages.ubuntu.com/groovy/polybar
        # <th><a href="/groovy/amd64/polybar/download">amd64</a></th>
        # /groovy/amd64/polybar/download

        if [[ -n "${l_pack_detail_page}" ]]; then
            [[ "${l_pack_detail_page}" =~ ^https?:// ]] || l_pack_detail_page="${l_ubuntu_pack_site}/${l_pack_detail_page#*/}"
            # https://packages.ubuntu.com/groovy/amd64/polybar/download

            local l_pack_download_link=${l_pack_download_link:-}
            l_pack_download_link=$($l_download_method "${l_pack_detail_page}" 2> /dev/null | sed -r -n '/<li>.*?href=.*.'"${l_arch}"'.deb/{s@.*href="([^"]+)".*@\1@g;p;q}')
            # http://mirrors.kernel.org/ubuntu/pool/universe/p/polybar/polybar_3.4.3-2_amd64.deb
            echo -e "Real download link: ${l_pack_download_link}\n\nDownloading ..."

            local l_pack_download_name="${l_pack_download_link##*/}"
            cd /tmp
            $l_download_method "${l_pack_download_link}" > "${l_pack_download_name}"

            if [[ -f "${l_pack_download_name}" ]]; then
                echo 'Installing ...'
                sudo dpkg -i "${l_pack_download_name}" 2> /dev/null
                sudo apt-get install -f -y

                echo -e '\nTesting ...'
                whereis "${l_pname}"

                [[ -f "${l_pack_download_name}" ]] && rm "${l_pack_download_name}"
            fi
        else
            echo "Sorry, fail to find package ${l_pname} from ${l_ubuntu_pack_site}/${l_distro}".
        fi
    fi
}
```

Usage

```bash
# fn_UbuntuPackManualInstall pack_name system_arch ubuntu_distro_codename

fn_UbuntuPackManualInstall 'picom'

fn_UbuntuPackManualInstall 'polybar'

fn_UbuntuPackManualInstall 'tmux'

fn_UbuntuPackManualInstall 'ranger' 'all'
```


## Change Log

* Sep 26, 2020 Sat 14:50 ET
  * first draft
* Oct 25, 2020 Sun 19:26 ET
  * Issue fix


[papirus]:https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
[nerd-fonts]:https://github.com/ryanoasis/nerd-fonts/

<!-- End -->