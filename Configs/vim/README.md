# Vim/Neovim Configuration & Usage Note

[Vim][vim] is a terminal text editor, [Neovim][neovim] is a fork of [Vim][vim] aiming to improve user experience, plugins, and GUIs. See `:help vim-differences` for the complete reference of differences from Vim.

## TOC

1. [Official Wiki](#official-wiki)  
1.1 [Learning Resources](#learning-resources)  
2. [Deployment Procedure](#deployment-procedure)  
2.1 [Installation](#installation)  
2.2 [Initialization](#initialization)  
2.2.1 [Plugin Install](#plugin-install)  
2.2.2 [Update Alternatives](#update-alternatives)  
3. [Default Config](#default-config)  
3.1 [Difference](#difference)  
3.2 [Transitioning from Vim](#transitioning-from-vim)  
4. [Config Explanation](#config-explanation)  
5. [Issue Occuring](#issue-occuring)  
5.1 [Follow the link in help manual](#follow-the-link-in-help-manual)  
5.2 [Open Help in new tab](#open-help-in-new-tab)  
5.3 [Vim not enable true color in tmux](#vim-not-enable-true-color-in-tmux)  
6. [Change Log](#change-log)  


## Official Wiki

Item|Vim|Neovim
---|---|---
Official Site|https://www.vim.org|https://neovim.io
Official Guide|https://www.vim.org/docs.php|
Project (GitHub)|https://github.com/vim/vim|https://github.com/neovim/neovim
Project (GitHub) wiki||https://github.com/neovim/neovim/wiki
Arch Linux Wiki|https://wiki.archlinux.org/index.php/Vim|https://wiki.archlinux.org/index.php/Neovim
Gentoo Wiki|https://wiki.gentoo.org/wiki/Vim|

### Learning Resources

* [Learn Vimscript the Hard Way](https://learnvimscriptthehardway.stevelosh.com)
* [Interactive Vim tutorial](https://www.openvim.com)
* [Vim cheatsheet](https://devhints.io/vim)
* [Vim configuration - amix/vimrc](https://github.com/amix/vimrc)


If you are new to vim

>The Vim tutor is a one hour training course for beginners. Often it can be started as `vimtutor`. See `:help tutor` for more information.
>
>The best is to use `:help` in Vim. If you don't have an executable yet, read `runtime/doc/help.txt`. It contains pointers to the other documentation files. The User Manual reads like a book and is recommended to learn to use Vim. See `:help user-manual`. -- [GitHub README](https://github.com/vim/vim#documentation)


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```

### Installation

For Vim

```bash
# For Arch Linux
sudo pacman -S --noconfirm vim

# For Ubuntu
sudo apt-get install -y vim
```

For Neovim, GitHub wiki [Installing Neovim](https://github.com/neovim/neovim/wiki/Installing-Neovim) lists install method.

```bash
# For Arch Linux
sudo pacman -S --noconfirm neovim
# No Python executable found that can import neovim. Using the first available executable for diagnostics.
sudo pacman -S --noconfirm python-pynvim

# For Ubuntu
sudo apt-get install -y neovim
```


### Initialization

You can migrate existing [Vim][vim] configuration to [Neovim][neovim], to initialize configuration, just copy configuration files *vimrc* to *~/.vimrc* or *~/.vim/vimrc*.

```bash
# - vimrc
mkdir -p ~/.vim/
cp Configs/vim/vimrc ~/.vim/vimrc
[[ -n $(find /usr/share/vim/ -type f -name 'default.vim' -print 2> /dev/null) && -f ~/.vim/vimrc ]] && ln -fs ~/.vim/vimrc ~/.vimrc  # For Debian

# - plugin manager
curl -fsLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# - Migrate Vim configuration to Nvim
# ln -fs ~/.vimrc ~/.config/nvim/init.vim
# ln -fs ~/.vim/autoload/ ~/.local/share/nvim/site/autoload/

# From ':help nvim-from-vim'
mkdir -p ~/.config/nvim
cp Configs/vim/init.vim ~/.config/nvim/init.vim
```

#### Plugin Install

To manage plugin, running the following commands

```txt
:PlugInstall
:PlugUpdate
:PlugUpgrade
```

You can also install plugins via command line

```bash
# https://github.com/junegunn/vim-plug/issues/225

# For vim
vim +PlugInstall +visual +qall &> /dev/null

# For Neovim
nvim +PlugInstall +visual +qall &> /dev/null
```

#### Update Alternatives

You can [Set Vim as your default editor for Unix](https://vim.fandom.com/wiki/Set_Vim_as_your_default_editor_for_Unix).

```bash
sudo update-alternatives --config editor
```

<details>
<summary>Click to expand output</summary>

```txt
There are 5 choices for the alternative editor (providing /usr/bin/editor).

  Selection    Path                Priority   Status
------------------------------------------------------------
* 0            /bin/nano            40        auto mode
  1            /bin/ed             -100       manual mode
  2            /bin/nano            40        manual mode
  3            /usr/bin/nvim        30        manual mode
  4            /usr/bin/vim.basic   30        manual mode
  5            /usr/bin/vim.tiny    15        manual mode

Press <enter> to keep the current choice[*], or type selection number:
```

</details>


By default, `/usr/bin/vim` is symbolic link to `/etc/alternatives/vim`. You can set default */usr/bin/vim* via command `update-alternatives`.

```bash
sudo update-alternatives --config vim
```

<details>
<summary>Click to expand output</summary>

```txt
There are 2 choices for the alternative vim (providing /usr/bin/vim).

  Selection    Path                Priority   Status
------------------------------------------------------------
* 0            /usr/bin/vim.basic   30        auto mode
  1            /usr/bin/nvim        30        manual mode
  2            /usr/bin/vim.basic   30        manual mode

Press <enter> to keep the current choice[*], or type selection number:
```

</details>


## Default Config

A file that contains initialization commands is called a *vimrc* file. It is a Vim specific name.  Also see `:help vimrc-intro` or `:help vimrc` in command mode.

Default personal initializations (from `:help vimrc`)

Item|Vim|Neovim
---|---|---
Config dir (Default)|`$HOME/.vim/`|`~/.config/nvim/`
Data dir (Default)|`$HOME/.vim/`|`~/.local/share/nvim/`
User config|`$HOME/.vimrc`<br/>`$HOME/.vim/vimrc`|`~/.config/nvim/init.vim` or `$XDG_CONFIG_HOME/nvim/init.vim`

The files are searched in the order specified above and only the first one that is found is read. It means the first that exists is used, the others are ignored.

To easily find the location and name of the file on any operating system, run `:echo $MYVIMRC` in [Vim][vim]/[Neovim][neovim]. The path will be displayed at the bottom of the screen.


### Difference

From `:help nvim-configuration`, `:help standard-path`

* Use `$XDG_CONFIG_HOME/nvim/init.vim` instead of `.vimrc` for configuration.
* Use `$XDG_CONFIG_HOME/nvim` instead of `.vim` to store configuration files.
* Use `$XDG_DATA_HOME/nvim/shada/main.shada` instead of `.viminfo` for persistent session information.

### Transitioning from Vim

From `:help nvim-from-vim`

```txt
1. To start the transition, create your init.vim (user config) file:

    :call mkdir(stdpath('config'), 'p')
    :exe 'edit '.stdpath('config').'/init.vim'

2. Add these contents to the file:

    set runtimepath^=~/.vim runtimepath+=~/.vim/after
    let &packpath = &runtimepath
    source ~/.vimrc

3. Restart Nvim, your existing Vim config will be loaded.
4. Check Neovim health status
    :checkhealth
```

<details>
<summary>Click to expand init.vim config</summary>

```txt
" From ':help nvim-from-vim'
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

"source ~/.vimrc
if !empty(expand(glob("~/.vimrc")))
    source ~/.vimrc
elseif !empty(expand(glob("~/.vim/vimrc")))
    source ~/.vim/vimrc
endif
```

</details>


## Config Explanation

Configuration directory tree architecture

```txt
.
├── init.vim
├── README.md
├── test
│   └── vimrc_test
└── vimrc

1 directory, 4 files
```

File [vimrc](./vimrc) is the [Vim][vim] configuration file.

File [init.vim](./init.vim) is the [Neovim][neovim] configuration file.


## Issue Occuring

### Follow the link in help manual

Press `Ctrl`-`]` to follow the link (jump to the quickref topic). After browsing the quickref topic, press `Ctrl`-`T` to go back to the previous topic. You can also press `Ctrl`-`O` to jump to older locations, or `Ctrl`-`I` to jump to newer locations.


### Open Help in new tab

[How do I open :help in a new tab?](http://vim.1045645.n5.nabble.com/How-do-I-open-help-in-a-new-tab-td1180564.html)

If I wanna *vimrc* help info in a new tab, solution is type `:tab h vimrc` in command mode.


### Vim not enable true color in tmux

[Vim][vim] doesn't enable true color in tmux, but [Neovim][neovim] works properly. Solution is set *termguicolors* from [issue](https://github.com/tmux/tmux/issues/1246).

```txt
" To use true colors in vim under tmux
if (has("termguicolors"))
  set termguicolors
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
```


## Change Log

* Sep 28, 2020 Mon 09:25 ET
  * first draft
* Sep 29, 2020 Tue 19:50 ET
  * vimrc add [Neovim][neovim] terminal configuration
* Oct 12, 2020 Mon 09:00 ET
  * Add file `init.vim`


[vim]:https://www.vim.org "Vim - the ubiquitous text editor"
[neovim]:https://neovim.io "hyperextensible Vim-based text editor"

<!-- End -->