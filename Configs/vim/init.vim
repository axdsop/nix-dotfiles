" From ':help nvim-from-vim'
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

"source ~/.vimrc
if !empty(expand(glob("~/.vimrc")))
    source ~/.vimrc
elseif !empty(expand(glob("~/.vim/vimrc")))
    source ~/.vim/vimrc
endif