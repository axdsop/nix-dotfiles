# Dunst Configuration & Usage Note

[Dunst][dunst] is a lightweight replacement for the notification daemons provided by most desktop environments.

It’s very customizable, isn’t dependent on any toolkits, and therefore fits into those window manager centric setups we all love to customize to perfection.

## TOC

1. [Official Wiki](#official-wiki)  
2. [Deployment Procedure](#deployment-procedure)  
2.1 [Installation](#installation)  
2.1.1 [For Arch Linux](#for-arch-linux)  
2.1.2 [For Ubuntu](#for-ubuntu)  
2.2 [Initialization](#initialization)  
3. [Config Explanation](#config-explanation)  
3.1 [Default Config](#default-config)  
4. [Change Log](#change-log)  


## Official Wiki

Item|Link
---|---
Official site|https://dunst-project.org/
Official documentation|https://dunst-project.org/documentation/
Project (GitHub)|https://github.com/dunst-project/dunst
Project (GitHub) wiki|https://github.com/dunst-project/dunst/wiki
Arch Linux|https://wiki.archlinux.org/index.php/Dunst
Gentoo|https://wiki.gentoo.org/wiki/Dunst


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```

### Installation

If you wanna build dunst manually, please read GitHub wiki [Installation](https://github.com/dunst-project/dunst/wiki/Installation).

By default, [Dunst][dunst] looks for the [gnome-icon-theme](https://commons.wikimedia.org/wiki/GNOME_Desktop_icons) icons. Here choose [Papirus icon theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) as icon theme, save directory is */usr/share/icons/*. [dunstrc][dunstrc] config parameter is `icon_path`.

[Dunst][dunst] invokes `notify-send` to push notification.


#### For Arch Linux

```bash
sudo pacman -S --noconfirm dunst
# example config /usr/share/dunst/dunstrc

# Icon theme dir /usr/share/icons/
# sudo pacman -R --noconfirm gnome-icon-theme
sudo pacman -S --noconfirm papirus-icon-theme
# save path /usr/share/icons/Papirus/

# notify-send
sudo pacman -S --noconfirm libnotify
```

#### For Ubuntu

```bash
sudo apt-get install -y dunst
# example config /usr/share/doc/dunst/dunstrc.gz

# Icon theme dir /usr/share/icons/
# sudo apt-get install -y gnome-icon-theme
sudo apt-get install -y papirus-icon-theme
# save path /usr/share/icons/Papirus/

# notify-send
sudo apt-get install -y libnotify-bin
```

But the package *papirus-icon-theme* isn't the latest release version, to install Papirus icon theme manually, see note [Components.md](/Configs/Components.md) section *Papirus*.


### Initialization

To initialize configuration, just save configuration files to directory *~/.config/dunst/*.


## Config Explanation

Specific configuration see file [dunstrc][dunstrc].


Configuration directory tree architecture

```txt
.
├── dunstrc
└── README.md

0 directories, 2 files
```

### Default Config

An example configuration file is included (usually */usr/share/dunst/dunstrc*). To change the configuration, copy this file to *~/.config/dunst/dunstrc* and edit it accordingly.

File [dunstrc][dunstrc] custom configuration

item|value
---|---
padding|20
horizontal_padding|20
font|Monospace 12
line_height|4
show_age_threshold|30
max_icon_size|256
icon_path|/usr/share/icons/Papirus/64x64/devices/:/usr/share/icons/Papirus/48x48/status/:/usr/share/icons/Papirus/64x64/apps/
browser|/usr/bin/firefox -new-tab


## Change Log

* May 08, 2020 Fri 09:10 ET
  * first draft
* Sep 20, 2020 Sun 09:20 ET
  * readme rewrite


[dunst]:https://dunst-project.org/ "Dunst is a lightweight replacement for the notification daemons provided by most desktop environments."
[dunstrc]:./dunstrc

<!-- End -->