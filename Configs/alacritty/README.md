# Alacritty Configuration & Usage Note

[Alacritty][alacritty] is a cross-platform, GPU-accelerated terminal emulator. It is written by Rust, so it may be  the fastest terminal emulator in existence.

## TOC

1. [Official Wiki](#official-wiki)  
2. [Deployment Procedure](#deployment-procedure)  
2.1 [Installation](#installation)  
2.2 [Initialization](#initialization)  
3. [Config Explanation](#config-explanation)  
3.1 [Default Config](#default-config)  
3.2 [Color Schemes](#color-schemes)  
4. [Issues Occuring](#issues-occuring)  
4.1 [tmux not recognize alacritty](#tmux-not-recognize-alacritty)  
5. [Change Log](#change-log)  


## Official Wiki

Item|Link
---|---
Project (GitHub)|https://github.com/alacritty/alacritty
Project (GitHub) wiki|https://github.com/alacritty/alacritty/wiki
Arch Linux|https://wiki.archlinux.org/index.php/Alacritty
Gentoo|https://wiki.gentoo.org/wiki/Alacritty


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```


### Installation

GitHub [releases page](https://github.com/alacritty/alacritty/releases) provides precompiled binaries.

You can also install it via package management tool.

```bash
# - For Arch Linux
sudo pacman -S --noconfirm alacritty
# cp /usr/share/doc/alacritty/example/alacritty.yml ~/.config/alacritty/

# - For Ubuntu
sudo apt-get install -y alacritty
```


### Initialization

To initialize configuration, just save configuration files to directory *~/.config/alacritty/*.


## Config Explanation

Configuration directory tree architecture

```txt
.
├── alacritty.yml
├── import
│   └── color_schemes.yml
└── README.md

1 directory, 3 files
```


### Default Config

[Alacritty][alacritty] doesn't create the config file by default, but it looks for the following locations:

```bash
# https://github.com/alacritty/alacritty/blob/master/alacritty.yml

$XDG_CONFIG_HOME/alacritty/alacritty.yml
$XDG_CONFIG_HOME/alacritty.yml
$HOME/.config/alacritty/alacritty.yml
$HOME/.alacritty.yml
```

Here choose *$HOME/.config/alacritty/alacritty.yml*.

File [alacritty.yml](./alacritty.yml) configuration

setcion|item|value
---|---|---
env|TERM|xterm-256color
window|title|Alacritty New World
font|size|14.0
background_opacity||0.95
cursor|style|Underline
live_config_reload||true
colors||*kde


### Color Schemes

[Alacritty][alacritty] provides some [Color schemes](https://github.com/alacritty/alacritty/wiki/Color-schemes).

Custom color schemes saves in file [color_schemes.yml](./import/color_schemes.yml).

```yml
import:
  - ~/.config/alacritty/import/color_schemes.yml
```


## Issues Occuring

### tmux not recognize alacritty

Also see tmux [note](/Configs/tmux/RADEME.md#alacritty).

>open terminal failed: missing or unsuitable terminal: alacritty

Like issue [tmux doesn't recognize alacritty #2487](https://github.com/alacritty/alacritty/issues/2487), solution

```yml
# method 1 - setting environment parameter TERM, default value is alacritty
export TERM=xterm-256color

# method 2 - set env parameter in config file ~/.config/alacritty/alacritty.yml
env:
  # TERM variable
  #
  # This value is used to set the `$TERM` environment variable for
  # each instance of Alacritty. If it is not present, alacritty will
  # check the local terminfo database and use `alacritty` if it is
  # available, otherwise `xterm-256color` is used. Default is `alacritty`.
  TERM: xterm-256color
```

Reason

>The difference is that *xterm-256color* is for the version of the terminal emulator for the X Window System that supports 256 colors. The *alacritty* terminfo is thus for the Alacritty terminal emulator. It describes "the capabilities of hundreds of different display terminals. This allows external programs to be able to have character-based display output, independent of the type of terminal." (Per the Wikipedia article: https://en.wikipedia.org/wiki/Terminfo).

For SSH, just add parameter *RemoteCommand* file `~/.ssh/config` (solution form [How to set TERM environment variable in ssh configuration?](https://serverfault.com/questions/976779/how-to-set-term-environment-variable-in-ssh-configuration#1002884))

```bash
Host XXXX
  User XXXX
  Hostname XXXX
  RemoteCommand TERM=xterm-256color $SHELL
  RequestTTY yes
```


## Change Log

* May 08, 2020 Fri 09:05 ET
  * first draft
* Sep 21, 2020 Mon 08:29 ET
  * readme rewrite
* Oct 25, 2020 Sun 18:45 ET
  * add code section collapse setting
* Apr 23, 2021 Fri 20:20 ET
  * update config template, seperate color schemes in single file


[alacritty]:https://github.com/alacritty/alacritty "A cross-platform, GPU-accelerated terminal emulator"


<!-- End -->