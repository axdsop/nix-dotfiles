# xarchiver Configuration & Usage Note

>[Xarchiver][xarchiver] is the ultimate solution for handling archives on Linux and FreeBSD.
>
>It's a Desktop Environment independent archiver front end. All common archive formats are supported, deb and RPM packages are handled without their package managers being installed. Xarchiver only depends on the GTK+ libraries and utilizes various (un)compressor/(un)archiver command line programs at runtime. Installation of xdg-utils is optional but installing it will allow Xarchiver to support opening more file types directly from inside an archive.


## Official Wiki

Item|Link
---|---
Project (GitHub)|https://github.com/ib/xarchiver
Project (GitHub) wiki|https://github.com/ib/xarchiver/wiki


## Deployment Procedure

### Installation

You can install it via package management tool.

For Arch Linux

```bash
sudo pacman -S --noconfirm xarchiver

Optional dependencies for xarchiver
    arj: ARJ support
    binutils: deb support [installed]
    bzip2: bzip2 support [installed]
    cpio: RPM support
    gzip: gzip support [installed]
    lha: LHA support
    lrzip: lrzip support
    lz4: LZ4 support [installed]
    lzip: lzip support
    lzop: LZOP support
    p7zip: 7z support
    tar: tar support [installed]
    unarj: ARJ support
    unrar: RAR support
    unzip: ZIP support [installed]
    xdg-utils: recognize more file types to open [installed]
    xz: xz support [installed]
    zip: ZIP support [installed]
    zstd: zstd support [installed]
```

For Ubuntu

```bash
sudo apt-get install -y xarchiver
```


### Initialization

To initialize configuration, just save configuration files to directory *~/.config/xarchiver/*.


## Config Explanation

Configuration directory tree architecture

```txt
.
├── README.md
├── xarchiverrc
└── xarchiverrc.init

0 directories, 3 files
```

### Default Config

[Xarchiver][xarchiver]'s configurations live in *${XDG_CONFIG_HOME}/xarchiver/*, on most systems this is *~/.config/xarchiver/*.

The name of the main configuration file is *xarchiverrc*. (*~/.config/xarchiver/xarchiverrc*).

Here choose *~/.config/xarchiver/xarchiverrc*.


## Change Log

* Nov 23, 2020 Mon 10:19 ET
  * first draft


[xarchiver]:https://github.com/ib/xarchiver "GTK+ frontend for most used compression formats"

<!-- End -->