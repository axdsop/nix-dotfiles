# Polybar Configuration & Usage Note


[Polybar][polybar] is a status bar written by Python, it can be used to replace [i3status](https://github.com/i3/i3status) bar.

Personal configuration is based on project [Murzchnvok/polybar-nord](https://github.com/Murzchnvok/polybar-nord).


## TOC

1. [Official Wiki](#official-wiki)  
2. [Deployment Procedure](#deployment-procedure)  
2.1 [Installation](#installation)  
2.2 [Initialization](#initialization)  
3. [Default Config](#default-config)  
4. [Config Explanation](#config-explanation)  
4.1 [launch.sh](#launchsh)  
4.2 [config.ini](#configini)  
4.3 [modules](#modules)  
4.3.1 [glyphs/icons](#glyphsicons)  
4.4 [custom scripts](#custom-scripts)  
5. [Change Log](#change-log)  


## Official Wiki

Item|Link
---|---
Project (GitHub)|https://github.com/polybar/polybar
Project (GitHub) wiki|https://github.com/polybar/polybar/wiki
Arch Linux|https://wiki.archlinux.org/index.php/Polybar


Some community themes or scripts:

* [polybar/polybar-scripts](https://github.com/polybar/polybar-scripts)
* [adi1090x/polybar-themes](https://github.com/adi1090x/polybar-themes)
* [TiagoDanin/Awesome-Polybar](https://github.com/TiagoDanin/Awesome-Polybar)


How to insert unicode character see stackoverflow [post](https://stackoverflow.com/questions/1239712/entering-unicode-data-in-visual-studio-c-sharp#56033456).

>Just in case someone is looking how to do this in Linux, `Ctrl`+`Shift`+`U`, then type the code, and finally press `Space` or `Enter`, then the character will appear. Works with every application.


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```


### Installation

For Arch Linux

```bash
# - install polybar
yay -S --noconfirm polybar

# sudo pacman -S --noconfirm xorg-fonts-misc
# yay -S --noconfirm ttf-unifont siji-git
# cp /usr/share/doc/polybar/config ~/.config/i3/polybar/
# polybar -c ~/.config/i3/polybar/config example # testing

# Optional dependencies for polybar
#     i3-wm: i3 module support [installed]
#     ttf-unifont: Font used in example config
#     siji-git: Font used in example config
#     xorg-fonts-misc: Font used in example config

# - install SauceCodePro Nerd Font 28.3 MB
# https://aur.archlinux.org/packages/nerd-fonts-source-code-pro/
# yay -S --noconfirm nerd-fonts-source-code-pro
sudo pacman -S --noconfirm ttf-sourcecodepro-nerd
```

For Ubuntu

```bash
# From 20.10 (groovy)
sudo apt-get install -y polybar
```

For Ubuntu 20.04 (focal), it doesn't provide prebuilt binary package. But it can be downloaded from [Ubuntu 20.10 (groovy)](https://packages.ubuntu.com/groovy/polybar). To install it manually, see note [Components.md](/Configs/Components.md) section *Package Manually Install*.

```bash
# fn_UbuntuPackManualInstall 'polybar' 'amd64' 'groovy'
fn_UbuntuPackManualInstall 'polybar'
```

If you need `notify-send` to push notification.

```bash
# notify-send push notification
sudo apt-get install -y libnotify-bin
```

To install SauceCodePro Nerd Font manually, see note [Components.md](/Configs/Components.md) section *Sauce Code Pro*.


### Initialization

To initialize configuration, just save configuration files to directory *~/.config/polybar/*.

To enable polybar in [i3][i3], modifing configuration file *~/.config/i3/config*:

* Comment *bar{}* section
* Add the following configuration

```bash
# bindsym $mod+x mode "$mode_display"
bindsym $mod+x exec --no-startup-id bash ~/.config/polybar/scripts/monitor_menu

# Polybar
exec_always --no-startup-id bash ~/.config/polybar/launch.sh
```


## Default Config

Wiki [Configuration](https://github.com/polybar/polybar/wiki/Configuration) says:

>The configuration uses the [INI file format](https://en.wikipedia.org/wiki/INI_file).

From `man polybar`, by default, the configuration file of [Polybar][polybar] is loaded from:

```bash
$XDG_CONFIG_HOME/polybar/config
$HOME/.config/polybar/config

# sample config:  /usr/share/doc/polybar/config
```

Here choose *~/.config/polybar/config*. But some configuration files will add suffix `.ini`.


## Config Explanation

Configuration directory tree architecture

```txt
.
├── config.ini
├── envs_init.sh
├── includes
│   ├── bar_entry.ini
│   ├── colors
│   │   └── color.ini
│   ├── includes.ini
│   └── modules
│       ├── configs
│       │   ├── custom.ini
│       │   ├── mpris.ini
│       │   └── sys.ini
│       └── positions
│           ├── bar_mpris.ini
│           └── bar_normal.ini
├── launch.sh
├── README.md
└── scripts
    ├── monitor_menu
    ├── mpris_player
    │   ├── develop_data
    │   │   ├── mpris_cli_operation.md
    │   │   └── mpris_player_raw_data.md
    │   ├── mpris_player_control
    │   └── README.md
    ├── power_menu
    └── screen_recorder

8 directories, 19 files
```

### launch.sh

Script [launch.sh](./launch.sh) is used to launch [Polybar][polybar]. It has 2 purposes:

1. Kill existed running Polybar process;
2. Start new Polybar process;

Default configuration is too simple, here I add other operation:

* monitors detection;
* network card name detection;
* Polybar config path detection;


<details>
<summary>Click to expand script envs_init.sh</summary>

```bash
#!/usr/bin/env bash

# Export environment vairables for Polybar


export XRANDR_MONITOR_DEFAULT=$(ls /sys/class/drm/*/drm_dp_aux*/dev | xargs -i{} bash -c "echo -n {}' '; cat < {}" | sed -r -n '/:0$/{s@.*?card[[:digit:]]+-([^\/]+).*@\1@g;p}')
export XRANDR_MONITOR_EXTEND=$(ls /sys/class/drm/*/status | xargs -i{} bash -c "echo -n {}' '; cat < {}" | sed -r -n '/[[:space:]]+connected/{s@.*?card[[:digit:]]+-([^\/]+).*@\1@g;/^'"${XRANDR_MONITOR_DEFAULT}"'/d;p;q}')
# sed -r -n '/INTERFACE/I{s@^[^=]+=@@g;p;q}' /sys/class/net/wl*/uevent
export DEFAULT_NETWORK_INTERFACE=$(ip route 2> /dev/null | sed -r -n '/default/{s@.*?dev[[:space:]]*([^[:space:]]+).*@\1@p}')
export DEFAULT_BATTERY_BATTERY=$(ls /sys/class/power_supply/*/type | xargs -i{} sh -c "echo -n {}' '; cat < {}" | sed -r -n '/battery$/I{s@.*power_supply\/([^\/]+).*@\1@g;p;q}')
export DEFAULT_BATTERY_ADAPTER=$(ls /sys/class/power_supply/ 2> /dev/null | xargs -l | sed -n '/^AC/{p;q}')
DEFAULT_BATTERY_FULL_DEFAULT=${DEFAULT_BATTERY_FULL_DEFAULT:-95}
DEFAULT_BATTERY_FULL=${DEFAULT_BATTERY_FULL:-0}
[[ -f /sys/class/power_supply/${DEFAULT_BATTERY_BATTERY}/charge_control_end_threshold ]] && DEFAULT_BATTERY_FULL=$(head -n 1 /sys/class/power_supply/${DEFAULT_BATTERY_BATTERY}/charge_control_end_threshold)
[[ "${DEFAULT_BATTERY_FULL}" -eq 0 ]] && DEFAULT_BATTERY_FULL="${DEFAULT_BATTERY_FULL_DEFAULT}"
export DEFAULT_BATTERY_FULL="${DEFAULT_BATTERY_FULL_DEFAULT}"


# Detect polybar configuration files path
polybar_conf_dir_default=${polybar_conf_dir_default:-"$HOME/.config/polybar"}
polybar_conf_dir="${polybar_conf_dir_default}"
[[ -d "${polybar_conf_dir}" ]] || polybar_conf_dir=$(dirname $(readlink -nf "$0"))
[[ -d "${polybar_conf_dir_default}" ]] || ln -fs "${polybar_conf_dir}" "${polybar_conf_dir_default}"
polybar_config_path="${polybar_conf_dir}/config.ini"
[[ -f "${polybar_config_path}" ]] || polybar_config_path="${polybar_conf_dir}/config"
# export POLYBAR_CONF_DIR="${polybar_conf_dir}"

# Script End
```

</details>

<details>
<summary>Click to expand script lanuch.sh</summary>

```bash
#!/usr/bin/env bash

# Terminate already running bar instances
#killall -q polybar 2> /dev/null

# Wait until the processes have been shut down
#while pgrep -u $UID -x polybar &> /dev/null; do sleep 1; done
while pgrep -u $UID -x polybar &> /dev/null; do killall -q polybar 2> /dev/null; done

# include environments detecting script
current_dir=$(dirname $(readlink -nf "$0"))
envs_script_path="${current_dir}/envs_init.sh"

if [[ -f "${envs_script_path}" ]]; then
    . "${envs_script_path}"

    # Launch polybar (primary, extend are bar name)
    [[ -n $XRANDR_MONITOR_DEFAULT ]] && polybar primary --reload -c "${polybar_config_path}" &
    [[ -n $XRANDR_MONITOR_EXTEND ]] && polybar extend --reload -c "${polybar_config_path}" &
fi

# [[ -n $(dbus-send --print-reply --dest=org.freedesktop.DBus /org/freedesktop/DBus org.freedesktop.DBus.ListNames 2> /dev/null | sed -r -n '/org.mpris.MediaPlayer2/{s@.*org.mpris.MediaPlayer2.([^"]+).*@\1@g;p}') ]] && polybar primary_mpris --reload -c "${polybar_config_path}" &

# Script End
```

</details>

To enable Polybar in [i3][i3]

```bash
# Polybar
exec_always --no-startup-id bash ~/.config/polybar/launch.sh
```

### config.ini

File [config.ini](./config.ini) is the core and entry configuration file. By default, it is monolithic which means all configurations saved in single file. To make it easy to maintance, I divide it into several components:

```txt
polybar/includes
.
├── bar_entry.ini
├── colors
│   └── color.ini
├── includes.ini
└── modules
    ├── configs
    │   ├── custom.ini
    │   ├── mpris.ini
    │   └── sys.ini
    └── positions
        ├── bar_mpris.ini
        └── bar_normal.ini

4 directories, 8 files
```

File [includes.ini](./includes/includes.ini) includes components need to be inclued.

```ini
; bars
include-file = ~/.config/polybar/includes/bar_entry.ini
;colors
include-file = ~/.config/polybar/includes/colors/color.ini
; modules
include-file = ~/.config/polybar/includes/modules/configs/sys.ini
include-file = ~/.config/polybar/includes/modules/configs/custom.ini
include-file = ~/.config/polybar/includes/modules/configs/mpris.ini
```

File [bar_entry.ini](./includes/bar_entry.ini) contains bar configurations.

File [color.ini](./includes/colors/color.ini) contains color configurations.

Bar modules list in directory [modules/configs](./includes/modules/configs).

Bar modules position organization list in directory [modules/positions](./includes/modules/positions).


### modules

Here list 3 module configuration file

item|explanation
---|---
[sys.ini](./includes/modules/configs/sys.ini)|modules provided by [Polybar][polybar] (see [wiki](https://github.com/polybar/polybar/wiki))
[custom.ini](./includes/modules/configs/custom.ini)|perconal common custom moduels
[mpris.ini](./includes/modules/configs/mpris.ini)|modules just for mpris control (script [mpris_player_control](./scripts/mpris_player))


#### glyphs/icons

Bar [modules](./includes/modules/configs) use glyphs/icons font [Sauce Code Pro Nerd Font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/SourceCodePro).

To install SauceCodePro Nerd Font manually, see note [Components.md](/Configs/Components.md) section *Sauce Code Pro*.


### custom scripts

To make [Polybar][polybar] more powerfully, I write some Shell scripts invoked by bar modules or [i3][i3].

```txt
.
├── monitor_menu
├── mpris_player
│   ├── mpris_cli_operation.md
│   ├── mpris_player_control
│   ├── mpris_player_raw_data.md
│   └── README.md
├── nmcli_wifi_operation
├── power_menu
└── screen_recorder

1 directory, 8 files
```

To learn more about these scripts, please read its source code or readme if provided.

For example, scipt [mpris_player_control](./scripts/mpris_player/) provides [README](./scripts/mpris_player/README.md). To list help info

```bash
bash mpris_player_control -h
```


## Change Log

* May 10, 2020 Sun 20:23 ET
  * first draft
* Jun 17, 2020 Wed 22:03 ET
  * script mpris_player_control support ipc module
* Sep 26, 2020 Sat 09:20 ET
  * reorganize architecture, rewrite readme


[i3]:https://i3wm.org/ "i3 - improved tiling wm"
[polybar]:https://polybar.github.io/ "A fast and easy to use tool for creating status bars"


<!-- End -->