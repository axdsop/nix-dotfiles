#!/usr/bin/env bash

# Export environment vairables for Polybar


export XRANDR_MONITOR_DEFAULT=$(ls /sys/class/drm/*/drm_dp_aux*/dev | xargs -i{} bash -c "echo -n {}' '; cat < {}" | sed -r -n '/:0$/{s@.*?card[[:digit:]]+-([^\/]+).*@\1@g;p}')
export XRANDR_MONITOR_EXTEND=$(ls /sys/class/drm/*/status | xargs -i{} bash -c "echo -n {}' '; cat < {}" | sed -r -n '/[[:space:]]+connected/{s@.*?card[[:digit:]]+-([^\/]+).*@\1@g;/^'"${XRANDR_MONITOR_DEFAULT}"'/d;p;q}')
# sed -r -n '/INTERFACE/I{s@^[^=]+=@@g;p;q}' /sys/class/net/wl*/uevent
export DEFAULT_NETWORK_INTERFACE=$(ip route 2> /dev/null | sed -r -n '/default/{s@.*?dev[[:space:]]*([^[:space:]]+).*@\1@p}')
export DEFAULT_BATTERY_BATTERY=$(ls /sys/class/power_supply/*/type | xargs -i{} sh -c "echo -n {}' '; cat < {}" | sed -r -n '/battery$/I{s@.*power_supply\/([^\/]+).*@\1@g;p;q}')
export DEFAULT_BATTERY_ADAPTER=$(ls /sys/class/power_supply/ 2> /dev/null | xargs -l | sed -n '/^AC/{p;q}')
DEFAULT_BATTERY_FULL_DEFAULT=${DEFAULT_BATTERY_FULL_DEFAULT:-95}
DEFAULT_BATTERY_FULL=${DEFAULT_BATTERY_FULL:-0}
[[ -f /sys/class/power_supply/${DEFAULT_BATTERY_BATTERY}/charge_control_end_threshold ]] && DEFAULT_BATTERY_FULL=$(head -n 1 /sys/class/power_supply/${DEFAULT_BATTERY_BATTERY}/charge_control_end_threshold)
[[ "${DEFAULT_BATTERY_FULL}" -eq 0 ]] && DEFAULT_BATTERY_FULL="${DEFAULT_BATTERY_FULL_DEFAULT}"
(( DEFAULT_BATTERY_FULL-=1 ))
export DEFAULT_BATTERY_FULL


# Detect polybar configuration files path
polybar_conf_dir_default=${polybar_conf_dir_default:-"$HOME/.config/polybar"}
polybar_conf_dir="${polybar_conf_dir_default}"
[[ -d "${polybar_conf_dir}" ]] || polybar_conf_dir=$(dirname $(readlink -nf "$0"))
[[ -d "${polybar_conf_dir_default}" ]] || ln -fs "${polybar_conf_dir}" "${polybar_conf_dir_default}"
polybar_config_path="${polybar_conf_dir}/config.ini"
[[ -f "${polybar_config_path}" ]] || polybar_config_path="${polybar_conf_dir}/config"
# export POLYBAR_CONF_DIR="${polybar_conf_dir}"

# Script End