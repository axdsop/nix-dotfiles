#!/usr/bin/env bash

# Terminate already running bar instances
#killall -q polybar 2> /dev/null

# Wait until the processes have been shut down
#while pgrep -u $UID -x polybar &> /dev/null; do sleep 1; done
while pgrep -u $UID -x polybar &> /dev/null; do killall -q polybar 2> /dev/null; done

# include environments detecting script
current_dir=$(dirname $(readlink -nf "$0"))
envs_script_path="${current_dir}/envs_init.sh"

if [[ -f "${envs_script_path}" ]]; then
    . "${envs_script_path}"

    # Launch polybar (primary, extend are bar name)
    [[ -n $XRANDR_MONITOR_DEFAULT ]] && polybar primary --reload -c "${polybar_config_path}" &
    [[ -n $XRANDR_MONITOR_EXTEND ]] && polybar extend --reload -c "${polybar_config_path}" &
fi

# [[ -n $(dbus-send --print-reply --dest=org.freedesktop.DBus /org/freedesktop/DBus org.freedesktop.DBus.ListNames 2> /dev/null | sed -r -n '/org.mpris.MediaPlayer2/{s@.*org.mpris.MediaPlayer2.([^"]+).*@\1@g;p}') ]] && polybar primary_mpris --reload -c "${polybar_config_path}" &

# Script End