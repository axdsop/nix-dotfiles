# Ranger Configuration & Usage Note

[Ranger][ranger] is a text VIM-inspired filemanager for the console.

It is written by Python, so its speed is a little slow while opening large size file, sometimes it will be freezed.


## TOC

1. [Official Wiki](#official-wiki)  
2. [Deployment Procedure](#deployment-procedure)  
2.1 [Installation](#installation)  
2.1.1 [For Arch Linux](#for-arch-linux)  
2.1.2 [For Ubuntu](#for-ubuntu)  
2.2 [Initialization](#initialization)  
3. [Config Explanation](#config-explanation)  
3.1 [Default Config](#default-config)  
3.2 [File Preview](#file-preview)  
3.2.1 [octet-stream support](#octet-stream-support)  
4. [Plugins](#plugins)  
4.1 [linemode devicons](#linemode-devicons)  
4.1.1 [Sauce Code Pro Nerd Font](#sauce-code-pro-nerd-font)  
5. [Issues Occuring](#issues-occuring)  
5.1 [command identify not found](#command-identify-not-found)  
6. [Learning Resource](#learning-resource)  
6.1 [Cheatsheet](#cheatsheet)  
7. [Change Log](#change-log)  


## Official Wiki

Item|Link
---|---
Official site|https://ranger.github.io/
Official documentation|https://ranger.github.io/documentation.html
Project (GitHub)|https://github.com/ranger/ranger
Project (GitHub) wiki|https://github.com/ranger/ranger/wiki
Arch Linux|https://wiki.archlinux.org/index.php/Ranger


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```

### Installation

Official project site lists [optional dependencies](https://github.com/ranger/ranger#optional-dependencies) to enhance file previews (with [scope.sh](https://github.com/ranger/ranger/blob/master/ranger/data/scope.sh)).


#### For Arch Linux

```bash
# - text-based file manager
sudo pacman -S --noconfirm ranger

# Optional dependencies for ranger
#     atool: for previews of archives
#     elinks: for previews of html pages
#     ffmpegthumbnailer: for video previews
#     highlight: for syntax highlighting of code
#     libcaca: for ASCII-art image previews
#     lynx: for previews of html pages
#     mediainfo: for viewing information about media files
#     odt2txt: for OpenDocument texts
#     perl-image-exiftool: for viewing information about media files
#     poppler: for pdf previews
#     python-chardet: in case of encoding detection problems
#     sudo: to use the "run as root"-feature [installed]
#     transmission-cli: for viewing bittorrent information
#     w3m: for previews of images and html pages


# To stop ranger from loading both the default and your custom rc.conf, please set the environment variable RANGER_LOAD_DEFAULT_RC to FALSE.
# add 'export RANGER_LOAD_DEFAULT_RC=false' into ~/.bashrc

# ~/.config/ranger/
# ranger --copy-config=all
```

Optional pakcages

```bash
# - Images and HTML pages preview
sudo pacman -S --noconfirm w3m
# change rc.conf 'set preview_images true', 'set preview_images_method w3m'
[[ -f /usr/lib/w3m/w3mimgdisplay ]] && sudo ln /usr/lib/w3m/w3mimgdisplay /usr/bin

# - Archive preview
sudo pacman -S --noconfirm atool

# - Video previews (install 38+1 packages)
# sudo pacman -S --noconfirm ffmpegthumbnailer

# - Media info view
sudo pacman -S --noconfirm mediainfo  # provide mediainfo
sudo pacman -S --noconfirm perl-image-exiftool  # provide exiftool

# - PDF preview
sudo pacman -S --noconfirm poppler
# pdf viewer choose mupdf (install 2 packages) https://www.mupdf.com/docs/manual-mupdf-gl.html # sudo pacman -S --noconfirm mupdf
```

#### For Ubuntu

```bash
sudo apt-get install -y ranger

# Suggested packages:
# atool caca-utils highlight | python-pygments mediainfo | exiftool unoconv cmigemo dict dict-wn dictd libsixel-bin mpv w3m-el xsel
```

If you use Ubuntu 20.04 LTS (focal), this [package](https://packages.ubuntu.com/focal/ranger) is outdated.

Ubuntu 20.10 (groovy) provides the latest prebuilt binary [package](https://packages.ubuntu.com/groovy/ranger) (architecture just has *all*). To install it manually, see note [Components.md](/Configs/Components.md) section *Package Manually Install*.

```bash
# fn_UbuntuPackManualInstall 'ranger' 'all' 'groovy'
fn_UbuntuPackManualInstall 'ranger' 'all'
```

Optional pakcages

```bash
# - Images and HTML pages preview
sudo apt-get install -y w3m
# change rc.conf 'set preview_images true', 'set preview_images_method w3m'
[[ -f /usr/lib/w3m/w3mimgdisplay ]] && sudo ln -fs /usr/lib/w3m/w3mimgdisplay /usr/local/bin

# - Archive preview
sudo apt-get install -y atool

# - Video previews (install 37 packages)
# sudo apt-get install -y ffmpegthumbnailer

# - Media info view
sudo apt-get install -y mediainfo  # provide mediainfo
sudo pacman -S --noconfirm libimage-exiftool-perl  # provide exiftool

# - PDF preview
sudo pacman -S --noconfirm poppler-utils
# pdf viewer choose mupdf (install 2 packages) https://www.mupdf.com/docs/manual-mupdf-gl.html # sudo apt-get install -y mupdf
```


### Initialization

To initialize configuration, just save configuration files to directory *~/.config/ranger/*.


```bash
# scope.sh need execute permission
chmod 750 scope.sh
```


## Config Explanation

Configuration directory tree architecture

```txt
├── commands_full.py
├── commands.py
├── plugins
│   ├── devicons_linemode.py
│   └── devicons.py
├── rc.conf
├── README.md
├── rifle.conf
└── scope.sh

1 directory, 8 files
```


### Default Config

After startup, ranger creates a configuration directory `~/.config/ranger`.

```bash
mkdir -pv ~/.config/ranger/
```

To copy the default configuration to this directory issue the following command:

```bash
# https://github.com/ranger/ranger/tree/master/ranger/config
ranger --copy-config=all

# rc.conf - startup commands and key bindings
# commands.py - commands which are launched with ':'
# rifle.conf - applications used when a given type of file is launched.
```

*rc.conf* only needs to include changes from the default file as both are loaded.


### File Preview

Script [scope.sh](./scope.sh) is used to file preview.

Attention: ranger provides suffix *.gz* file on Ubuntu.

```bash
# https://github.com/ranger/ranger/blob/master/ranger/data/scope.sh
ranger_doc_config_dir='/usr/share/doc/ranger/config'

if [[ -d "${ranger_doc_config_dir}" ]]; then
    [[ -f "${ranger_doc_config_dir}/scope.sh.gz" ]] && sudo gzip -d -k "${ranger_doc_config_dir}/scope.sh.gz"

    if [[ -f "${ranger_doc_config_dir}/scope.sh" ]]; then
        cat "${ranger_doc_config_dir}/scope.sh" > ~/.config/ranger/scope.sh
        chmod 750 ~/.config/ranger/scope.sh
    fi
fi
```

It must has execution permission, otherwise it will prompts error info:

>Priview script `/PATH/scope.sh` is not executable!

Uncomment functions in [scope.sh](./scope.sh) to support other format (e.g. pdf,video,audio), but need to install additional packages.


File [rc.conf](./rc.conf) configuration

item|value|note
---|---|---
use_preview_script|true|
preview_images|true
preview_images_method|w3m|use `w3m` for image preview


#### octet-stream support

Add support for MIME type *application/octet-stream*, add into file `scope.sh`.

```bash
handle_mime() {
    local mimetype="${1}"
    case "${mimetype}" in
        # ...
        # ...
        ## Generic binary data
        application/octet-stream )
            # https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
            # Generic binary data (or binary data whose true type is unknown) is application/octet-stream. It may be in any multimedia format.
            # music format .dts
            mediainfo "${FILE_PATH}" && exit 5
            exiftool "${FILE_PATH}" && exit 5
            exit 1;;
    esac
}
```

## Plugins

Plugins save directory is `~/.config/ranger/plugins/`.

### linemode devicons

Official wiki [Custom linemodes](https://github.com/ranger/ranger/wiki/Custom-linemodes).

Mappying file extensions to glyphs/icons which use font [Sauce Code Pro Nerd Font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/SourceCodePro).

```bash
# - For devicons.py
# https://github.com/alexanderjeurissen/ranger_devicons
$download_method https://raw.githubusercontent.com/alexanderjeurissen/ranger_devicons/main/devicons.py > ~/.config/ranger/plugins/devicons.py

# - For devicons_linemode.py
# https://git.mehalter.com/mehalter/dotfiles/src/branch/master/ranger
$download_method https://git.mehalter.com/mehalter/dotfiles/src/branch/master/ranger/.config/ranger/plugins/devicons_linemode.py > ~/.config/ranger/plugins/devicons_linemode.py
```

To enable it, add the following setting into file [rc.conf](./rc.conf)

```bash
# Custom-linemodes
default_linemode devicons
```


Add the following settings into file `.gitignore`

```git
**/__pycache__/
**/__init__.py
```

#### Sauce Code Pro Nerd Font

To install SauceCodePro Nerd Font manually, see note [Components.md](/Configs/Components.md) section *Sauce Code Pro*.


## Issues Occuring

### command identify not found

Script [scope.sh](./scope.sh) use command `identify` to process image preview in function *handle_image()*.

> orientation="$( identify -format '%[EXIF:Orientation]\n' -- "${FILE_PATH}" )"

It prompts error info

```txt
Command 'identify' not found, but can be installed with:

sudo apt install graphicsmagick-imagemagick-compat  # version 1.4+really1.3.35-1, or
sudo apt install imagemagick-6.q16                  # version 8:6.9.10.23+dfsg-2.1ubuntu11
sudo apt install imagemagick-6.q16hdri              # version 8:6.9.10.23+dfsg-2.1ubuntu11
```

Try to install these packages, but it still doesn't work.


## Learning Resource

### Cheatsheet

![cheatsheet](https://raw.githubusercontent.com/ranger/ranger/master/doc/cheatsheet.svg)


## Change Log

* May 08, 2020 Fri 09:05 ET
  * first draft
* May 16, 2020 Sat 20:30 ET
  * add optional packages info, cheatsheet
* Sep 20, 2020 Sun 20:01 ET
  * readme rewrite


[ranger]:https://ranger.github.io/ "A VIM-inspired filemanager for the console"


<!-- End -->