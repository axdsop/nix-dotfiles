# i3 Configuration & Usage Note

[i3][i3] is a tiling window manager for X11.


## TOC

1. [Official Wiki](#official-wiki)  
2. [Deployment Procedure](#deployment-procedure)  
2.1 [Installation](#installation)  
2.1.1 [For Arch Linux](#for-arch-linux)  
2.1.2 [For Ubuntu](#for-ubuntu)  
2.2 [Initialization](#initialization)  
3. [Config Explanation](#config-explanation)  
4. [Issue Occuring](#issue-occuring)  
4.1 [White Screen With HiDPI](#white-screen-with-hidpi)  
4.2 [GnuPG GUI Prompt Passphase](#gnupg-gui-prompt-passphase)  
5. [Bibliography](#bibliography)  
6. [Change Log](#change-log)  


## Official Wiki

Item|Link
---|---
Official Site|https://i3wm.org
Official Guide|https://i3wm.org/docs/userguide.html
Project (GitHub)|i3 https://github.com/i3/i3<br/>~~i3-gaps https://github.com/Airblader/i3~~
Arch Linux|https://wiki.archlinux.org/title/I3
Gentoo|https://wiki.gentoo.org/wiki/I3


~~Note: [i3-gaps][i3-gaps] is a fork of [i3][i3], adding a few additional features such as gaps between windows and more setting of borders.~~

Note: [i3-gaps][i3-gaps] (a fork of [i3][i3] with gaps and other features) was recently merged into [i3-wm](https://archlinux.org/packages/?name=i3-wm), and as such is no longer available. Install the `i3-wm` package instead.


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```

### Installation

Official [Downlaod](https://i3wm.org/downloads/) page lists *nix distributions provide prebuild package.

#### For Arch Linux

```bash
# - remove unneeded packages
sudo pacman -R --noconfirm i3-wm i3status dmenu

# - install i3-wm, i3lock
sudo pacman -S --noconfirm i3-wm i3lock

# Optional dependencies for i3-wm
#     dmenu: for the default program launcher
#     rofi: for a modern dmenu replacement
#     i3lock: for the default screen locker
#     xss-lock: for the default screen locker
#     i3status: for the default status bar generator
#     perl: for i3-save-tree and i3-dmenu-desktop
#     perl-anyevent-i3: for i3-save-tree
#     perl-json-xs: for i3-save-tree

```

Essential pakcages

```bash
# - screen brightness controls
# https://github.com/haikarainen/light It changes file /sys/class/backlight/intel_backlight/brightness which file group is 'video', login user need to add into group 'video' to make command light effect.
sudo pacman -S --noconfirm light
sudo usermod -a -G video $USER
sudo light -S 15  # Set brightness to value

# - volume control
# pactl https://wiki.archlinux.org/title/PulseAudio
sudo pacman -S --noconfirm pulseaudio pulseaudio-alsa
```

Optional packages

```bash
# https://wiki.archlinux.org/title/Screen_capture
# scrot (SCReenshOT) - command line screen capture utility
sudo pacman -S --noconfirm scrot

# - image viewer
# feh — image viewer and cataloguer, change desktop background
sudo pacman -S --noconfirm feh

# - PDF viewer
sudo pacman -S --noconfirm mupdf

# - File manager
# https://wiki.archlinux.org/title/thunar
# https://gitlab.com/axdsop/nixnotes/blob/master/GNULinux/Distros/ArchLinux/Notes/FileManagerComparison.md
sudo pacman -S --noconfirm thunar
sudo pacman -S --noconfirm tumbler libgsf libopenraw poppler-glib
sudo pacman -S --noconfirm thunar-archive-plugin xarchiver  # xarchiver is archive manager

# Optional dependencies for thunar
#     gvfs: for trash support, mounting with udisk and remote filesystems
#     xfce4-panel: for trash applet
#     tumbler: for thumbnail previews
#     thunar-volman: manages removable devices
#     thunar-archive-plugin: create and deflate archives
#     thunar-media-tags-plugin: view/edit id3/ogg tags

# - gvfs
# The Media Transfer Protocol (MTP) can be used to transfer media files to and from many mobile phones.
# https://wiki.archlinux.org/title/Media_Transfer_Protocol#File_manager_integration
# https://wiki.archlinux.org/title/Thunar#Thunar_Volume_Manager
sudo pacman -S --noconfirm gvfs-mtp  # install 20+1 packages, download size 7.21MiB, installed size: 39.92MiB

# - Others
sudo pacman -S --noconfirm man-db
```

#### For Ubuntu

```bash
# - remove unneeded packages
sudo apt-get purge -y i3-wm i3status

# - install i3, i3lock, has no i3-gaps
sudo apt-get install -y i3 i3lock
```

Essential pakcages

```bash
# - screen brightness controls
# https://github.com/haikarainen/light It changes file /sys/class/backlight/intel_backlight/brightness which file group is 'video', login user need to add into group 'video' to make command light effect.
sudo apt-get install -y light
sudo usermod -a -G video $USER
sudo light -S 15  # Set brightness to value

# - volume control
# pactl https://wiki.archlinux.org/title/PulseAudio
sudo apt-get install -y pulseaudio pulseaudio-utils
```

Optional packages

```bash
# https://wiki.archlinux.org/title/Screen_capture
# scrot (SCReenshOT) - command line screen capture utility
sudo apt-get install -y scrot

# - image viewer
# feh — image viewer and cataloguer, change desktop background
sudo apt-get install -y feh

# - PDF viewer
sudo apt-get install -y mupdf
```


### Initialization

When [i3][i3] is first started, it offers to run the configuration wizard `i3-config-wizard`. This tool creates *~/.config/i3/config* by rewriting a template configuration file in */etc/i3/config.keycodes*. But the default config is too simple and ugly.

To initialize configuration, just save configuration files to directory *~/.config/i3/*.

Xsession file saves in directory */usr/share/xsessions/*.

If you use graphical [display manager](https://wiki.archlinux.org/title/Display_manager), you can choose [i3][i3] from selection menu.

Otherwise, you need to start [i3][i3] manually via [xinit](https://wiki.archlinux.org/title/Xinit) which configuration file is *~/.xinitrc*.

Add the following configuration in *~/.xinitrc*

```bash
exec i3
```

Then execute `startx` to run this xinitrc as a session

```bash
startx /usr/bin/i3
```


## Config Explanation

Configuration directory tree architecture

```txt
.
├── config
├── i3status.conf
└── README.md

0 directories, 3 files
```

To enable Touchpad Tap To Click

```bash
# Enable Touchpad Tap To Click
touchpad_device=${touchpad_device:-}
touchpad_device=$(xinput 2> /dev/null | sed -r -n '/touchpad/I{s@^[^[:upper:]]*(.*?)id=.*$@\1@g;s@[[:space:]]+$@@g;p}')
# exec --no-startup-id xinput set-prop "SYNA1D31:00 06CB:CD48 Touchpad" "libinput Tapping Enabled" 1
[[ -n "${touchpad_device}" ]] && sed -r -i '/xinput set-prop/{s@(.*set-prop[[:space:]]*")[^"]+(.*$)@\1'"${touchpad_device}"'\2@g;}' config
```


## XDG Utils

### MIME Default Application

```bash
# https://wiki.archlinux.org/title/Xdg-utils
# xdg-mime(1) - Query and install MIME types and associations
# xdg-open(1) - Open a file or URI in the user's preferred application
# xdg-settings(1) - Get or set the default web browser and URL handlers

# - For file manager
# https://bbs.archlinux.org/viewtopic.php?id=157033
xdg-mime query default inode/directory
xdg-mime default thunar.desktop inode/directory # for thunar
```

## Issue Occuring

### White Screen With HiDPI

For laptop with [HiDPI](https://wiki.archlinux.org/title/HiDPI) screen (3000x2000), if you only install [i3][i3] without graphical desktop environment. While you open file browser in web brower (Firefox) or text editor (VSCode), it just shows a **white screen** (actually it is a giant draggble pop window), one solution is install a desktop environment (e.g. GNOME),just like post [Can't save image from browser shows white screen](https://bbs.archlinux.org/viewtopic.php?id=222641), [Firefox and GTKFileChooser HiDPi issue on i3](https://www.reddit.com/r/archlinux/comments/clg5r0/firefox_and_gtkfilechooser_hidpi_issue_on_i3/).

It caused by HiDPI, file `~/.Xresources` set *Xft.dpi: 220*. Try to change the value to 96 (font too tiny) or 192, it will works.

[HiDPI support in Manjaro](https://forum.manjaro.org/t/hidpi-support-in-manjaro/26088) introduces HiDPI supporting in major desktop environments.


### GnuPG GUI Prompt Passphase

To make gpg prompt passphrase from cli instead of gui, try to config `pinentry`

```bash
# https://stackoverflow.com/questions/17769831/how-to-make-gpg-prompt-for-passphrase-on-cli/53641081

$ sudo update-alternatives --config pinentry

There are 2 choices for the alternative pinentry (providing /usr/bin/pinentry).

  Selection    Path                      Priority   Status
------------------------------------------------------------
* 0            /usr/bin/pinentry-gnome3   90        auto mode
  1            /usr/bin/pinentry-curses   50        manual mode
  2            /usr/bin/pinentry-gnome3   90        manual mode

Press <enter> to keep the current choice[*], or type selection number: 1
update-alternatives: using /usr/bin/pinentry-curses to provide /usr/bin/pinentry (pinentry) in manual mode
```

## Bibliography

* [Defining program colors through .Xresources](https://www.reddit.com/r/unixporn/comments/8giij5/guide_defining_program_colors_through_xresources/)


## Change Log

* Jan 03, 2022 Tue 09:30 ET
  * arch linux merge `i3-gaps` into `i3-wm`, update doc link
* Apr 23, 2020 Thu 08:35 ET
  * first draft
* Sep 19, 2020 Sat 15:15 ET
  * add support for Ubuntu
* Sep 26, 2020 Sat 19:20 ET
  * rewrite readme


[i3]:https://i3wm.org/ "i3 - improved tiling wm"
[i3-gaps]:https://github.com/Airblader/i3


<!-- End -->