# This file contains essential custom functions needed by shell script from https://gitlab.com/MaxdSre/axd-ShellScript
# Writer: MaxdSre

# Change Log:
# - Aug 14, 2019 14:40 Wed ET - add variable internet_connect_status and quiet_mode for function internet check
# - Aug 10, 2019 13:14 Sat ET - Code refactoring
# - Dec 29, 2018 21:43 Sat ET - create base file to put essential function


#########  0. Initial Variables Setting  #########
# IFS=$'\n\t' #used in loop,  Internal Field Separator

# - default file permissions (umask)
umask 022    # temporarily change umask value to 022

# - project name
project_name='axdsop'

# custom variables
base_script_path=''

phase_flag_count=1    # used for phase statement
procedure_start_time=${procedure_start_time:-}
procedure_end_time=${procedure_end_time:-}

temp_save_dir='/tmp'      # Save path of downloaded packages
readonly bak_suffix='_bak'     # suffix string for file backup
script_self_name=$(basename "$(readlink -nf "$0")")
mktemp_format="Temp-${project_name}-${script_self_name%%.*}_XXXXXXXXX"
start_time=$(date +'%s')    # Start time of operation

# - Term cols / lines
# $(setty size) # height width
# tput: No value for $TERM and no -T specified
readonly terminal_cols=$(tput cols 2> /dev/null)
readonly terminal_lines=$(tput lines 2> /dev/null)
readonly terminal_clear=$(tput clear 2> /dev/null)

# - Text color
# usage: ${c_bold}${c_red}STRING${c_normal}

# https://misc.flogisoft.com/bash/tip_colors_and_formatting
# black 0, red 1, green 2, yellow 3, blue 4, magenta 5, cyan 6, gray 7
# \e[31m need to use `echo -e`
readonly c_red="$(tput setaf 1 2> /dev/null)"     # c_red='\e[31;1m'
readonly c_green="$(tput setaf 2 2> /dev/null)"    # c_green='\e[32m'
readonly c_yellow="$(tput setaf 3 2> /dev/null)"    # c_yellow='\e[33m'
readonly c_blue="$(tput setaf 4 2> /dev/null)"    # c_blue='\e[34m'
readonly c_magenta="$(tput setaf 5 2> /dev/null)"    # c_magenta='\e[35m'
readonly c_cyan="$(tput setaf 6 2> /dev/null)"    # c_cyan='\e[36m'
readonly c_gray="$(tput setaf 7 2> /dev/null)"    # c_gray='\e[37m'
readonly c_normal="$(tput sgr0 2> /dev/null)"     # c_normal='\e[0m'
readonly c_bold="$(tput bold 2> /dev/null)"


#########  1. Base Functions  #########
trap fnBase_TrapCleanUp INT QUIT

fnBase_TempFileRemove(){
    [[ -d /tmp/ ]] && rm -rf /tmp/"${mktemp_format%_*}"* 2>/dev/null
}

fnBase_TrapCleanUp(){
    # https://en.wikipedia.org/wiki/Exception_handling
    # trap '' HUP	#overlook SIGHUP when internet interrupted or terminal shell closed
    # trap '' INT   #overlook SIGINT when enter Ctrl+C, QUIT is triggered by Ctrl+\
    fnBase_TempFileRemove
    printf "Detecting $(tput setaf 1)%s$(tput sgr0) or $(tput setaf 1)%s$(tput sgr0), begin to exit shell\n" "CTRL+C" "CTRL+\\"
    exit
}

fnBase_ExitStatement(){
    local l_str="$*"
    [[ -n "${l_str}" ]] && echo -e "${l_str}\n"
    fnBase_TempFileRemove
    exit
}

fnBase_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fnBase_CommandExistCheckPhase(){
    local l_item="${1:-}"
    local l_utility="${2:-}"

    if [[ -n "${l_item}" ]]; then
        if ! fnBase_CommandExistIfCheck "${l_item}"; then
            local l_output="${c_red}Error${c_normal}, no command ${c_yellow}${l_item}${c_normal} finds!"
            [[ -n "${l_utility}" ]] && l_output="${l_output} Please install utility ${c_yellow}${l_utility}${c_normal}."
            fnBase_ExitStatement "${l_output}"
        fi
    fi
}

fnBase_VersionNumberComparasion(){
    local l_item1="${1:-0}"    # old val
    local l_item2="${2:-0}"    # new val
    local l_operator=${3:-'<'}
    local l_output=${l_output:-0}

    if [[ -n "${l_item1}" && -n "${l_item2}" ]]; then
        local l_arr1=( ${l_item1//./ } )
        local l_arr2=( ${l_item2//./ } )
        local l_arr1_len=${l_arr1_len:-"${#l_arr1[@]}"}
        local l_arr2_len=${l_arr2_len:-"${#l_arr2[@]}"}
        local l_max_len=${l_max_len:-"${l_arr1_len}"}
        [[ "${l_arr1_len}" -lt "${l_arr2_len}" ]] && l_max_len="${l_arr2_len}"

        for (( i = 0; i < "${l_max_len}"; i++ )); do
            [[ "${l_arr1_len}" -lt $(( i+1 )) ]] && l_arr1[$i]=0
            [[ "${l_arr2_len}" -lt $(( i+1 )) ]] && l_arr2[$i]=0

            if [[ "${l_arr1[i]}" -lt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=0 ;;
                    '<'|* ) l_output=1 ;;
                esac
                break
            elif [[ "${l_arr1[i]}" -gt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=1 ;;
                    '<'|* ) l_output=0 ;;
                esac
                break
            else
                continue
            fi
        done
    fi

    echo "${l_output}"
}

fnBase_InternetConnectionCheck(){
    local l_quiet_mode_diable=${1:-1} # 1 disable quiet, 0 enable quiet
    internet_connect_status=${internet_connect_status:-0}  # 0 is fail

    # Windows Subsystem for Linux has no file 'carrier' in Ubuntu 18.04
    if [[ -d /sys/class/net/ ]]; then
        # https://stackoverflow.com/questions/929368/how-to-test-an-internet-connection-with-bash#answer-44280885
        [[ $(find /sys/class/net/ -maxdepth 1 -mindepth 1 ! -name "lo" ! -name "docker*" ! -name "tun*" -exec sh -c 'cat "$0"/carrier 2> /dev/null | sed "/^1$/!d"' {} \; 2> /dev/null | wc -l) -gt 0 ]] && internet_connect_status=1

        # https://stackoverflow.com/questions/929368/how-to-test-an-internet-connection-with-bash#answer-14939373
        # for item in $(find /sys/class/net/* -print 2> /dev/null | sed '/\/lo/d;/\/docker/d'); do
        #     interface_carrier="${item%/}/carrier"
        #     if [[ -f "${interface_carrier}" && $(cat "${interface_carrier}" 2> /dev/null) -eq 1 ]]; then
        #         internet_connect_status=1
        #         break
        #     fi
        # done
    fi

    if [[ "${internet_connect_status}" -eq 0 ]]; then
        local l_gateway_ip=${l_gateway_ip:-}
        l_gateway_ip=$(ip route 2> /dev/null | sed -n '/^default/p' | cut -d' ' -f3)
        [[ -n "${l_gateway_ip}" ]] || l_gateway_ip=$(netstat -rn 2> /dev/null | sed -r -n '/^Destination[[:space:]]*/{n;s@[[:blank:]]{2,}@|@g;p}' | cut -d\| -f2)

        if [[ -n "${l_gateway_ip}" ]]; then
            ping -q -w 1 -c 1 "$l_gateway_ip" &> /dev/null && internet_connect_status=1
        fi
    fi

    if [[ "${l_quiet_mode_diable}" -eq 1 ]]; then
        [[ "${internet_connect_status}" -ne 1 ]] && fnBase_ExitStatement "${c_red}Error${c_normal}: Internet connection test failed, please check it!"
    fi
}

fnBase_DownloadMethodCheck(){
    proxy_server_specify=${proxy_server_specify:-}
    download_method=${download_method:-}

    local l_proxy_pattern_1="^((http|https|socks4|socks5):)?[0-9]{1,5}$"
    local l_proxy_pattern_2="^((http|https|socks4|socks5):)?([0-9]{1,3}.){3}[0-9]{1,3}$"    # If the port number is not specified in the proxy string, it is assumed to be 1080.
    local l_proxy_pattern_3="^((http|https|socks4|socks5):)?([0-9]{1,3}.){3}[0-9]{1,3}:[0-9]{1,5}$"
    local l_proxy_proto_pattern="^((http|https|socks4|socks5):)"
    local l_localhost='127.0.0.1'
    local l_proxy_port_default='1080'
    local l_proxy_proto=''
    local l_proxy_host=''

    if [[ -n "${proxy_server_specify}" ]]; then
        if [[ "${proxy_server_specify}" =~ ${l_proxy_pattern_1} ]]; then
            if [[ "${proxy_server_specify}" =~ ${l_proxy_proto_pattern} ]]; then
                l_proxy_proto="${proxy_server_specify%%:*}"
                l_proxy_host="${l_localhost}:${proxy_server_specify#*:}"
            else
                l_proxy_proto='http'
                l_proxy_host="${l_localhost}:${proxy_server_specify}"
            fi
        elif [[ "${proxy_server_specify}" =~ ${l_proxy_pattern_2} ]]; then
            if [[ "${proxy_server_specify}" =~ ${l_proxy_proto_pattern} ]]; then
                l_proxy_proto="${proxy_server_specify%%:*}"
                l_proxy_host="${proxy_server_specify#*:}:${l_proxy_port_default}"
            else
                l_proxy_proto='http'
                l_proxy_host="${proxy_server_specify}:${l_proxy_port_default}"
            fi
        elif [[ "${proxy_server_specify}" =~ ${l_proxy_pattern_3} ]]; then
            if [[ "${proxy_server_specify}" =~ ${l_proxy_proto_pattern} ]]; then
                l_proxy_proto="${proxy_server_specify%%:*}"
                l_proxy_host="${proxy_server_specify#*:}"
            else
                l_proxy_proto='http'
                l_proxy_host="${proxy_server_specify}"
            fi
        else
            fnBase_ExitStatement "${c_red}Error${c_normal}: please specify right proxy host addr like ${c_blue}[protocol:][ip:]port${c_normal}!"
        fi
    fi

    local l_retry_times=${l_retry_times:-5}
    local l_retry_delay_time=${l_retry_delay_time:-1}
    local l_connect_timeout_time=${l_connect_timeout_time:-2}
    local l_referrer_page=${l_referrer_page:-"https://duckduckgo.com/?q=${project_name}"}
    # local l_referrer_page=${l_referrer_page:-'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:68.0) Gecko/20100101 Firefox/68.0'}

    if fnBase_CommandExistIfCheck 'curl'; then
        # curl -fsSL URL -o /PATH/FILE
        download_method_origin="curl -fsL"
        # --retry <num>    Added in 7.12.3.
        # --retry-delay <seconds>    Added in 7.12.3.
        # --retry-max-time <seconds>    Added in 7.12.3.
        # --connect-timeout <seconds>    Since version 7.32.0, this option accepts decimal values.
        download_method="${download_method_origin} --retry ${l_retry_times} --retry-delay ${l_retry_delay_time} --connect-timeout ${l_connect_timeout_time} --no-keepalive"
        # --referer ${l_referrer_page} /  --user-agent ${l_referrer_page}

        if [[ -n "${proxy_server_specify}" ]]; then
            local curl_version_no=${curl_version_no:-}
            curl_version_no=$(curl --version 2> /dev/null | sed -r -n '1s@^[^[:digit:]]*([[:digit:].]*).*@\1@p')

            # http_proxy [protocol://]<host>[:port]    Sets the proxy server to use for HTTP.
            # HTTPS_PROXY [protocol://]<host>[:port]    Sets the proxy server to use for HTTPS.
            # --socks5-hostname <host[:port]> / --socks5 <host[:port]>    Added in 7.18.0
            # -x, --proxy [protocol://]host[:port]    Added in curl 7.21.7
            # --preproxy [protocol://]host[:port]    Since 7.52.0,  --preproxy  can  be used to specify a SOCKS proxy at the same time -x, --proxy is used with an HTTP/HTTPS proxy. In such a case curl first connects to the  SOCKS  proxy  and  then connects (through SOCKS) to the HTTP or HTTPS proxy.

            case "${l_proxy_proto}" in
                http ) export http_proxy="${l_proxy_host}" ;;
                https ) export HTTPS_PROXY="${l_proxy_host}" ;;
                socks4 )
                    if [[ $(fnBase_VersionNumberComparasion "${curl_version_no}" '7.21.7' '>') -eq 1 ]]; then
                        download_method="${download_method} -x ${l_proxy_proto}a://${l_proxy_host}"
                    else
                        download_method="${download_method} --socks4a ${l_proxy_host}"
                    fi
                    ;;
                socks5 )
                    if [[ $(fnBase_VersionNumberComparasion "${curl_version_no}" '7.21.7' '>') -eq 1 ]]; then
                        download_method="${download_method} -x ${l_proxy_proto}h://${l_proxy_host}"
                    else
                        download_method="${download_method} --socks5-hostname ${l_proxy_host}"
                    fi
                    ;;
                * ) export http_proxy="${l_proxy_host}" ;;
            esac
        fi

    elif fnBase_CommandExistIfCheck 'wget'; then
        # wget -q URL -O /PATH/FILE
        download_method_origin="wget -qO-"
        download_method="${download_method_origin} --tries=${l_retry_times} --waitretry=${l_retry_delay_time} --connect-timeout ${l_connect_timeout_time} --no-http-keep-alive"
        # --referer=${l_referrer_page} / --user-agent=${l_referrer_page}

        # wget --version 2> /dev/null | sed -r -n '1s@^[^[:digit:]]+([[:digit:].]+).*@\1@p'
        if [[ -n "${proxy_server_specify}" ]]; then
            if [[ "${l_proxy_proto}" == 'https' ]]; then
                export https_proxy="${l_proxy_host}"
            else
                export http_proxy="${l_proxy_host}"
            fi
        fi
    else
        fnBase_ExitStatement "${c_red}Error${c_normal}: no command ${c_blue}curl${c_normal} or ${c_blue}wget${c_normal} find!"
    fi
}


#########  2. OS Info Functions  #########
# - GNU/Linux OS Info
fnBase_OSInfoRetrieve(){
    # os check script path
    local l_script_path="${1:-}"
    local l_compress_action="${2:-1}"
    local l_is_compress=0
    local l_output=${l_output:-}  # json format

    if [[ "${l_compress_action}" -eq 1 ]]; then
        if fnBase_CommandExistIfCheck 'gzip' && fnBase_CommandExistIfCheck 'openssl'; then
            l_is_compress=1
        fi
    fi

    if [[ "${l_is_compress}" -eq 1 ]]; then
        local l_encryption_pass=${l_encryption_pass:-"$(openssl rand -base64 $(( RANDOM%64 + 32 )) | head -n 1)"}
        l_output=$($download_method "${l_script_path}" | bash -s -- -j -E "${l_encryption_pass}" 2> /dev/null | openssl enc -aes-256-cbc -d -a -A -salt -pass pass:"${l_encryption_pass}" 2> /dev/null | gzip -d 2> /dev/null)
    else
        l_output=$($download_method "${l_script_path}" | bash -s -- -j 2> /dev/null)
    fi

    if [[ -z "${l_output}" ]]; then
        # fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"
        l_output=''
    elif [[ $(echo "${l_output}" | sed -r -n 's@.*"status":\{([^}]+).*@\1@g;s@.*flag":"([^"]+).*@\1@g;p') == 'fail' ]]; then
        # fnBase_ExitStatement "${c_red}Fatal${c_normal}: this script doesn't support your system!"
        l_output=''
    else
        # extract system info
        l_output=$(echo "${l_output}" | sed -r -n 's@.*"system":\{([^\{]*)\}.*$@\1@g;s@","@\n@g;s@":"@|@g;s@(^"|"$)@@g;p')
    fi

    echo "${l_output}"
}

fnBase_OSInfoDirectiveValExtraction(){
    # distro_fullname=$(fnBase_OSInfoRetrieve 'distro_fullname' "${l_output}")
    local l_item="${1:-}"
    local l_source="${2:-}"
    local l_output=''
    [[ -n "${l_item}" && -n "${l_source}" ]] && l_output=$(echo "${l_source}" | sed -r -n '/^'"${l_item}"'\|/{s@^[^|]+\|(.*)$@\1@g;p}')
    echo "${l_output}"
}


#########  2.1 Service Management  #########
# - System Service Management
fnBase_SystemServiceManagement(){
    # systemctl / service & chkconfig
    local l_service_name="${1:-}"
    local l_action="${2:-}"
    [[ -n "${l_action}" ]] && l_action="${l_action,,}"

    if fnBase_CommandExistIfCheck 'systemctl'; then
        case "${l_action}" in
            start|stop|reload|restart|status|enable|disable )
                systemctl unmask "${l_service_name}" &> /dev/null
                systemctl daemon-reload "${l_service_name}" &> /dev/null

                case "${l_action}" in
                    enable )
                        systemctl enable "${l_service_name}" &> /dev/null
                        l_action='start'
                        ;;
                    disable )
                        systemctl disable "${l_service_name}" &> /dev/null
                        l_action='stop'
                        ;;
                esac

                systemctl "${l_action}" "${l_service_name}" &> /dev/null
                ;;
            * ) systemctl status "${l_service_name}" &> /dev/null ;;
        esac

    elif fnBase_CommandExistIfCheck 'service'; then
        case "${l_action}" in
            start|stop|restart|status|enable|disable )
                if fnBase_CommandExistIfCheck 'chkconfig'; then
                    local sysv_command='chkconfig'  # for RedHat/OpenSUSE
                elif fnBase_CommandExistIfCheck 'sysv-rc-conf'; then
                    local sysv_command='sysv-rc-conf'   # for Debian
                fi

                case "${l_action}" in
                    enable )
                        $sysv_command "${l_service_name}" on &> /dev/null
                        l_action='start'
                        ;;
                    disable )
                        $sysv_command "${l_service_name}" off &> /dev/null
                        l_action='stop'
                        ;;
                esac

                service "${l_service_name}" "${l_action}" &> /dev/null
                ;;
            * ) service "${l_service_name}" status &> /dev/null ;;
        esac
    fi
}

# - SELinux Operation
fnBase_SELinuxSemanageOperation(){
    # selinux type / selinux boolean
    local l_item="${1:-}"
    # port num / on/off
    local l_val="${2:-}"
    # port/boolean/fcontext
    local l_type="${3:-'port'}"
    # add -a/delete -d/modify -m for port
    local l_action="${4:-'add'}"
    # tcp/udp
    local l_protocol="${5:-'tcp'}"

    # semanage fcontext -l | grep ssh_home_t
    # semanage fcontext -a -t ssh_home_t "${login_user_home}/.ssh/"
    # restorecon -v "${login_user_home}/.ssh/"

    # semanage boolean -l | grep ssh
    # getsebool ssh_keysign
    # setsebool ssh_keysign on      #temporarily modify until reboot
    # setsebool -P ssh_keysign on   # persist modify

    # semanage port -l | grep ssh
    # semanage port -a -t ssh_port_t -p tcp 22

    if [[ -n "${l_item}" && -n "${l_val}" ]]; then
        case "${l_type,,}" in
            fcontext|f )
                if fnBase_CommandExistIfCheck 'semanage'; then
                    case "${l_action,,}" in
                        add|a ) l_action='--add' ;;
                        delete|d) l_action='--delete' ;;
                        modify|m) l_action='--modify' ;;
                    esac
                    l_val="${l_val%/}"
                    semanage fcontext ${l_action} -t "${l_item}" "${l_val}(/.*)?" 2> /dev/null
                    fnBase_CommandExistIfCheck 'restorecon' &&  restorecon -F -R "${l_val}" 2> /dev/null
                fi
                ;;
            boolean|b )
                if fnBase_CommandExistIfCheck 'setsebool'; then
                    [[ "${l_val}" != 'on' ]] && l_val='off'
                    setsebool "${l_item}" "${l_val}" 2> /dev/null
                    setsebool -P "${l_item}" "${l_val}" 2> /dev/null
                fi
                ;;
            port|p )
                if fnBase_CommandExistIfCheck 'semanage'; then
                    case "${l_action,,}" in
                        add|a ) l_action='--add' ;;
                        delete|d) l_action='--delete' ;;
                        modify|m) l_action='--modify' ;;
                    esac

                    case "${l_protocol,,}" in
                        tcp ) l_protocol='tcp' ;;
                        udp ) l_protocol='udp' ;;
                    esac
                    semanage port "${l_action}" -t "${l_item}" -p "${l_protocol}" "${l_val}" 2> /dev/null
                fi
                ;;
        esac
    fi
}


#########  2.2 Package Management  #########
# - Package managerment
# variable `pack_manager` from fnBase_OSInfoRetrieve
fnBase_PackageManagerOperation(){
    local l_action="${1:-'update'}"
    local l_package_lists=(${2:-})

    case "${pack_manager}" in
        apt-get )
            # E: dpkg was interrupted, you must manually run 'dpkg --configure -a' to correct the problem.
            # dpkg --configure -a

            # disable dialog prompt
            export DEBIAN_FRONTEND=noninteractive
            # export DEBIAN_PRIORITY=low

            # E: dpkg was interrupted, you must manually run 'sudo dpkg --configure -a' to correct the problem.
            fnBase_CommandExistIfCheck 'dpkg' && dpkg --configure -a &> /dev/null

            # apt-get [options] command
            case "${l_action}" in
                install|in )
                    apt-get -yq --no-install-recommends install "${l_package_lists[@]}" &> /dev/null
                    apt-get -yq -f install &> /dev/null
                    ;;
                remove|rm )
                    apt-get -yq purge "${l_package_lists[@]}" &> /dev/null
                    apt-get -yq autoremove 1> /dev/null
                    ;;
                upgrade|up )
                    # https://askubuntu.com/questions/165676/how-do-i-fix-a-e-the-method-driver-usr-lib-apt-methods-http-could-not-be-foun#211531
                    # https://github.com/koalaman/shellcheck/wiki/SC2143
                    if ! dpkg --list 2> /dev/null | grep -q 'apt-transport-https'; then
                        apt-get -yq install apt-transport-https &> /dev/null
                    fi

                    apt-get -yq clean all 1> /dev/null
                    apt-get -yq update 1> /dev/null
                    apt-get -yq --fix-broken install 1> /dev/null

                    if [[ "${#l_package_lists[@]}" -eq 0 ]]; then
                        apt-get -yq upgrade &> /dev/null
                        apt-get -yq dist-upgrade &> /dev/null
                    else
                        apt-get -yq install --only-upgrade "${l_package_lists[@]}" &> /dev/null
                    fi
                    apt-get -yq autoremove 1> /dev/null
                    ;;
                * )
                    apt-get -yq clean all 1> /dev/null
                    apt-get -yq update 1> /dev/null
                    ;;
            esac

            unset DEBIAN_FRONTEND
            ;;
        dnf )
            # dnf [options] COMMAND
            case "${l_action}" in
                install|in )
                    dnf -yq install "${l_package_lists[@]}" &> /dev/null
                    ;;
                remove|rm )
                    dnf -yq remove "${l_package_lists[@]}" &> /dev/null
                    dnf -yq autoremove 2> /dev/null
                    ;;
                upgrade|update|up )
                    dnf -yq makecache &> /dev/null

                    if [[ "${#l_package_lists[@]}" -eq 0 ]]; then
                        dnf -yq upgrade &> /dev/null    #dnf has no command update
                    else
                        dnf -yq upgrade "${l_package_lists[@]}" &> /dev/null
                    fi

                    dnf -yq autoremove 2> /dev/null
                    ;;
                * )
                    dnf -yq clean all &> /dev/null
                    dnf -yq makecache &> /dev/null
                    ;;
            esac
            ;;
        yum )
            fnBase_CommandExistIfCheck 'yum-complete-transaction' && yum-complete-transaction --cleanup-only &> /dev/null
            # yum [options] COMMAND
            case "${l_action}" in
                install|in )
                    yum -y -q install "${l_package_lists[@]}" &> /dev/null
                    ;;
                remove|rm )
                    yum -y -q erase "${l_package_lists[@]}" &> /dev/null
                    # yum -y -q remove "${l_package_lists[@]}" &> /dev/null
                    yum -y -q autoremove &> /dev/null
                    ;;
                upgrade|up )
                    yum -y -q makecache fast &> /dev/null
                    # https://www.blackmoreops.com/2014/12/01/fixing-there-are-unfinished-transactions-remaining-you-might-consider-running-yum-complete-transaction-first-to-finish-them-in-centos/
                    fnBase_CommandExistIfCheck 'yum-complete-transaction' || yum -y -q install yum-utils &> /dev/null

                    if [[ "${#l_package_lists[@]}" -eq 0 ]]; then
                        yum -y -q update &> /dev/null
                        yum -y -q upgrade &> /dev/null
                    else
                        yum -y -q update "${l_package_lists[@]}" &> /dev/null
                    fi
                    ;;
                * )
                    yum -y -q clean all &> /dev/null
                    yum -y -q makecache fast &> /dev/null
                    ;;
            esac
            ;;
        zypper )
            # zypper [--global-opts] command [--command-opts] [command-arguments]
            case "${l_action}" in
                install|in )
                    zypper in --no-recommends -yl "${l_package_lists[@]}" &> /dev/null
                    ;;
                remove|rm )
                    zypper rm -yu "${l_package_lists[@]}" &> /dev/null
                    # remove unneeded packages & dependencies
                    # zypper packages --unneeded | awk -F\| 'match($1,/^i/){print $3}' | xargs zypper rm -yu &> /dev/null
                    zypper packages --unneeded | sed -r -n '/^i[[:space:]]+\|/{s@[[:space:]]+\|[[:space:]]+@|@g;p}' | cut -d\| -f3 | xargs zypper rm -yu &> /dev/null
                    ;;
                upgrade|up )
                    zypper clean -a 1> /dev/null
                    zypper ref -f &> /dev/null

                    if [[ "${#l_package_lists[@]}" -eq 0 ]]; then
                        zypper up -yl 1> /dev/null
                        zypper dup -yl 1> /dev/null
                        zypper patch -yl 1> /dev/null
                    else
                        zypper up -yl "${l_package_lists[@]}" &> /dev/null
                    fi
                    ;;
                * )
                    zypper clean -a 1> /dev/null
                    zypper ref -f &> /dev/null
                    ;;
            esac
            ;;
        pacman )
            # https://wiki.archlinux.org/index.php/Pacman
            # https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks
            # pacman <operation> [options] [targets]
            case "${l_action}" in
                install|in )
                    # -S, --sync    Synchronize packages. Packages are installed directly from the remote repositories, including all dependencies required to run the packages.
                    pacman -S --noconfirm "${l_package_lists[@]}" &> /dev/null

                    # For package group
                    # https://wiki.archlinux.org/index.php/Package_group
                    # pacman -Sg gnome | cut -d' ' -f2 | xargs pacman -S --noconfirm
                    ;;
                remove|rm )
                    # -R, --remove    Remove package(s) from the system.
                    # -s, --recursive    Remove each target specified including all of their dependencies
                    # -n, --nosave    Instructs pacman to ignore file backup designations.
                    # remove a package and its dependencies which are not required by any other installed package
                    pacman -Rsn --noconfirm "${l_package_lists[@]}" &> /dev/null
                    ;;
                upgrade|up )
                    if [[ "${#l_package_lists[@]}" -eq 0 ]]; then
                        pacman -Syyu --noconfirm
                    else
                        pacman -Syu --noconfirm "${l_package_lists[@]}" &> /dev/null
                    fi
                    ;;
                * )
                    # -Qdt    list all packages no longer required as dependencies (orphans)
                    # -q, --quiet    Show less information for certain sync operations.
                    pacman -Qtdq | xargs sudo pacman -Rns --noconfirm &> /dev/null

                    # Cache directory: /var/cache/pacman/pkg/
                    # Database directory: /var/lib/pacman/
                    pacman -Scc --noconfirm &> /dev/null
                    ;;
            esac
            ;;
    esac
}


#########  3. Tool Functions  #########
# - Strong Random Password Generation
fnBase_RandomPasswordGeneration(){
    # funcation LENGTH TYPE
    local l_len=${1:-36}
    local l_type=${2:-0}
    local l_tr_pattern='a-zA-Z0-9!?@#()&$%{}<>^_+'
    local l_new_pass=''

    # https://dev.mysql.com/doc/refman/8.0/en/validate-password.html
    # https://www.howtogeek.com/howto/30184/10-ways-to-generate-a-random-password-from-the-command-line/
    # https://serverfault.com/questions/261086/creating-random-password
    # https://unix.stackexchange.com/questions/462/how-to-create-strong-passwords-in-linux

    [[ "${l_type}" -eq 0 ]] && l_tr_pattern='a-zA-Z0-9@#_+'

    # https://www.gnupg.org/gph/en/manual/r1114.html
    # gpg --gen-random --armor 2 "${l_len}" 2> /dev/null | head -c "${l_len}"

    # head -n $(( RANDOM%999 + 100 )) /dev/urandom | tr -dc 'a-zA-Z0-9@#_+' | fold -w $(( RANDOM%99 + 50)) | head -n $(( RANDOM%300 + 200 )) | openssl dgst -sha512 -binary | openssl enc -a -A -salt -S $(( RANDOM%2000 + 200 )) | head -c "${l_len}" | xargs

    l_new_pass=$(head -n $(( RANDOM%999 + 100 )) /dev/urandom | tr -dc "${l_tr_pattern}" | fold -w $(( RANDOM%199 + 99 )) | head -c "${l_len}" | xargs)
    l_new_pass="${l_new_pass// /}" # sed -r 's@[[:blank:]]*@@g'

    if [[ "${#l_new_pass}" -eq "${l_len}" && "${l_new_pass}" =~ ^[1-9a-zA-Z] && "${l_new_pass}" =~ [1-9a-zA-Z]$ ]]; then
        echo "${l_new_pass}"
    else
        fnBase_RandomPasswordGeneration "${l_len}" "${l_type}"
    fi
}

# - Hash Digest Verification
fnBase_HashDigestVerification(){
    local l_file_path="${1:-}"
    local l_hash_type="${2:-}"  # default is sha256
    local l_output=''

    if [[ -f "${l_file_path}" ]]; then
        if [[ -n "${l_hash_type}" ]]; then
            l_hash_type="${l_hash_type,,}"

            l_hash_type="${l_hash_type#sha}"
            l_hash_type="${l_hash_type%sum}"
            # l_hash_type=$(echo "${l_hash_type}" | sed 's@^sha@@g;s@sum$@@g')
        else
            l_hash_type='256'
        fi

        local l_openssl_exist=${l_openssl_exist:-0}
        fnBase_CommandExistIfCheck 'openssl' && l_openssl_exist=1
        local l_shasum_exist=${l_shasum_exist:-0}
        fnBase_CommandExistIfCheck 'shasum' && l_shasum_exist=1

        # shasum -a, --algorithm   1 (default), 224, 256, 384, 512, 512224, 512256
        case "${l_hash_type}" in
            md5 )
                fnBase_CommandExistIfCheck 'md5sum' && l_output=$(md5sum "${l_file_path}" 2>/dev/null | cut -d' ' -f1)
                # b4af5730763faec8ec7e2445d0e577ba  /tmp/test
                # sed -r 's@^[[:space:]]*([^[:space:]]+).*$@\1@g'
                [[ -z "${l_output}" && "${l_openssl_exist}" -eq 1 ]] && l_output=$(openssl dgst -md5 "${l_file_path}" | cut -d' ' -f2)
                # MD5(/tmp/test)= b4af5730763faec8ec7e2445d0e577ba
                # sed -r 's@^[^=]*=[[:space:]]*@@g'
                ;;
            512224|512256 )
                [[ "${l_shasum_exist}" -eq 1 ]] && l_output=$(shasum -a "${l_hash_type}" "${l_file_path}" 2>/dev/null | cut -d' ' -f1)
                # 38811d3fb1acaaae11f6d878fac48ff75c627d4fc9480000fdfc5639  /tmp/test
                # sed -r 's@^[[:space:]]*([^[:space:]]+).*$@\1@g'
                ;;
            1|224|256|384|512 )
                [[ "${l_shasum_exist}" -eq 1 ]] && l_output=$(shasum -a "${l_hash_type}" "${l_file_path}" 2>/dev/null | cut -d' ' -f1)
                # 820f8357fbae0679fe7dbc66a6b89ba810dd86263011bd147949ce964a74d234  /tmp/test
                # sed -r 's@^[[:space:]]*([^[:space:]]+).*$@\1@g'

                [[ -z "${l_output}" && "${l_openssl_exist}" -eq 1 ]] && l_output=$(openssl dgst -sha"${l_hash_type}" "${l_file_path}" | cut -d' ' -f2)
                # SHA256(/tmp/test)= 820f8357fbae0679fe7dbc66a6b89ba810dd86263011bd147949ce964a74d234
                # sed -r 's@^[^=]*=[[:space:]]*@@g'
                ;;
        esac
    fi

    # remove white space
    [[ -n "${l_output}" ]] && l_output="${l_output// /}"
    echo "${l_output}"
}

# - GPG Signature Verification
fnBase_GPGSignatureVerification(){
    # https://gitlab.com/axdsop/nixnotes/tree/master/CyberSecurity/GnuPG#package-signature-verifying

    # Attention: if GPG key is included in sig file, then package file and sig file must in same dir, because gpg assumes package file (signed data) is saved in the same path. Otherwise it will prompt error info "gpg: no signed data", "gpg: can't hash datafile: No data" while import key

    local l_file_path="${1:-}"
    local l_sig_path="${2:-}"
    local l_gpg_path="${3:-}"
    local l_download_tool='wget -qO-'
    [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsSL'


    if [[ -n "${l_file_path}" && -f "${l_file_path}" ]]; then
        # echo -e "File: \e[33m${l_file_path}\e[0m\nSig: \e[33m${l_sig_path}\e[0m\n"
        local l_gpg_temp_save_dir=$(mktemp -d -t XXXXXXXX)
        local l_gpg_import_path="${l_gpg_temp_save_dir}/gpg_import.gpg"
        local l_file_dir="$(dirname "${l_file_path}")"
        local l_sig_need_remove=${l_sig_need_remove:-0}

        echo -e "\e[33mImporting PGP key\e[0m"

        if [[ -n "${l_gpg_path}" ]]; then
            local l_gpg_need_remove=${l_gpg_need_remove:-0}

            if [[ "${l_gpg_path}" =~ ^https?:// ]]; then
                local l_gpg_path_temp="${l_gpg_temp_save_dir}/keyring.gpg"
                $l_download_tool "${l_gpg_path}" > "${l_gpg_path_temp}"
                l_gpg_path="${l_gpg_path_temp}"
                l_gpg_need_remove=1
            fi
            # import gpg key
            gpg --no-default-keyring --keyring "${l_gpg_import_path}" --import "${l_gpg_path}"
             # 2> /dev/null

            [[ "${l_gpg_need_remove}" -eq 1 && -f "${l_gpg_path}" ]] && rm -f "${l_gpg_path}"

            if [[ "${l_sig_path}" =~ ^https?:// ]]; then
                local l_sig_path_temp="${l_gpg_temp_save_dir}/${l_sig_path##*/}"
                # local l_sig_path_temp="${l_file_dir}/${l_sig_path##*/}"
                $l_download_tool "${l_sig_path}" > "${l_sig_path_temp}"
                l_sig_path="${l_sig_path_temp}"
                l_sig_need_remove=1
            fi

        else
            # gpg key included in .sig file
            if [[ "${l_sig_path}" =~ ^https?:// ]]; then
                # keep .sig and package file (signed data) in same dir
                # local l_sig_path_temp="${l_gpg_temp_save_dir}/${l_sig_path##*/}"
                local l_sig_path_temp="${l_file_dir}/${l_sig_path##*/}"
                $l_download_tool "${l_sig_path}" > "${l_sig_path_temp}"
                l_sig_path="${l_sig_path_temp}"
                l_sig_need_remove=1
            fi
            # import gpg key
            # gpg: assuming signed data in '/tmp/gnupg-2.2.17.tar.bz2'
            gpg --no-default-keyring --keyring "${l_gpg_import_path}" --keyserver-options auto-key-retrieve --verify "${l_sig_path}"
        fi

        echo -e "\n\e[33mVerifying signature\e[0m"
        gpg --no-default-keyring --keyring "${l_gpg_import_path}" --verify "${l_sig_path}" "${l_file_path}"
         # 2> /dev/null | sed -r -n '/Good signature/{p}'

        [[ "${l_sig_need_remove}" -eq 1 && -f "${l_sig_path}" ]] && rm -f "${l_sig_path}"

        if [[ -d "${l_gpg_temp_save_dir}" ]]; then
            rm -rf "${l_gpg_temp_save_dir}"
            echo -e "\nTemporary directory \e[33m${l_gpg_temp_save_dir}\e[0m has been removed."
        fi
    else
        echo "Usage: GPGSignatureVerification 'PACK' 'PACK.sig' ['GPG KEY']"
    fi
}
# 2>&1 | sed -r -n '/Verifying signature/,${/Good signature/{p}}'


#########  4. Notification Functions  #########
# - Notify Send
fnBase_NotifySendStatement(){
    # apt-get install libnotify-bin
    # yum/dnf install libnotify
    # pacman -S libnotify
    # https://wiki.archlinux.org/index.php/Desktop_notifications
    local l_summary=${1:-}
    local l_body=${2:-}
    local l_expire_time=${3:-'2000'}
    local l_urgency_level=${4:-'normal'}

    if fnBase_CommandExistIfCheck 'notify-send' && [[ -n "${l_body}" ]]; then
        notify-send -t "${l_expire_time}" -u "${l_urgency_level}" "${l_summary}" "${l_body}"
        # sudo -u "${login_user}" DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u ${login_user})/bus notify-send -t "${l_expire_time}" -u "${l_urgency_level}" "${l_summary}" "${l_body}"
    fi
}

# - Output Format
fnBase_CentralOutput(){
    local l_item="${1:-}"
    local l_val="${2:-}"

    if [[ -n "${l_item}" ]]; then
        local l_item_width
        l_item_width="${#l_item}"

        local l_left_space
        l_left_space=$(( (terminal_cols - l_item_width) / 2 ))
        local l_output
        l_output=$((l_left_space + l_item_width))

        # '#' stantd for title line
        if [[ "${l_val}" == '#' ]]; then
            printf "%${l_output}s\n" "${l_item}"
        else
            printf "%$((l_output - l_item_width / 2))s ${c_bold}${c_red}%s${c_normal}\n" "${l_item}:" "${l_val}"
        fi
    fi
}

fnBase_CentralOutputTitle(){
    local l_item="${1:-}"
    local l_level="${2:-0}"

    echo ''
    [[ "${l_level}" -eq 0 ]] && fnBase_CentralOutput '==========================================' '#'
    if [[ -n "${l_item}" ]]; then
        fnBase_CentralOutput "${l_item}" '#'
        fnBase_CentralOutput '==========================================' '#'
    fi
    echo ''
}

fnBase_OutputControl(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    [[ -n "${l_item}" && -n "${l_val}" ]] && fnBase_CentralOutput "${l_item}" "${l_val}"
}

# - Operation Phase Statement
fnBase_OperationPhaseStatement(){
    local l_phase="${1:-}"
    [[ -n "${l_phase}" ]] && printf "\n${c_blue}Phase ${phase_flag_count}${c_normal} - ${c_bold}${c_blue}%s${c_normal}\n" "${l_phase}"
    (( phase_flag_count++ ))  # let phase_flag_count++ / phase_flag_count=$((phase_flag_count+1))
}

fnBase_OperationProcedureStatement(){
    local l_item="${1:-}"
    if [[ -n "${l_item}" ]]; then
        [[ -n "${procedure_start_time}" ]] || procedure_start_time=$(date +'%s')
        echo -n -e " ${c_yellow}procedure${c_normal} - ${l_item} ...... "
    fi
}

fnBase_OperationProcedureResult(){
    local l_str="${1:-}"
    local l_item="${2:-0}"
    procedure_end_time=$(date +'%s')
    local l_time_cost=$((procedure_end_time-procedure_start_time))
    procedure_start_time="${procedure_end_time}"

    case "${l_item,,}" in
        0|'ok' ) l_item="${c_green}ok${c_normal}" ;;
        1|'fail'|* ) l_item="${c_red}fail${c_normal}" ;;
    esac

    if [[ -n "${l_str}" ]]; then
        echo -n -e "[${l_item}] (${c_red}${l_str}${c_normal})"
    else
        echo -n -e "[${l_item}]"
    fi

    echo " [${c_yellow}${l_time_cost}${c_normal}s]"
}


#########  6. Unset Variables  #########
# - unset variables
fnBase_UnsetVars(){
    [[ -d "${temp_save_dir}" ]] && rm -rf "${temp_save_dir}/${mktemp_format%%_*}"* 2>/dev/null
    unset temp_save_dir
    unset script_self_name
    unset mktemp_format
    unset phase_flag_count
    unset procedure_start_time
    unset procedure_end_time
    unset start_time
    unset finish_time
    unset total_time_cost
    unset download_method
}


#########  6. Running Environment Check  #########
fnBase_RunningEnvironmentCheck(){
    local l_need_sudo="${1:-1}"
    local l_need_internet_check="${2:-1}"

    # 1 - bash version check  ${BASH_VERSINFO[@]} ${BASH_VERSION}
    # bash --version | sed -r -n '1s@[^[:digit:]]*([[:digit:].]*).*@\1@p'
    [[ "${BASH_VERSINFO[0]}" -lt 4 ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: this script need BASH version 4+, your current version is ${c_blue}${BASH_VERSION%%-*}${c_normal}."

    # 2 - OS support check
    [[ -s /etc/os-release || -s /etc/SuSE-release || -s /etc/redhat-release || -s /etc/arch-release || (-s /etc/debian_version && -s /etc/issue.net) ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: this script doesn't support your system!"

    # 3 - Check root or sudo privilege
    [[ "${l_need_sudo}" -eq 1 && "$UID" -ne 0 ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: this script requires superuser privileges (eg. root, su)."

    # - Login user info
    # $EUID is 0 if run as root, default is user id, use command `who` to detect login user name
    # $USER exist && $SUDO_USER not exist, then use $USER
    [[ -n "${USER:-}" && -z "${SUDO_USER:-}" ]] && login_user="${USER:-}" || login_user="${SUDO_USER:-}"
    [[ -z "${login_user}" ]] && login_user=$(logname 2> /dev/null)
    # [[ -z "${login_user}" ]] && login_user=$(who 2> /dev/null | cut -d' ' -f 1)
    # not work properly while using sudo
    [[ -z "${login_user}" ]] && login_user=$(id -u -n 2> /dev/null)
    [[ -z "${login_user}" ]] && login_user=$(ps -eo 'uname,comm,cmd' | sed -r -n '/xinit[[:space:]]+\/(root|home)\//{s@^([^[:space:]]+).*@\1@g;p;q}')

    # awk -F: 'match($1,/^'"${login_user}"'$/){print $6}' /etc/passwd
    # sed -n '/^'"${login_user}"':/{p}' /etc/passwd | cut -d: -f6
    [[ "${login_user}" == 'root' ]] && login_user_home='/root' || login_user_home="/home/${login_user}"
    login_user_home="${login_user_home%/}"

    login_user_download_dir=${login_user_download_dir:-"${login_user_home}/Downloads/"}

    user_download_dir=${user_download_dir:-"${login_user_home}/Downloads/"}

    [[ "${l_need_internet_check}" -eq 1 ]] && fnBase_InternetConnectionCheck
    fnBase_DownloadMethodCheck
    # tput clear  # Echo the clear-screen sequence for the current terminal.
}


# Base Script End
