#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Project Site: https://github.com/MaxdSre/axd-ShellScript
# Target: AxdSop Boost Tool Configuration On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 Thu 05:36 ET - boost installation function rewrite, ranger script scope.sh need execute permission
# - Jun 02, 2020 Tue 10:50 ET - add LS_COLORS configuration
# - May 15, 2020 Fri 22:51 ET - add parameter -n to manually specify user name to save boost tool
# - Oct 29, 2019 Tue 23:27 ET - add distro_family_own detection
# - Oct 16, 2019 Wed 08:59 ET - fix bashrc optimization bug
# - Oct 15, 2019 Tue 20:55 ET - fix software list extraction and installed software update operation
# - Oct 15, 2019 Tue 12:58 ET - change custom script url
# - Jan 01, 2019 20:22 Tue ET - code reconfiguration, include base funcions from other file
# - Nov 20, 2018 12:53 Tue ET - fix available software list
# - Jul 24, 2018 10:11 Tue ET - add update all installed software
# - Jul 10, 2018 10:46 Tue ET - add software type choose prompt for choose_software_script
# - May 09, 2018 08:44 Wed ET - add official repository download
# - Mar 18, 2018 16:15 Sun ET


#########  0-1. Singal Setting  #########
trap funcTrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly repository_name='axdsop/nixnotes'
readonly custom_shellscript_url="https://gitlab.com/${repository_name}/raw/master"
readonly os_check_script="${custom_shellscript_url}/ShellScripts/Toolkits/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly login_bashrc_config="${custom_shellscript_url}/GNULinux/Dotfiles/bashrc"
readonly repo_readme_page="${custom_shellscript_url}/ShellScripts/Toolkits/Application/README.md"

os_detect=${os_detect:-0}
install_boost_tool=${install_boost_tool:-0}
username_specify=${username_specify:-}
documents_download=${documents_download:-0}
list_available_software=${list_available_software:-0}
choose_software_script=${choose_software_script:-0}
update_installed_software_script=${update_installed_software_script:-0}
system_postinstallation=${system_postinstallation:-0}
repository_download=${repository_download:-0}
proxy_server_specify=${proxy_server_specify:-}
timestamp_diff_threshold=${timestamp_diff_threshold:-900} # seconds


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | [sudo] bash -s -- [options] ...

Project Site:
- https://gitlab.com/axdsop/nix-dotfiles
- https://gitlab.com/axdsop/nixnotes

Installation & Configuring AxdSop Boost Tool On GNU/Linux!

Support distros: RHEL/CentOS/Fedora/Amazon Linux/Debian/Ubuntu/SLES/OpenSUSE/Arch Linux.

This script requires superuser privileges (eg. root, su) if set '-u'.

[available option]
    -h    --help, show help info
    -o    --os info, detect os distribution info
    -i    --install/overwrite axdsop boost tool into file ~/.bashrc
    -n username    --manually specify user name to save boost tool, along with -i
    -d    --prompt usage of official documents downloading
    -l    --list available software installation script name
    -c    --prompt usage of script which is listed in '-l'
    -u    --update all installed software script under dir /opt
    -s    --prompt usage of system post-installation configuration script
    -r    --download repository ${repository_name}
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hoin:dlcurp:" option "$@"; do
    case "$option" in
        o ) os_detect=1 ;;
        i ) install_boost_tool=1 ;;
        n ) username_specify="$OPTARG" ;;
        d ) documents_download=1 ;;
        l ) list_available_software=1 ;;
        c ) choose_software_script=1 ;;
        u ) update_installed_software_script=1 ;;
        # s ) system_postinstallation=1 ;;
        r ) repository_download=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_CommandCheck(){ local l_name="${1:-}"; local l_output=${l_output:-1}; [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0; return "${l_output}"; }  # $? -- 0 is find, 1 is not find

fn_InitializationCheck(){
    # download method detection
    download_tool=${download_tool:-'wget -qO-'}
    fn_CommandCheck 'curl' && download_tool='curl -fsL'

    if [[ "${os_detect}" -eq 1 ]]; then
        ${download_tool} "${os_check_script}" | bash -s --
        exit
    fi

    # - user specify
    user_specify="${USER:-}"
    [[ -n "${username_specify}" ]] && user_specify="${username_specify}"
    user_specify_home=${user_specify_home:-"/home/${user_specify}"}
    [[ -d "${user_specify_home}" ]] || user_specify_home=$(sed -r -n '/^'"${user_specify}"':/{p}' /etc/passwd | cut -d: -f6)

    user_codebase_dir=${user_codebase_dir:-"${user_specify_home}/.config/axdsop"}
    [[ -d "${user_codebase_dir}" ]] || mkdir -p "${user_codebase_dir}"

    sudo_operation=${sudo_operation:-'sudo '}
    [[ "${user_specify}" == 'root' ]] && sudo_operation=''

    # - base function
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"

        # tput clear  # Echo the clear-screen sequence for the current terminal.
        # doesn't need to check internet connection status forcelly
        fnBase_RunningEnvironmentCheck '0'
    else
        echo "Sorry, fail to get base script path."
        exit
    fi
}


#########  2-1 Post-installation Configuration Script  #########
# fn_SystemPostinstallation(){
#     local l_script_path=${l_script_path:-"${custom_shellscript_url}/ShellScripts/Toolkits/GNULinux/gnuLinuxPostInstallationConfiguration.sh"}

#     printf "\nSystem Post-installation Configuratoion Command: \n\n${c_yellow}%s${c_normal}\n\n" "${download_method_origin} ${l_script_path} | ${sudo_operation}bash -s -- -h"
#     printf "\nUsage Sample: \n\n${c_yellow}%s${c_normal}\n\n" "${download_method_origin} ${l_script_path} | ${sudo_operation}bash -s -- -a -k -r -g 1 -Z 2 -b -A"
# }


#########  2-2 Official Documents Download  #########
fn_OfficialDocumentsDownload(){
    local l_script_path=${l_script_path:-"${custom_shellscript_url}/ShellScripts/Toolkits/GNULinux/gnuLinuxOfficialDocumentationDownload.sh"}

    printf "\nGNU/Linux Official Documents Downloading Command is: \n\n${c_yellow}%s${c_normal}\n\n" "${download_method_origin} ${l_script_path} | bash -s -- -h"
}


#########  2-3 Initialization Prepatation  #########
fn_DistroReleaseInfoDetection(){
    local distro_release_file=${distro_release_file:-}
    local distro_fullname=${distro_fullname:-}
    # local distro_official_site=${distro_official_site:-}
    local distro_name=${distro_name:-}
    local distro_version_id=${distro_version_id:-}
    local distro_codename=${distro_codename:-}
    local distro_family_own=${distro_family_own:-}
    local distro_pack_manager=${distro_pack_manager:-}

    # CentOS 5, CentOS 6, Debian 6 has no file /etc/os-release
    if [[ -s '/etc/os-release' ]]; then
        distro_release_file='/etc/os-release'
        local l_distro_release_info
        l_distro_release_info=$(sed -r -n 's@=@|@g;s@"@@g;/^$/d;p' "${distro_release_file}")

        #distro name，eg: centos/rhel/fedora,debian/ubuntu,opensuse/sles, arch
        distro_name=$(echo "${l_distro_release_info}" | sed -r -n '/^ID\|/{s@^.*?\|(.*)$@\L\1@g;p}')

        #version id, eg: 7/8, 16.04/16.10, 13.2/42.2
        distro_version_id=$(echo "${l_distro_release_info}" | sed -r -n '/^VERSION_ID\|/{s@^.*?\|(.*)$@\L\1@g;p}')
        distro_version_id_details=''

        case "${distro_name,,}" in
            arch )
                # file /etc/arch-release empty
                # version id is rolling
                distro_version_id=$(echo "${l_distro_release_info}" | sed -r -n '/^BUILD_ID\|/{s@^.*?\|(.*)$@\L\1@g;p}')
                ;;
            debian ) [[ -f /etc/debian_version && -s /etc/debian_version ]] && distro_version_id=$(cat /etc/debian_version) ;;
            ubuntu )
                distro_version_id_details=$(echo "${l_distro_release_info}" | sed -r -n '/^VERSION\|/{s@^.*?\|([[:digit:].]+).*$@\L\1@g;p}')
                [[ -n "${distro_version_id_details}" ]] || distro_version_id_details=$(echo "${l_distro_release_info}" | sed -r -n '/^PRETTY_NAME\|/{s@^.*?\|[^[:digit:]]+([[:digit:].]+).*$@\L\1@g;p}')
                ;;
            centos )
                if [[ "${distro_version_id:0:1}" -ge 6 && -s /etc/redhat-release ]]; then
                    # 7.5.1804    7.1804
                    distro_version_id=$(sed -r -n 's@^[^[:digit:]]*([^[:space:]]*).*$@\1@g;p' /etc/redhat-release)
                fi
                ;;
            alpine ) [[ -f /etc/alpine-release && -s /etc/alpine-release ]] && distro_version_id=$(cat /etc/alpine-release) ;;
        esac

        #distro full pretty name, for CentOS ,file redhat-release is more detailed
        if [[ -s '/etc/redhat-release' ]]; then
            distro_fullname=$(cat /etc/redhat-release)
        else
            distro_fullname=$(echo "${l_distro_release_info}" | sed -r -n '/^PRETTY_NAME\|/{s@^.*?\|(.*)$@\1@g;p}')

            case "${distro_name,,}" in
                debian )
                    if fn_CommandCheck 'lsb_release'; then
                        distro_fullname=$(lsb_release -d 2> /dev/null | sed -r -n 's@^[^:]+:[[:space:]]*@@g;p')
                    else
                        distro_fullname=$(echo "${distro_fullname}" | sed -r -n 's@[[:space:]]+[[:digit:].]+[[:space:]]+@ '"${distro_version_id}"' @g;p')
                    fi
                    ;;
            esac
        fi

        # Fedora, Debian，SUSE has no parameter ID_LIKE, only has ID
        # CentOS7 'rhel fedora'
        # Amazon Linux ID_LIKE v1 'rhel fedora'; v2 'centos rhel fedora'
        # Arch Linu only has ID, has no ID_LIKE
        # Pop!_OS 'ubuntu debian'
        distro_family_own=$(echo "${l_distro_release_info}" | sed -r -n '/^ID_LIKE\|/{s@^.*?\|(.*)$@\L\1@g;p}')
        [[ "$distro_family_own" == '' ]] && distro_family_own="${distro_name}"

        if [[ "${distro_name}" == 'amzn' || "${distro_family_own,,}" =~ (centos|fedora|rhel) ]]; then
            # amzn <==> The Amazon Linux
            distro_family_own='rhel'
        elif [[ "${distro_name}" == 'alpine' ]]; then
            distro_family_own='alpine'
        elif [[ "${distro_name}" == 'arch' || "${distro_name}" =~ ^manjaro ]]; then
            distro_family_own='arch'
        elif [[ "${distro_family_own,,}" =~ ^(debian|ubuntu) ]]; then
            distro_family_own='debian'
        fi

        # GNU/Linux distribution official site
        # distro_official_site=$(echo "${l_distro_release_info}" | sed -r -n '/^HOME_URL\|/{s@^.*?\|(.*)$@\L\1@g;p}')

        case "${distro_name,,}" in
            debian|ubuntu ) distro_codename=$(echo "${l_distro_release_info}" | sed -r -n '/^VERSION\|/{s@^.*?\|[^\(]+\(([^[:space:]\)]+).*$@\L\1@g;p}') ;;
            opensuse ) distro_codename=$(sed -r -n '/^CODENAME[[:space:]]*=/{s@^[^=]+=[[:space:]]*(.*)$@\L\1@g;p}' /etc/SuSE-release) ;;
            * ) distro_codename='' ;;
        esac
    elif [[ -s '/etc/redhat-release' ]]; then
        distro_release_file='/etc/redhat-release' # for CentOS 5, CentOS 6
        distro_family_own='rhel'   # family is rhel (RedHat)
        distro_fullname=$(cat "${distro_release_file}")

        # distro_name=$(rpm -q --qf "%{name}" -f "${distro_release_file}") # centos-release , fedora-release
        # distro_name=${distro_name%%-*}    # centos, fedora
        distro_name="${distro_fullname%% *}"
        distro_name="${distro_name,,}"

        distro_version_id=$(sed -r -n 's@^[^[:digit:]]*([^[:space:]]*).*$@\1@g;p' "${distro_release_file}")
    elif [[ -s '/etc/debian_version' && -s '/etc/issue.net' ]]; then
        distro_release_file='/etc/issue.net'   #Debian GNU/Linux 6.0
        distro_name=$(sed -r -n 's@([^[:space:]]*).*@\L\1@p' "${distro_release_file}")
        distro_version_id=$(sed -r -n 's@[^[:digit:]]*([[:digit:]]{1}).*@\1@p' "${distro_release_file}") #6
        distro_fullname=$(cat "${distro_release_file}")
        distro_family_own='debian'   # family is debian (Debian)

        # 6.0|Squeeze|2011-02-06|2016-02-29
        # 5.0|Lenny|2009-02-14|2012-02-06
        # 4.0|Etch|2007-04-08|2010-02-15
        # 3.1|Sarge|2005-06-06|2008-03-31
        # 3.0|Woody|2002-07-19|2006-06-30
        # 2.2|Potato|2000-08-15|2003-06-30
        # 2.1|Slink|1999-03-09|2000-10-30
        # 2.0|Hamm|1998-07-24|
        # 1.3|Bo|1997-07-02|
        # 1.2|Rex|1996-12-12|
        # 1.1|Buzz|1996-06-17|
        case "${distro_version_id:0:1}" in
            6 ) distro_codename='squeeze' ;;
            5 ) distro_codename='lenny' ;;
            4 ) distro_codename='etch' ;;
            * ) distro_codename='' ;;
        esac
    fi

    # - Convert distro family name
    case "${distro_family_own,,}" in
        debian ) distro_family_own='Debian' ;;
        suse|sles ) distro_family_own='SUSE' ;;
        rhel ) distro_family_own='RedHat' ;;
        alpine ) distro_family_own='Alpine Linux' ;;
        arch ) distro_family_own='Arch Linux' ;;
        * ) distro_family_own='Unknown' ;;
    esac

    # Package Manager (Debian/SUSE/RedHat)
    # OpenSUSE has utility apt-get, aptitude.
    case "${distro_family_own,,}" in
        arch* ) distro_pack_manager='pacman' ;;
        alpine* ) distro_pack_manager='apk' ;;
        debian ) distro_pack_manager='apt-get' ;;
        suse ) distro_pack_manager='zypper' ;;
        redhat )
            distro_pack_manager='yum'
            fn_CommandCheck 'dnf' && distro_pack_manager='dnf'
            ;;
    esac

    # Pretty Name|Distro Name|Code Name|Version ID|Family Name|Package Manager
    echo "${distro_fullname}|${distro_name}|${distro_codename}|${distro_version_id}|${distro_family_own}|${distro_pack_manager}"
}

# .update.lock
# -- exist      --> check modification timestamp
#                     --> <= 1800s
#                     --> > 1800s  --> check latest commit id
#                                      --> ==
#                                      --> !=  --> Download
# -- not exist  --> Download

fn_BoostToolInstallation(){
    # - Lock file check
    local l_update_lock_path=${l_update_lock_path:-"${user_codebase_dir}/.update.lock"} # save latest commit id
    local l_is_need_download=${l_is_need_download:-0}

    local l_project_namespace=${l_project_namespace:-'axdsop/nix-dotfiles'}
    local l_latest_commit_id=${l_latest_commit_id:-}

    if [[ -f "${l_update_lock_path}" ]]; then
        # - timestamp comparasion
        local l_current_timestamp=${l_current_timestamp:-"$(date +'%s')"}
        local l_modification_timestamp=${l_modification_timestamp:-$(stat -c '%Y' "${l_update_lock_path}" 2> /dev/null)}

        if [[ $(( l_current_timestamp - l_modification_timestamp )) -gt "${timestamp_diff_threshold}" ]]; then
            # - latest commit id comparasion
            # https://gitlab.com/gitlab-org/gitlab-ce/issues/33133
            # https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
            # axdsop/nixnotes --> axdsop%2Fnixnotes
            l_latest_commit_id=$(${download_tool} "https://gitlab.com/api/v4/projects/${l_project_namespace//\//%2F}/repository/commits/HEAD" 2> /dev/null | sed -r -n 's@.*"id"[[:space:]]*:[[:space:]]*"([^"]+).*$@\1@g;p')

            [[ -n "${l_latest_commit_id}" && $(head -n 1 "${l_update_lock_path}") == "${l_latest_commit_id}" ]] || l_is_need_download=1
        fi
    else
        l_is_need_download=1
    fi

    # - Download (Install/Update)
    local l_custom_flag=${l_custom_flag:-'AxdSop Setting'}
    local l_custom_flag_start=${l_custom_flag_start:-"${l_custom_flag} Start"}
    local l_custom_flag_end=${l_custom_flag_start:-"${l_custom_flag} End"}

    local l_user_bashrc=${l_user_bashrc:-"${user_specify_home}/.bashrc"}
    local l_user_boost_bashrc=${l_user_boost_bashrc:-"${user_codebase_dir}/bashrc"}
    
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        local l_temp_save_dir=$(mktemp -d -t XXXXXXXX)

        if fn_CommandCheck 'tar'; then
            if fn_CommandCheck 'gzip'; then
                #  -z, --gzip, --gunzip, --ungzip Filter the archive through gzip(1).
                ${download_tool} "https://gitlab.com/${l_project_namespace}/-/archive/master/nix-dotfiles-master.tar.gz" | tar xzf - -C "${l_temp_save_dir}" --strip-components=1

            else
                ${download_tool} "https://gitlab.com/${l_project_namespace}/-/archive/master/nix-dotfiles-master.tar" | tar xf - -C "${l_temp_save_dir}" --strip-components=1
            fi
        elif fn_CommandCheck 'git'; then
            git clone -q "https://gitlab.com/${l_project_namespace}.git" "${l_temp_save_dir}"
            [[ -d "${l_temp_save_dir}/.git" ]] && rm -rf "${l_temp_save_dir}/.git"
        fi

        # - ranger script scope.sh needs execute permission
        find "${l_temp_save_dir}" -type f -name 'scope.sh' -exec chmod 750 {} \;

        # - bashrc relocate and initialization
        local l_bashrc_relocate=${l_bashrc_relocate:-"${l_temp_save_dir}/bashrc"}
        # [[ -f "${l_temp_save_dir}/Configs/axdsop/bashrc" ]] && cp "${l_temp_save_dir}/Configs/axdsop/bashrc" "${l_bashrc_relocate}"
        find "${l_temp_save_dir}" -type f -name 'bashrc' -exec cp {} "${l_bashrc_relocate}" \;

        # - LS_COLORS from https://github.com/trapd00r/LS_COLORS
        local l_user_boost_lscolors=${l_user_boost_lscolors:-"${l_temp_save_dir}/lscolors.sh"}
        local l_lscolors_raw_url='https://raw.githubusercontent.com/trapd00r/LS_COLORS/master/LS_COLORS'
        local l_lscolors_raw_temp_save_path=$(mktemp -t XXXXXXX)
        ${download_tool} "${l_lscolors_raw_url}" > "${l_lscolors_raw_temp_save_path}"
        # keep DIR color provided by terminal theme
        sed -r -i '/^DIR[[:space:]]+/{s@^@#@g}' "${l_lscolors_raw_temp_save_path}"
        # -b, --sh, --bourne-shell    output Bourne shell code to set LS_COLORS
        dircolors -b "${l_lscolors_raw_temp_save_path}" > "${l_user_boost_lscolors}"
        [[ -f "${l_lscolors_raw_temp_save_path}" ]] && rm -f "${l_lscolors_raw_temp_save_path}"
        # dirname "$(readlink -f -- "$0")"
        # sed -r -i '/^PS1/a # - LS_COLORS From https://github.com/trapd00r/LS_COLORS\n[ -f '"~/.${l_user_boost_lscolors#*.}"' ] && source '"~/.${l_user_boost_lscolors#*.}"'' "${l_bashrc_relocate}"
        sed -r -i '/^PS1/a \ \n# - LS_COLORS From https://github.com/trapd00r/LS_COLORS\n[ -f '"~/.${user_codebase_dir#*.}/${l_user_boost_lscolors##*/}"' ] && source '"~/.${user_codebase_dir#*.}/${l_user_boost_lscolors##*/}"'' "${l_bashrc_relocate}"

        # specific distro system update
        local l_distro_info=''
        l_distro_info=$(fn_DistroReleaseInfoDetection)
        # Pretty Name|Distro Name|Code Name|Version ID|Family Name|Package Manager
        local l_pack_manager=$(cut -d\| -f6 <<< "${l_distro_info}")
        local l_distro_name=$(cut -d\| -f2 <<< "${l_distro_info}")
        local l_distro_family_own=$(cut -d\| -f5 <<< "${l_distro_info}")
  
        # system update
        sed -r -i '/'"${l_custom_flag_start}"'/,/'"${l_custom_flag_end}"'/{/_system_update=/{/'"${l_pack_manager}"'/!d;s@^#?[[:space:]]*@@g;}}' "${l_bashrc_relocate}" 2> /dev/null

        # remove older version kernel
        sed -r -i '/'"${l_custom_flag_start}"'/,/'"${l_custom_flag_end}"'/{/_kernel_(old|remove)=/{/'"${l_pack_manager}"'/!d;s@^#?[[:space:]]*@@g;}}' "${l_bashrc_relocate}" 2> /dev/null

        # Arch Linux
        [[ "${l_distro_name}" == 'arch' || "${l_distro_family_own}" =~ ^Arch ]] && sed -r -i '/_archlinux_/{/alias/{s@^#?[[:space:]]*@@g}}' "${l_bashrc_relocate}" 2> /dev/null

        # mesg: ttyname failed: Inappropriate ioctl for device
        fn_CommandCheck 'gpg2' && sed -r -i '/GPG_TTY=/{s@^#?[[:space:]]*@@g}' "${l_bashrc_relocate}" 2> /dev/null

        # docker
        fn_CommandCheck 'docker' && sed -r -i '/docker usage start/,/docker usage end/{/alias/{s@^#?[[:space:]]*@@g;}}' "${l_bashrc_relocate}" 2> /dev/null

        # aws cli
        if fn_CommandCheck 'aws'; then
            sed -r -i '/_awscli_operation/{s@^#?[[:space:]]*@@g}' "${l_bashrc_relocate}" 2> /dev/null
            local l_aws_absolute_path=''
            local l_aws_completer_absolute_path=''
            l_aws_absolute_path=$(readlink -f $(which aws 2> /dev/null || command -v aws 2> /dev/null))
            [[ -n "${l_aws_absolute_path}" && -f "${l_aws_absolute_path}" && -d "${l_aws_absolute_path%/*}" ]] && l_aws_completer_absolute_path="${l_aws_absolute_path%/*}/aws_completer"
            # complete -C '/opt/aws/bin/aws_completer' aws
            [[ -n "${l_aws_completer_absolute_path}" && -f "${l_aws_completer_absolute_path}" ]] && sed -r -i '/aws operation start/,/aws operation end/{/complete -C/{s@^#[[:space:]]*@@g;s@AWS_COMPLETER_PATH@'"${l_aws_completer_absolute_path}"'@g;}}'  "${l_bashrc_relocate}" 2> /dev/null
        fi

        # remove sudo in command
        [[ "${user_specify_home}" =~ ^/home/ ]] || sed -r -i 's@[[:space:]]*sudo[[:space:]]+@ @g;' "${l_bashrc_relocate}" 2> /dev/null


        # - replace codebase dir ~/.config
        [[ -d "${user_codebase_dir}" ]] && mv "${user_codebase_dir}" "${user_codebase_dir}.bak"
        mv "${l_temp_save_dir}" "${user_codebase_dir}"
        rm -rf "${user_codebase_dir}.bak"

        # [[ -f "${l_update_lock_path}" ]] && sudo chattr -i "${l_update_lock_path}" 2> /dev/null
        [[ -n "${l_latest_commit_id}" ]] && echo "${l_latest_commit_id}" > "${l_update_lock_path}"
        echo "Operation time: $(date)" >> "${l_update_lock_path}"

        chown -R "${user_specify}" "${user_codebase_dir}"
        chgrp -hR "${user_specify}" "${user_codebase_dir}" &> /dev/null

        # [[ -f "${l_update_lock_path}" ]] && sudo chattr +i "${l_update_lock_path}" 2> /dev/null

        echo "Boost tool installed in ${user_codebase_dir} successfully."

        [[ -f "${user_codebase_dir}/deploy.sh" ]] && echo -e "\nDeploying Init:\n\nbash ${user_codebase_dir}/deploy.sh -h\n\n"
    else
        echo 'Nothing need to do.'
    fi

    # - ~/.bashrc append
    sed -r -i '/'"${l_custom_flag_start}"'/,/'"${l_custom_flag_end}"'/d' "${l_user_bashrc}" &> /dev/null
    echo -e "# ${l_custom_flag_start}\n[ -f ~/.${l_user_boost_bashrc#*.} ] && . ~/.${l_user_boost_bashrc#*.}\n# ${l_custom_flag_end}" >> "${l_user_bashrc}"
}


#########  2-4 Available Software Script  #########
fn_AvailableSoftwareListExtraction(){
    available_software_list=${available_software_list:-}
    available_software_list=$(${download_tool} "${repo_readme_page}" | sed -r -n '/TOC/,${/TOC/,/^$/d;/^(#{2,}|\*)/!d;/^\*/{s@^[^[]+\[([^]]+)\]\(\.([^)]+)\).*@\1|\2@g};p}' | while read -r line; do if [[ "${line}" =~ ^\# ]]; then group_name="${line#* }"; elif [[ -n "${line}" ]]; then script_name="${line##*/}"; script_name="${script_name%%.*}"; echo "${group_name}|${line%%|*}|${script_name%%.*}|${line##*|}"; fi done )
    # Contianer|Docker CE|Docker-CE|/Container/Docker-CE.sh
    if [[ -z "${available_software_list}" ]]; then
        echo "${c_red}Sorry${c_normal}: fail to get available softwate list!"
        eixt
    fi
}

fn_AvailableSoftwareScriptOperation(){
    # available_software_list
    fn_AvailableSoftwareListExtraction

    # list available software
    if [[ "${list_available_software}" -eq 1 ]]; then
        echo -e "${c_bold}${c_red}Available Software List:${c_normal}\n"
        echo "${available_software_list}" | sed '=' | sed 'N;s@\n@|@g;' | while IFS="|" read -r item_num item_type item_name item_script_name item_url; do
            printf "${c_yellow}%4s${c_normal} - ${c_bold}${c_yellow}%s${c_normal} (%s)\n" "${item_num}" "${item_name}" "${item_type}"
        done

    # choose software
    elif [[ "${choose_software_script}" -eq 1 ]]; then
        local l_software_type_choose=${l_software_type_choose:-}
        local l_software_choose=${l_software_choose:-}
        echo "${c_bold}${c_red}Available Software Type:${c_normal}"
        PS3="${c_yellow}Choose Software Type Number(e.g. 1, 2,...): ${c_normal}"

        IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
        IFS="|" # Setting temporary IFS

        select item in $(echo "${available_software_list}" | cut -d\| -f1 | sort | uniq | sed ':a;N;$!ba;s@\n@|@g'); do
            l_software_type_choose="${item}"
            [[ -n "${l_software_type_choose}" ]] && break
        done < /dev/tty
        IFS=${IFS_BAK}  # Restore IFS
        unset IFS_BAK

        printf "\nSoftware type you choose is ${c_bold}${c_yellow}%s${c_normal}.\n\n" "${l_software_type_choose}"

        IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
        IFS="|" # Setting temporary IFS

        echo "${c_bold}${c_red}Available Software List Under ${l_software_type_choose}:${c_normal}"
        PS3="${c_yellow}Choose Software Number(e.g. 1, 2,...): ${c_normal}"

        select item in $(echo "${available_software_list}" | sed -r -n '/^'"${l_software_type_choose}"'\|/{s@^[^\|]+\|([^\|]+).*$@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g'); do
            l_software_choose="${item}"
            [[ -n "${l_software_choose}" ]] && break
        done < /dev/tty
        IFS=${IFS_BAK}  # Restore IFS
        unset IFS_BAK

        printf "\nOperation command of ${c_bold}${c_yellow}%s${c_normal}: \n\n${c_yellow}%s${c_normal}\n\n" "${l_software_choose}" "${download_method_origin} ${repo_readme_page%/*}$(echo "${available_software_list}" | sed -r -n '/^'"${l_software_type_choose}"'\|'"${l_software_choose//\//\\/}"'\|/{s@.*\|(.*)$@\1@g;p}') | ${sudo_operation}bash -s -- -h"
    fi
}


#########  2-4 Update All Installed Software  #########
fn_AllInstalledSoftwareUpdate(){
    # - from gitlab
    # available_software_list
    fn_AvailableSoftwareListExtraction
    local l_online_software_list
    l_online_software_list=$(mktemp -t "${mktemp_format}")
    echo "${available_software_list}" > "${l_online_software_list}"

    # from dir /opt/
    local l_installed_software_list
    l_installed_software_list=$(mktemp -t "${mktemp_format}")
    # SC2011: Use 'find .. -print0 | xargs -0 ..' or 'find .. -exec .. +' to allow non-alphanumeric filenames.
    # ls -d /opt/* 2> /dev/null | xargs -l | sed -r -n 's@.*\/(.*)$@\1@g;p' > "${l_installed_software_list}"

    find /opt/* -maxdepth 0 -type d -print | sed -r -n 's@.*\/(.*)$@\1@g;p' > "${l_installed_software_list}"
    # remove empty line
    sed -r -i '/^$/d' "${l_installed_software_list}"

    if [[ -s "${l_online_software_list}" && -s "${l_installed_software_list}" ]]; then
        local l_available_software_list=${l_available_software_list:-}

        # awk multiple files comparasion
        # https://www.gnu.org/software/gawk/manual/gawk.html#Input-Summary
        # FNR indicates how many records have been read from the current input file;
        # NR indicates how many records have been read in total.

        # Contianer|Docker CE|Docker-CE|/Container/Docker-CE.sh
        l_available_software_list=$(awk -F\| 'NR==FNR{a[$3]=$4;next}{if (a[$1]) print a[$1]}' "${l_online_software_list}" "${l_installed_software_list}")    # /Container/Docker-CE.sh

        if [[ -n "${l_available_software_list}" ]]; then
            echo "${l_available_software_list}" | while read -r line;do
                if [[ -n "${proxy_server_specify}" ]]; then
                    ${download_method_origin} "${repo_readme_page%/*}${line}" | ${sudo_operation}bash -s -- -p "${proxy_server_specify}"
                else
                    ${download_method_origin} "${repo_readme_page%/*}${line}" | ${sudo_operation}bash -s --
                fi
                echo ''
            done
        fi
    fi

    [[ -f "${l_online_software_list}" ]] && rm "${l_online_software_list}"
    [[ -f "${l_installed_software_list}" ]] && rm "${l_installed_software_list}"
}


#########  2-5 Repository download  #########
fn_OfficialRepositoryDownload(){
    if fn_CommandCheck 'git'; then
        # git@gitlab.com:axdsop/nixnotes.git
        # https://gitlab.com/axdsop/nixnotes.git
        printf "\nRepository ${c_bold}${c_red}%s${c_normal} download command: \n\n${c_yellow}%s${c_normal}\n\n" "${repository_name}" "git clone https://gitlab.com/${repository_name}.git"
    else
        # https://gitlab.com/axdsop/nixnotes/-/archive/master/nixnotes-master.zip
        # https://gitlab.com/axdsop/nixnotes/-/archive/master/nixnotes-master.tar.gz
        local l_file_suffix=${l_file_suffix:-'zip'}
        fn_CommandCheck 'gzip' && l_file_suffix='tar.gz'

        printf "\nRepository ${c_red}%s${c_normal} download command: \n\n${c_yellow}%s${c_normal}\n\n" "${repository_name}" "${download_method_origin} https://gitlab.com/${repository_name}/-/archive/master/${repository_name##*/}-master.${l_file_suffix} > ${repository_name##*/}.${l_file_suffix}"
    fi
}

#########  3. Executing Process  #########
fn_InitializationCheck

# [[ "${system_postinstallation}" -eq 1 ]] && fn_SystemPostinstallation
[[ "${documents_download}" -eq 1 ]] && fn_OfficialDocumentsDownload
[[ "${install_boost_tool}" -eq 1 ]] && fn_BoostToolInstallation
[[ "${list_available_software}" -eq 1 || "${choose_software_script}" -eq 1 ]] && fn_AvailableSoftwareScriptOperation
[[ "${update_installed_software_script}" -eq 1 ]] && fn_AllInstalledSoftwareUpdate
[[ "${repository_download}" -eq 1 ]] && fn_OfficialRepositoryDownload


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    unset os_detect
    unset install_boost_tool
    unset documents_download
    unset list_available_software
    unset choose_software_script
    unset update_installed_software_script
    unset system_postinstallation
    unset repository_download
    unset proxy_server_specify
    unset login_user
    unset login_user_home
    unset sudo_operation
}

trap fn_TrapEXIT EXIT

# Script End