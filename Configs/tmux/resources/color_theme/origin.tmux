# Personal testing color schema 

set -g status-style fg=colour166,bg=colour222 # bg LightGoldenrod2 if no -g, it will prompts error info 'no current session'
set -g pane-border-style fg=colour222
set -g pane-active-border-style fg=colour166 # fg DarkOrange3
set -g display-panes-active-colour colour166
set -g display-panes-colour colour69 # CornflowerBlue
set -g message-style bg=colour111,fg=white,bright
#set -g message-style bg=colour224,fg=colour166 # bg MistyRose1
set -g message-command-style fg=colour222,bg=colour243
set -g window-style fg=colour111
set -g window-active-style fg=colour166
setw -g window-status-separator '|'
setw -g window-status-style fg=colour243,bg=default # fg Grey46
setw -g window-status-current-style fg=colour166,italics,bg=default,bright # fg DarkOrange3