# Onedark Theme
# Extracting from https://github.com/odedlaz/tmux-onedark-theme

# color definition
c_black="#282c34"
c_blue="#61afef"
c_yellow="#e5c07b"
c_red="#e06c75"
c_white="#aab2bf"
c_green="#98c379"
c_grey="#5c6370"
c_visual_grey="#3e4452"

# Extra glyphs for powerline separators https://github.com/ryanoasis/powerline-extra-symbols
powerline_sep_entity=    # \Ue0c0
powerline_sep_empty=   # \Ue0c1


# setting
set -g status-style fg=$c_green,bg=$c_black

set -g pane-border-style fg=$c_grey,bg=$c_black
set -g pane-active-border-style fg=$c_blue,bright,bg=$c_black
set -g display-panes-colour $c_yellow
set -g display-panes-active-colour $c_blue

set -g message-style fg=$c_yellow,bg=$c_black
set -g message-command-style fg=$c_white,bg=$c_black

set -g window-style fg=$c_yellow
set -g window-active-style fg=$c_blue   # c_red
#setw -g window-status-separator ''
#setw -g window-status-style fg=$c_yellow,italics,bg=$c_black
#setw -g window-status-current-style fg=$c_blue,bright,bg=$c_black

set -g status-left-length 100
set -g status-right-length 100

set -g status-left "#[fg=$c_black,bg=$c_green]S: #S #[fg=$c_green,bg=$c_black,nobold,nounderscore,noitalics]$powerline_sep_entity"

set -g window-status-format "#[fg=$c_yellow,italics,bg=$c_black]w#I$powerline_sep_empty#W #[fg=$c_black,bg=$c_black,nobold,nounderscore,noitalics]$powerline_sep_entity"
set -g window-status-current-format "#[fg=$c_blue,bold,bg=$c_visual_grey]w#I$powerline_sep_empty#W #[fg=$c_visual_grey,bg=$c_black,nounderscore,noitalics]$powerline_sep_entity"


# End