# tmux Configuration & Usage Note

[tmux][tmux] is a terminal multiplexer: it enables a number of terminals to be created, accessed, and controlled from a single screen. tmux may be detached from a screen and continue running in the background, then later reattached.

Its release runs on OpenBSD, FreeBSD, NetBSD, Linux, OS X and Solaris.


## TOC

1. [Official Wiki](#official-wiki)  
2. [Deployment Procedure](#deployment-procedure)  
2.1 [Installation](#installation)  
2.1.1 [tmux](#tmux)  
2.1.2 [bash completion](#bash-completion)  
2.2 [Initialization](#initialization)  
3. [Config Explanation](#config-explanation)  
3.1 [Default Config](#default-config)  
3.2 [Default prefix](#default-prefix)  
3.3 [Plugin Manager](#plugin-manager)  
3.4 [Color Theme](#color-theme)  
3.4.1 [Powerline Extra Symbols](#powerline-extra-symbols)  
3.5 [Source File](#source-file)  
3.5.1 [Configuration](#configuration)  
4. [Session Template](#session-template)  
4.1 [Source 1](#source-1)  
4.2 [Source 2](#source-2)  
4.3 [Session 3](#session-3)  
5. [Issues Occuring](#issues-occuring)  
5.1 [GPG Use GUI pinentry](#gpg-use-gui-pinentry)  
5.2 [alacritty](#alacritty)  
6. [Learning Resource](#learning-resource)  
7. [Change Log](#change-log)  


## Official Wiki

Item|Link
---|---
Official (GitHub)|[wiki](https://github.com/tmux/tmux/wiki)
Arch Linux|[wiki](https://wiki.archlinux.org/index.php/Tmux)
Gentoo|[wiki](https://wiki.gentoo.org/wiki/Tmux)


## Deployment Procedure

Download method detection

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'
```

### Installation

#### tmux

For Arch Linux

```bash
# - For Arch Linux
sudo pacman -S --noconfirm tmux
```

For Debian/Ubuntu

```bash
# common method
sudo apt-get install -y tmux
```

If you use Ubuntu 20.04 LTS (focal), this [package](https://packages.ubuntu.com/focal/tmux) is outdated, and it doesn't work properly.

Ubuntu 20.10 (groovy) provides the latest prebuilt binary [package](https://packages.ubuntu.com/groovy/tmux) (architecture just has *all*). To install it manually, see note [Components.md](/Configs/Components.md) section *Package Manually Install*.

```bash
# fn_UbuntuPackManualInstall 'tmux' 'amd64' 'groovy'
fn_UbuntuPackManualInstall 'tmux'
```

#### bash completion

GitHub project [page](https://github.com/tmux/tmux#documentation) intorduces tmux bash completion script [imomaliev/tmux-bash-completion](https://github.com/imomaliev/tmux-bash-completion).

```bash
# - tmux bash completion
# sudo curl -fsSL -o /usr/share/bash-completion/completions/tmux https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux

bash_completion_dir='/usr/share/bash-completion/completions'
bash_completion_tmux_path="${bash_completion_dir}/tmux"
if [[ -d "${bash_completion_dir}" ]]; then
    [[ -f "${bash_completion_tmux_path}" ]] || ${download_method} https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux | sudo tee 1> /dev/null "${bash_completion_tmux_path}"
fi
```

### Initialization

To initialize configuration, just save configuration files to directory *~/.config/tmux/*.

```bash
# ln -fs ~/.config/tmux/tmux.conf ~/.tmux.conf

cp -R Configs/tmux ~/.config/tmux
```

## Config Explanation

Specific configuration see file [tmux.conf][tmux_conf].

<!-- new-session (alias: new) / new-window (alias: neww) -->

Configuration directory tree architecture

```txt
.
├── RADEME.md
├── resources
│   ├── color_theme
│   │   ├── onedark.tmux
│   │   ├── origin.tmux
│   │   ├── solarized_256.tmux
│   │   ├── solarized_dark.tmux
│   │   └── solarized_light.tmux
│   └── source_template
│       ├── source_dashboard_override
│       ├── source_init_new
│       └── source_init_override
└── tmux.conf

3 directories, 10 files
```

### Default Config

From `man tmux`, by default, [tmux][tmux] loads the system configuration file from */etc/tmux.conf*, if present, then looks for a user configuration file at *~/.tmux.conf*.

Here choose *~/.tmux.conf*, but it is a symbolic link.

```bash
ln -fs ~/.config/tmux/tmux.conf ~/.tmux.conf
```


### Default prefix

```bash
# - Change default prefix binding from Ctrl-b to Ctrl-a
unbind C-b
set -g prefix C-a
# set -g prefix m-'\'  # use Alt (called Meta) instead of Ctrl.
bind C-a send-prefix

# - Setting the correct term
set -g default-terminal "tmux-256color"
```

### Plugin Manager

Here choose [Tmux Plugin Manager][tpm] (TPM) to install and load [tmux][tmux] plugins. Official documentation [docs](https://github.com/tmux-plugins/tpm/tree/master/docs).

I integrated **tpm** auto-installation and plugins auto-installation into file *tmux.conf*.

Here I set `TMUX_PLUGIN_MANAGER_PATH` to *~/.tmux/plugins/*

```conf
# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
plugin_save_path='~/.tmux/plugins'
# Default plugin save path is ~/.config/tmux/plugins/
# tmux show-env -g TMUX_PLUGIN_MANAGER_PATH
set-environment -g TMUX_PLUGIN_MANAGER_PATH ${plugin_save_path}
plugin_tpm_path="${plugin_save_path}/tpm"

run-shell -b "if [ -f ${plugin_tpm_path}/tpm ]; then ${plugin_tpm_path}/tpm; else git clone -q https://github.com/tmux-plugins/tpm ${plugin_tpm_path}; [ -f ${plugin_tpm_path}/bin/install_plugins ] && ${plugin_tpm_path}/bin/install_plugins; fi"
```

### Color Theme

Configuration file [tmux.conf][tmux_conf] has the following settings.

```bash
tmux_conf_dir="~/.config/tmux"

# source-file ~/.config/tmux/resources/color_theme/onedark.tmux
color_theme_path=${tmux_conf_dir}/specify_color_theme
source_color_name=onedark
run-shell -b "[ -L $color_theme_path ] || ln -fs ${tmux_conf_dir}/resources/color_theme/${source_color_name}.tmux ${color_theme_path}"
source-file ${color_theme_path}
```

Default is theme *onedark.tmux*, if you wanna change it. Choosing one color theme from directory *~/.config/tmux/resources/color_theme/*, then creating a soft link to *~/.config/tmux/specify_color_theme*.

```bash
ln -fs ~/.config/tmux/color/onedark.tmux ~/.config/tmux/color_theme
```

Then reloading configuration to make it works.


#### Powerline Extra Symbols

Color theme [onedark.tmux](./resources/color_theme/onedark.tmux) uses Nerd extra glyphs as powerline separators ([ryanoasis/powerline-extra-symbols](https://github.com/ryanoasis/powerline-extra-symbols)). These glyphs are now available in the patched fonts from [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts).

![powerline-extra-symbols](https://raw.githubusercontent.com/ryanoasis/powerline-extra-symbols/master/img/fontforge.png)

Here choose font [Sauce Code Pro Nerd Font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/SourceCodePro). If you wanna install this font, see note [Components.md](/Configs/Components.md) section *Sauce Code Pro*.


### Source File

[tmux][tmux] uses command `source-file` to load pre-configured file. Source files save in directory *~/.config/tmux/resources/source_template*


#### Configuration

Configuration file [tmux.conf][tmux_conf] has the following settings.

```conf
tmux_conf_dir="~/.config/tmux"
# - Session initialization
# initialize source
source_file_path=${tmux_conf_dir}/specify_source_file
source_template_name=source_init_new
run-shell -b "[ -L ${source_file_path} ] || ln -fs ${tmux_conf_dir}/resources/source_template/${source_template_name} ${source_file_path}"
bind F source-file ${source_file_path}
```

Specifing the default initialized source file. Choosing one source file from directory *~/.config/tmux/resources/source_template/*, then creating a soft link to *~/.config/tmux/specify_source_file*.

```bash
ln -fs ~/.config/tmux/resources/source_template/source_init_new ~/.config/tmux/specify_source_file
```


## Session Template

If you create multiple windows from source file, the selected pane of selected window will auto into copy mode. To exit copy mode, use `send-keys -X cancel`. ([How can I bind a key to “exit copy-mode” in tmux?](https://stackoverflow.com/questions/50311446/how-can-i-bind-a-key-to-exit-copy-mode-in-tmux#50392365))

### Source 1

Create New session

```bash
# Scenario: Used for tmux server started, create new session named 'Init'

#new -Ad -s 'Init' # -d create new session 'Init', -Ad create new session, but current session is still the existed session
new -AD -s 'Init' # -D create new session 'Init', -AD create and change to new session
selectw -T
#rename 'Init'
renamew -t 1 'Hello'
splitw -hb -c $HOME -l 50% -t 1
splitw -v -c $HOME -l 50% -t 1 # the new vertical pane index become 2
splitw -v -c $HOME -l 50% -t 3 # the inital horizon right pane index become from 2 to 3

send-keys -t 1 'date' 'Enter'
send-keys -t 2 'man tmux'
send-keys -t 3 'axdsop_distro_system_update'
send-keys -t 4 ''

selectp -t 1

# Session Setting End
```

### Source 2

Override existed session

```bash
# Scenario: Used for tmux server started, name current existed session to 'Init'

rename 'Init'

#selectw -T
renamew -t 1 'Hello'

splitw -hb -c $HOME -l 50% -t 1
splitw -v -c $HOME -l 50% -t 1 # the new vertical pane index become 2
splitw -v -c $HOME -l 50% -t 3 # the inital horizon right pane index become from 2 to 3

send-keys -t 1 'date' 'Enter'
send-keys -t 2 'man tmux'
send-keys -t 3 'axdsop_distro_system_update'
send-keys -t 4 ''

selectp -t 1  # (AA) the selected pane will auto in COPY mode


# 2nd window
neww -adP -c $HOME -n 'Coding'
selectw -n  # change to next window, same as 'selectw -t 2'
splitw -h -c $HOME -l 50% -t 1
send-keys -t 1 'ranger' 'Enter'
send-keys -t 2 'date' 'Enter'
selectp -t 1

# cancel 1st window selected pane from COPY mode
selectw -p  # change to previous window, same as 'selectw -t 1'
send-keys -X -t 1 cancel  # (AA) cancel CPOY mode from selected pane

# Session Setting End
```

### Session 3

Create one session (override) with 2 windows

```bash
# Scenario: Used for tmux server started, name current session to 'Dashboard'

new -AD
rename 'Dashboard'
selectw -T
renamew -t 1 'Monitoring'
splitw -hb -c $HOME -l 50% -t 1
selectp -t 1
splitw -v -c $HOME -l 50% -t 2

send-keys -t 1 'ytop -pa -b -I 2' 'Enter'  # yay -S --noconfirm ytop-bin
send-keys -t 2 'cal -w' 'Enter'
send-keys -t 3 'clear' 'Enter'
selectp -t 3   # the selected pane will auto in COPY mode, ~~I have not find a solution to solve this issue ???~~ (Solved by `send-keys -X cancel`)

# 2nd window
neww -adP -c $HOME -n 'FileManager'
selectw -n  #change to next window, same as 'selectw -t 2'
splitw -h -c $HOME -l 50% -t 1
send-keys -t 1 'ranger' 'Enter'
send-keys -t 2 'date' 'Enter'
selectp -t 1

# cancel 1st window selected pane from copy mode
selectw -p  # change to previous window, same as 'selectw -t 1'
send-keys -X -t 3 cancel

# Session Setting End
```

## Issues Occuring

* [How to attach to tmux session over SSH](https://blog.sleeplessbeastie.eu/2019/11/06/how-to-attach-to-tmux-session-over-ssh/)

### GPG Use GUI pinentry

Solution sees [How to force GPG to use console-mode pinentry to prompt for passwords?](https://superuser.com/questions/520980/how-to-force-gpg-to-use-console-mode-pinentry-to-prompt-for-passwords#1443119), just unset environment variable `DISPLAY` first.

```bash
export GPG_TTY=$(tty
unset DISPLAY
```

### alacritty

>open terminal failed: missing or unsuitable terminal: alacritty

Like issue [tmux doesn't recognize alacritty #2487](https://github.com/alacritty/alacritty/issues/2487), solution

```yml
# method 1 - setting environment parameter TERM, default value is alacritty
export TERM=xterm-256color

# method 2 - set env parameter in config file ~/.config/alacritty/alacritty.yml
env:
  # TERM variable
  #
  # This value is used to set the `$TERM` environment variable for
  # each instance of Alacritty. If it is not present, alacritty will
  # check the local terminfo database and use `alacritty` if it is
  # available, otherwise `xterm-256color` is used. Default is `alacritty`.
  TERM: xterm-256color
```

Reason

>The difference is that *xterm-256color* is for the version of the terminal emulator for the X Window System that supports 256 colors. The *alacritty* terminfo is thus for the Alacritty terminal emulator. It describes "the capabilities of hundreds of different display terminals. This allows external programs to be able to have character-based display output, independent of the type of terminal." (Per the Wikipedia article: https://en.wikipedia.org/wiki/Terminfo).

For SSH, just add parameter *RemoteCommand* file `~/.ssh/config` (solution form [How to set TERM environment variable in ssh configuration?](https://serverfault.com/questions/976779/how-to-set-term-environment-variable-in-ssh-configuration#1002884))

```bash
Host XXXX
  User XXXX
  Hostname XXXX
  RemoteCommand TERM=xterm-256color $SHELL
  RequestTTY yes
```


## Learning Resource

* Book [The Tao of tmux](https://leanpub.com/the-tao-of-tmux/read)
* [Tmux Cheat Sheet & Quick Reference](https://tmuxcheatsheet.com/)
* [Oh My Tmux](https://github.com/gpakosz/.tmux)


## Change Log

* May 05, 2020 Tue 09:10 ET
  * first draft
* May 13, 2020 Wed 08:02 ET
  * add issue tmux not support alacritty and solution
* May 16, 2020 Sat 20:12 ET
  * add session initialization sample
* Jun 03, 2020 Wed 21:37 ET
  * add color theme
* Jun 11, 2020 Thu 15:22 ET
  * add source configuration and templates
* Jul 26, 2020 Sun 09:07 ET
  * solve selected pane auto into COPY mode when create multiple windows
* Sep 20, 2020 Sun 09:10 ET
  * update source template, readme rewrite
* Oct 12, 2020 Mon 11:15 ET
  * integrate *tpm* auto-install process into [tmux.conf][tmux_conf]


[tmux]:https://github.com/tmux/tmux "tmux is a terminal multiplexer"
[tpm]:https://github.com/tmux-plugins/tpm
[tmux_conf]:./tmux.conf

<!-- End -->