#!/usr/bin/env bash

#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails
# IFS=$'\n\t' #IFS  Internal Field Separator

# Target: Configuration Network Proxy For GNOME/Utility Desktop Enviroment In GNU/Linux
# Developer: MaxdSre
# Change Log:
# - Oct 08, 2020 08:12 Thu ET - initialization check method update
# - Sep 27, 2019 09:25 Fri ET - change to new base functions
# - Jan 01, 2019 20:02 Tue ET - code reconfiguration, include base funcions from other file
# - May 01, 2018 16:05 Tue ET - add tor socks proxy mode
# - Nov 25, 2017 14:18 Sat +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT


#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
proxy_mode=${proxy_mode:-'none'}   # none/manual/auto
proxy_type=${proxy_type:-'socks'}   # ftp/http/https/socks for manual mode
proxy_server_specify=${proxy_server_specify:-''}
socks_service=${socks_service:-''}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Configuring Network Proxy For GNOME/Utility Desktop Enviroment In GNU/Linux (RHEL/SUSE/Debian)

Running as normal user.

[available option]
    -h    --help, show help info
    -t proxy_type    --specify proxy type (ftp/http/https/socks), default is 'socks'
    -p ip:port    --specify proxy host info, e.g. '8.8.8.8:8888', default is ''
    -s socks_service    --specify socks proxy service name (ssh|tor), default is ''
\e[0m"
}

# -m proxy_mode    --specify proxy mode (none/manual/auto), default is 'none'


while getopts "ht:p:s:" option "$@"; do
    case "$option" in
        # m ) proxy_mode="$OPTARG" ;;
        t ) proxy_type="$OPTARG" ;;
        p ) proxy_server_specify="$OPTARG" ;;
        s ) socks_service="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '0'

    fnBase_CommandExistCheckPhase 'gawk'
    [[ "${login_user}" != 'root' ]] && fnBase_CommandExistCheckPhase 'sudo'
}


#########  2-1. Parameters Processing  #########
fn_SocksProxyInfoDetection(){
    local l_service_name="$1"
    local l_output=${l_output:-}

    local l_use_sudo=${l_use_sudo:-''}
    [[ "${login_user}" == 'root' ]] || l_use_sudo='sudo'
    # https://stackoverflow.com/questions/14126705/setting-proxy-through-bash-script-in-ubuntu
    fnBase_CommandExistIfCheck 'ss' && l_output=$(${l_use_sudo} bash -c 'ss -tnlp' | awk 'match($NF,/'''${l_service_name}'''/)&&match($4,/^127.0.0.1/){print $4; exit}')

    if [[ -z "${l_output}" ]]; then
        fnBase_CommandExistIfCheck 'netstat' && l_output=$(${l_use_sudo} bash -c 'netstat -tnlp 2>&1' | awk 'match($NF,/\/'''${l_service_name}'''$/)&&match($4,/^127.0.0.1/){print $4;exit}')
    fi

    echo "${l_output}"
}

fn_ParametersProcessing(){
    # - specify proxy server manually
    if [[ -n "${proxy_server_specify}" ]]; then
        if [[ "${proxy_server_specify}" =~ ^([0-9]{1,3}.){3}[0-9]{1,3}:[0-9]{1,5}$ ]]; then
            proxy_type="${proxy_type,,}"
            case "${proxy_type}" in
                ftp|http|https|socks )
                    proxy_mode='manual'
                    proxy_type="${proxy_type}"
                    ;;
                * ) fnBase_ExitStatement "${c_red}Error${c_normal}: please specify correspond proxy type (ftp/http/https/socks)!" ;;
            esac

        else
            fnBase_ExitStatement "${c_red}Error${c_normal}: please specify correct proxy host info like ${c_blue}ip:port${c_normal}!"
        fi    # end if proxy_server_specify

    else
        local l_socks_service=${l_socks_service:-}
        # - use ssh/tor socks tunnel
        case "${socks_service,,}" in
            s|ssh ) l_socks_service='ssh' ;;
            t|tor ) l_socks_service='tor' ;;
        esac

        if [[ -n "${l_socks_service}" ]]; then
            local l_proxy_info=${l_proxy_info:-}
            l_proxy_info=$(fn_SocksProxyInfoDetection "${l_socks_service}")
            if [[ -n "${l_proxy_info}" ]]; then
                proxy_mode='manual'
                proxy_type='socks'
                proxy_server_specify="${l_proxy_info}"
            else
                fnBase_ExitStatement "${c_red}Error${c_normal}: fail to detect ${l_socks_service} proxy info in local system!"
            fi
        # - nothing, close ssh socks tunnel use
        else
            proxy_mode='none'
            proxy_type='socks'
        fi

    fi
}


#########  2-2. gsettings Configuration  #########
fn_GsettingsConfiguration(){
    fnBase_CommandExistIfCheck 'gsettings' || fnBase_ExitStatement "${c_red}Error${c_normal}: fail to find command ${c_blue}gsettings${c_normal} to configure network proxy!"

    fn_ParametersProcessing

    local proxy_host=${proxy_host:-''}
    local proxy_port=${proxy_port:-0}

    case "${proxy_mode,,}" in
        'none' )
            gsettings set org.gnome.system.proxy.${proxy_type} host "${proxy_host}"
            gsettings set org.gnome.system.proxy.${proxy_type} port "${proxy_port}"
            gsettings set org.gnome.system.proxy mode 'none'
            ;;
        'manual' )
            if [[ -n "${proxy_server_specify}" ]]; then
                proxy_host="${proxy_server_specify%%:*}"
                proxy_port="${proxy_server_specify##*:}"
            fi

            if [[ -n "${proxy_port}" && "${proxy_port}" -gt 0 && "${proxy_port}" -le 65535 ]]; then
                gsettings set org.gnome.system.proxy.${proxy_type} host "${proxy_host}"
                gsettings set org.gnome.system.proxy.${proxy_type} port "${proxy_port}"
                # gsettings get org.gnome.system.proxy.${proxy_type} host
                # gsettings get org.gnome.system.proxy.${proxy_type} port

                gsettings set org.gnome.system.proxy mode 'manual'
            else
                gsettings set org.gnome.system.proxy mode 'none'
                fnBase_ExitStatement "${c_red}Error${c_normal}: proxy ${c_blue}${proxy_server_specify}${c_normal} you specify is illegal!"
            fi
            ;;
    esac
}

#########  3. Executing Process  #########
fn_InitializationCheck
fn_GsettingsConfiguration



#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset proxy_mode
    unset proxy_type
    unset proxy_server_specify
    unset socks_service
}

trap fn_TrapEXIT EXIT

# Script End
