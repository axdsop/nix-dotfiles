#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Generation Random Available Port (System/User/Ephemeral) On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:15 Thu ET - initialization check method update
# - Oct 02, 2019 10:07 Wed ET - change to new base functions
# - Jan 01, 2019 21:39 Tue ET - code reconfiguration, include base funcions from other file
# - Dec 30, 2017 11:46 Sat +0800    - Performance optimization
# - Dec 02, 2017 20:30 Sat +0800
# - June 08, 2017 14:01 Thu +0800
# - May 6, 2017 17:53 Sat ET
# - Feb 27, 2017 09:59 Mon +0800
# - Dec 21, 2016 15:56 Wed +0800
# - Nov 01, 2016 11:23 Thu +0800
# - Sep 08, 2016 23:00 Thu +0800


# Service Name and Transport Protocol Port Number Registry
# https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
# https://tools.ietf.org/html/rfc6335

# Port numbers are assigned in various ways, based on three ranges:
# 1. System Ports (0-1023)      need root privilege
# 2. User Ports (1024-49151)    not need root privilege
# 3. Dynamic and/or Private Ports (49152-65535)    ephemeral port


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
json_output=${json_output:-0}
port_type=${port_type:-'h'}
simple_output=${simple_output:-0}
port_start=${port_start:-}
port_end=${port_end:-}
generate_type=${generate_type:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | [sudo] bash -s -- [options] ...

Generating Random Available Port (System/User/Ephemeral/High ephemeral) On GNU/Linux!

[available option]
    -h    --help, show help info
    -j    --json, output result via json format
    -t port_type    --specify port type(r/n/e/h), 'r' is system ports (0,1023], 'n' is user ports [1024,32767], 'e' is regular ephemeral port [32768,49151], 'h' is high ephemeral port [49152,65535]. Default is 'h'.
    -s    --simple output, just output port generated
\e[0m"
}

while getopts "hjt:s" option "$@"; do
    case "$option" in
        j ) json_output=1 ;;
        t ) port_type="$OPTARG" ;;
        s ) simple_output=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    fnBase_RunningEnvironmentCheck '0' '0'

    fnBase_CommandExistCheckPhase 'gawk'

    if fnBase_CommandExistIfCheck 'ss'; then
        check_tool='ss'   # ss -tuanp
        port_field='5'   # awk field $5
        state_field='2'   # awk field $2
    elif fnBase_CommandExistIfCheck 'netstat'; then
        check_tool='netstat' # netstat -tuanp
        port_field='4'   # awk field $4
        state_field='6'   # awk field $6
    else
        fnBase_ExitStatement "${c_red}Error${c_normal}, No ${c_blue}ss${c_normal} or ${c_blue}netstat${c_normal} command found!"
    fi
}

fn_OutputJsonConcat(){
    local l_item=${1:-}
    local l_val="${2:-}"
    local l_var=${3:-'output_json'}
    local l_output=${l_output:-}
    if [[ "${l_item}" == '_s' ]]; then
        l_output="{"
    elif [[ "${l_item}" == '_e' ]]; then
        l_output="${!l_var}"
        l_output=${l_output%,*}
        l_output=${l_output}"}"
    else
        if [[ -n "${l_item}" && -n "${l_val}" ]]; then
            l_output="${!l_var}\"${l_item}\":\"${l_val}\","
        else
            l_output="${!l_var}"
        fi
    fi
    echo "${l_output}"
}


#########  Core Processing Procedure  #########
# - Usable Ports Range Check
fn_AvailablePortRangeCheck(){
    # http://www.ncftp.com/ncftpd/doc/misc/ephemeral_ports.html
    # Kernel parameter 'ip_local_port_range' control ephemeral port range in GNU/Linux

    case "${port_type,,}" in
        r )
            generate_type='system'
            port_start=1
            port_end=1023
            ;;
        n )
            generate_type='user'
            port_start=1024
            port_end=32767
            ;;
        e)
            generate_type='ephemeral'
            port_start=32768
            port_end=49151
            ;;
        h|* )
            generate_type='high ephemeral'
            port_start=49152
            port_end=65535
            ;;
    esac
}

# - Used Port Check
fn_UsedPortCheck(){
    port_used_list=$(mktemp -t "${mktemp_format}") #temporary file
    # 1 - list ports being used by system
    "${check_tool}" -tuanp | awk 'match($1,/^(tcp|udp)/)&&match($'"${state_field}"',/(LISTEN|ESTAB|UNCONN|FIN-WAIT)/){port=gensub(/.*:(.*)/,"\\1","g",$'"${port_field}"'); a[port]++}END{for(i in a) print i}' > "${port_used_list}"

    # 2 - list ports being assigned from file /etc/services, appent unique ports to temporary file
    if [[ -s '/etc/services' ]]; then
        awk 'match($1,/^[^#]/){port=gensub(/([[:digit:]]+)\/.*/,"\\1","g",$2); a[port]++}END{for(i in a) print i}' /etc/services >> "${port_used_list}"
        # sed -n -r 's@.*[[:space:]]+([0-9]+)/.*@\1@p' /etc/services
    fi
}

# - Generate Random Port Available
fn_GenerateRandomPort(){
    local l_port_used_list="$1"
    local l_port_from="$2"
    local l_port_to="$3"
    local random_num
    random_num=$(head -n $(( RANDOM%500 + 100 )) /dev/urandom | cksum)    # 1503747052 5549
    random_num=${random_num%% *}    # 1503747052
    # https://github.com/koalaman/shellcheck/wiki/Sc2004
    local l_port_generated
    l_port_generated=$((random_num%l_port_to))

    [[ $(grep -c -w "${l_port_generated}" "${l_port_used_list}") -gt 0 || "${l_port_generated}" -lt "${l_port_from}" || "${l_port_generated}" =~ 4{2,} || "${l_port_generated}" =~ 4$ ]] && l_port_generated=$(fn_GenerateRandomPort "${l_port_used_list}" "${l_port_from}" "${l_port_to}") # function iteration
    echo "${l_port_generated}"
}

# - Port Generation & Output
fn_PortGenerationAndOutput(){
    local generated_port=${generated_port:-}
    generated_port=$(fn_GenerateRandomPort "${port_used_list}" "${port_start}" "${port_end}")    # invoke function

    [[ "${json_output}" -eq 1 ]] || fnBase_NotifySendStatement 'Available TCP Port Generation' "Port number: ${generated_port}"

    if [[ "${json_output}" -eq 1 ]]; then
        local output_json
        output_json=$(fn_OutputJsonConcat '_s')    # begin
        output_json=$(fn_OutputJsonConcat 'generate_type' "${generate_type}")
        [[ "${simple_output}" -eq 1 ]] || output_json=$(fn_OutputJsonConcat 'port_range' "${port_start}-${port_end}")
        output_json=$(fn_OutputJsonConcat 'port_no' "${generated_port}")
        output_json=$(fn_OutputJsonConcat '_e')    # end
        echo "${output_json}"
    else
        if [[ "${simple_output}" -eq 1 ]]; then
            echo "${generated_port}"
        else
            printf "Newly gengeated ${c_red}%s${c_normal} port num is ${c_blue}%s${c_normal}.\n" "${generate_type}" "${generated_port}"
        fi
    fi

    [[ -f "${port_used_list}" ]] && rm -f "${port_used_list}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_AvailablePortRangeCheck
fn_UsedPortCheck
fn_PortGenerationAndOutput


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset json_output
    unset port_type
    unset simple_output
    unset port_start
    unset port_end
    unset generate_type
}

trap fn_TrapEXIT EXIT

# Script End
