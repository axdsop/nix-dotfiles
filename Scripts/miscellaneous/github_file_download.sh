#!/usr/bin/env bash

set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://github.com/
# Target: Download Single File From GitHub Via Origin URL By Bash Script On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:07 Thu ET - initialization check method update
# - Sep 27, 2019 09:49 Fri ET - change to new base functions
# - Jan 01, 2019 19:30 Tue ET - code reconfiguration, include base funcions from other file
# - Jun 09, 2017 09:44 Fri +0800
# - Dec 21, 2016 15:45 Wed +0800
# - Nov 02, 2016 12:51 Wed +0800
# - Sep 23, 2016 11:51 Fri +0800
# - Jan 12, 2016 01:26 Tue +0800



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
github_url=${github_url:-}
save_dir=${save_dir:-}
proxy_server_specify=${proxy_server_specify:-}
origin_complete_name=${origin_complete_name:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Download Single File From GitHub Via Origin URL On GNU/Linux!

[available option]
    -h    --help, show help info
    -l url    --sepcify file url on GitHub
    -d dir    --specify file save dir, default is home dir (~/)
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hl:d:p:" option "$@"; do
    case "$option" in
        l ) github_url="$OPTARG" ;;
        d ) save_dir="$OPTARG" ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi
    

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '0'
}

fn_InitializationCheck_old(){
    local l_base_update_script_url='https://gitlab.com/axdsop/nixnotes/raw/master/GNULinux/Dotfiles/_Base/base_update.sh'
    local l_download_tool=${l_download_tool:-'wget -qO-'}
    [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
    local l_script_bash_path=''
    l_script_bash_path=$(${l_download_tool} "${l_base_update_script_url}" | bash -s -- 2> /dev/null)

    if [[ -f "${l_script_bash_path}" && -s "${l_script_bash_path}" ]]; then
        . "${l_script_bash_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '0'
}


#########  2.  Logical Process Function  #########
# - check GitHub url legal or not
fn_GitHubUrlCheck(){
    if [[ -z "${github_url}" ]]; then
        fnBase_ExitStatement "${c_red}Error${c_normal}: please specify a GitHub url!"
    elif [[ ! "${github_url}" =~ ^https://github.com/* ]]; then
        fnBase_ExitStatement "${c_red}Error${c_normal}: illegal url ${c_red}${github_url}${c_normal}, please specify a legal GitHub url!"
    fi
}

# - file save dir, 1. specified dir 2. home dir
fn_SaveDirCheck(){
    if [[ -n "${save_dir}" && -d "${save_dir}" ]]; then
        [[ -w "${save_dir}" ]] || fnBase_ExitStatement "${c_red}Error${c_normal}: directory you specified ${c_red}${save_dir}${c_normal} doesn't has ${c_red}write${c_normal} permission for current user ${c_red}${login_user}${c_normal}!"
    else
        save_dir="${login_user_home}"
    fi
    save_dir="${save_dir%/}"
}

# - Extract File's Complete Name Via Origin URL
fn_ExtractFileCompleteName(){
    # echo "Begin to extract file complete origin name, just be patient!"
    origin_complete_name=$($download_method "${github_url}" | sed -r -n '/final-path/s@.*<strong.*>(.*)<.*>@\1@p')
    [[ -z "${origin_complete_name}" ]] && fnBase_ExitStatement "${c_red}Error${c_normal}: fail to get file name via specified GitHub url!"
    # printf "Name of file you wanna download is ${c_blue}%s${c_normal}!\n" "${origin_complete_name}"
}


#########  2-1.  Download Target File Via Origin URL  #########
fn_DownloadFile(){
    echo "Begin to download file ${c_red}${origin_complete_name}${c_normal}, just be patient!"

    local file_save_path="${save_dir}/${origin_complete_name}"
    if [[ -f "${file_save_path}" ]]; then
        local file_backup_path="${file_save_path}_bak"
        [[ -f "${file_backup_path}" ]] && rm -f "${file_backup_path}"
        mv "${file_save_path}" "${file_backup_path}"   #backup existing file with same name
        echo "Rename existed file ${c_red}${file_save_path}${c_normal} to ${c_red}${file_backup_path}${c_normal}, download operation continue!"
    fi

    local transformed_url=${github_url//blob\/}
    local transformed_url="${transformed_url/github.com/raw.githubusercontent.com}"

    $download_method "${transformed_url}" > "${file_save_path}"

    if [[ -f "${file_save_path}" ]]; then
        chown "${login_user}":"${login_user}" "${file_save_path}"
        printf "\nFile ${c_red}%s${c_normal} has been saved under directory ${c_red}%s${c_normal} successfully!\n" "${origin_complete_name}" "${save_dir}"

        fnBase_NotifySendStatement 'Download File From GitHub' "${save_dir}/${origin_complete_name}"

        if fnBase_CommandExistIfCheck 'stat'; then
            echo ''
            stat "${file_save_path}"
        fi
    else
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to download file ${c_red}${origin_complete_name}${c_normal}!"
    fi
}


#########  3. Executing Process  #########
fn_InitializationCheck
fn_GitHubUrlCheck
fn_SaveDirCheck
fn_ExtractFileCompleteName
fn_DownloadFile


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset github_url
    unset save_dir
    unset proxy_server_specify
    unset download_method
    unset proxy_server_specify
    unset update_operation
    unset origin_complete_name
}

trap fn_TrapEXIT EXIT

# Script End
