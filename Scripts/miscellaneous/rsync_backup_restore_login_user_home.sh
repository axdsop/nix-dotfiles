#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Backup/Restore files in login user home $HOME On GNU/Linux.
# Developer: MaxdSre

# Change Log
# - Oct 26, 2019 08:22 Sat ET - change function name format
# - Oct 17, 2019 13:05 Thu ET - fix portable drive disk detection
# - Jun 03, 2019 16:38 Mon ET - first draft


#########  0. Initial Variables Setting  #########
# IFS=$'\n\t' #used in loop,  Internal Field Separator
# umask 022    # temporarily change umask value to 022
# - Color
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
# black 0, red 1, green 2, yellow 3, blue 4, magenta 5, cyan 6, gray 7
# \e[31m need to use `echo -e`
readonly c_red='\e[31m' # $(tput setaf 1)"
readonly c_green='\e[32m'
readonly c_yellow='\e[33m'
readonly c_blue='\e[34m'
readonly c_magenta='\e[35m'
readonly c_cyan='\e[36m'
readonly c_gray='\e[37m'
readonly c_normal='\e[0m' # $(tput sgr0)


#########  1. Variables Setting  #########
readonly portable_backup_dir_name='BackupSystem'
readonly protable_drive_brand_name='seagate'
readonly action_list='backup restore'
readonly bak_suffix='_bak'
hidden_dir_list='~/.aws|~/.ssh'
bandwidth_limit=30720 # KBPS 30mb for HDD

# - Detecting login user info
fn_LoginUserInfoDetection(){
    if [[ -z $(rsync --version 2> /dev/null | sed -r -n '/version/{p}') ]]; then
        echo -e "${c_red}Sorry${c_normal}, this script needs utility ${c_yellow}rsync${c_normal}."
        exit
    fi

    [[ -n "${USER:-}" && -z "${SUDO_USER:-}" ]] && login_user="${USER:-}" || login_user="${SUDO_USER:-}"
    [[ -z "${login_user}" ]] && login_user=$(logname 2> /dev/null)
    # [[ -z "${login_user}" ]] && login_user=$(who 2> /dev/null | cut -d' ' -f 1)
    # not work properly while using sudo
    [[ -z "${login_user}" ]] && login_user=$(id -u -n 2> /dev/null)
    [[ -z "${login_user}" ]] && login_user=$(ps -eo 'uname,comm,cmd' | sed -r -n '/xinit[[:space:]]+\/(root|home)\//{s@^([^[:space:]]+).*@\1@g;p;q}')

    if [[ "${login_user}" != 'root' && "$UID" -eq 0 ]]; then
        echo -e "${c_red}Sorry${c_normal}, this script doesn't require superuser privileges (eg. root, su)."
        exit
    else
        [[ "${login_user}" == 'root' ]] && login_user_home='/root' || login_user_home="/home/${login_user}"
        echo -e "Login user is ${c_yellow}${login_user}${c_normal}, user home is ${c_yellow}${login_user_home}${c_normal}."
    fi
}

# - Detecting disk drive type (HDD/SSD) located /home or /
fn_DiskDriveTypeDetection(){
    # Hard Disk Drive (HDD) | Solid State Drive (SSD)
    # check file /sys/block/sda/queue/rotational  If the above output is 0 (zero), then the disk is SSD (because SSD does not rotate). You should see output '1' on machines that has HDD disk.

    # find block device located /home, if not find /
    local rotational_val=1
    local disk_type='HDD'
    block_path=$(mount 2> /dev/null | sed -r -n '/\/home[[:space:]]+/{s@^[[:space:]]*([^[:space:]]+).*$@\1@g;p}')  # /home mount point  /dev/nvme0n1pX  /dev/sdaX  X is digit number
    [[ -z "${block_path}" ]] && block_path=$(mount 2> /dev/null | sed -r -n '/\/[[:space:]]+/{s@^[[:space:]]*([^[:space:]]+).*$@\1@g;p}')  # / mount point

    # /sys/block/nvme0n1 / /sys/block/sda
    find /sys/block/ -type l -not -name 'loop*' -print | while read -r line; do
        block_name="${line##*/}"
        match_pattern="^${block_name}"
        if [[ "${block_path##*/}" =~ $match_pattern ]]; then
            rotational_val=$(cat "/sys/block/${block_name}/queue/rotational")
            [[ "${rotational_val}" -eq 0 ]] && disk_type='SSD'
            echo -e "\nUser home ${c_yellow}/home${c_normal} is located in ${c_yellow}${block_path}${c_normal}, disk type ${c_red}${disk_type}${c_normal}."
            break
        fi
    done

    [[ "${rotational_val}" -eq 0 ]] && bandwidth_limit=256000 # KBPS 250mb for SSD
}


# - Detect portable drive disk
fn_PortableDriveDiskDetection(){
    portable_backup_full_path=''
    portable_mount_path=''
    portable_mount_path=$(lsblk -f 2> /dev/null | sed -r -n '/'"${protable_drive_brand_name,,}"'/I{s@[^\/]+(.*)$@\1@g;p}')

    if [[ -z "${portable_mount_path}" ]]; then
        local l_block_dev_name='' # /dev/sdb1
        l_block_dev_name=$(sudo fdisk -l 2> /dev/null | sed -r -n '/NTFS/{s@^([^[:space:]]+).*$@\1@g;p}')
        [[ -n "${l_block_dev_name}" ]] && portable_mount_path=$(lsblk  2> /dev/null | sed -r -n '/'${l_block_dev_name##*/}'/{s@[^\/]+(.*)$@\1@g;p}')
    fi

    if [[ -n "${portable_mount_path}" ]]; then
        if [[ "${portable_mount_path}" =~ ^/$ ]]; then
            echo -e "${c_red}Sorry${c_normal}, root path '/' is not proper portable drive."
            exit
        else
            if [[ -d "${portable_mount_path}" ]]; then
                echo -e "\n${c_yellow}Available operation type${c_normal}:\n- ${c_cyan}Backup${c_normal}: backup files from system to portable drive;\n- ${c_cyan}Restore${c_normal}: restore from portable drive to system;\n"
                echo -e "Detected portable drive path: ${c_yellow}${portable_mount_path}${c_normal}"
                portable_backup_full_path="${portable_mount_path}/${portable_backup_dir_name}"
                echo -e "Portabl drive backup dir path: ${c_yellow}${portable_backup_full_path}${c_normal}"
            else
                echo -e "${c_red}Sorry${c_normal}, ${c_yellow}${portable_mount_path}${c_normal} is not a directory."
                exit
            fi
        fi
    else
        echo -e "\n${c_red}Sorry${c_normal}, no mounted protable drive detected."
        exit
    fi
}

# - Action selection menu
fn_ActionSelectionMenu(){
    action_select=''
    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS=" " # Setting temporary IFS

    echo -e '\nPlease select action type:'
    PS3="Choose action type number(e.g. 1, 2,...):"

    select item in ${action_list}; do
        action_select="${item}"
        [[ -n "${action_select}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3
    echo -e "\nAction type you choose is ${c_red}${action_select^^}${c_normal}.\n"
}


# - Core Operation
fn_RsyncOperation(){
    # rsync -acvH --delete SRC DST
    # Using rsync to back up your Linux system  https://opensource.com/article/17/1/rsync-backup-linux
    # How to use advanced rsync for large Linux backups  https://opensource.com/article/19/5/advanced-rsync
    local l_src="${1:-}"
    local l_dst="${2:-}"

    if [[ -d "${l_src}" && -n "${l_dst}" ]]; then
        [[ -d "${l_dst}" ]] || mkdir -p "${l_dst}"
        # just backup unempty dirs
        find "${l_src}" -maxdepth 1 -type d -not -path "${l_src}/Downloads" -not -path '*\/.*' -not -empty -not -path "${l_src}" -exec rsync --bwlimit="${bandwidth_limit}" -acv --delete {} "${l_dst}" \;
    fi
}

fn_PermissionOperation(){
    local l_path="${1:-}"

    chown -R "${login_user}" "${l_path}"
    chgrp -R "${login_user}" "${l_path}" 2> /dev/null

    if [[ -d "${l_path}" ]]; then
        find "${l_path}" -type d -exec chmod 750 {} \;
        find "${l_path}" -type f -exec chmod 640 {} \;
    else
        chmod 640 "${l_path}"
    fi
}

fn_MainOperation(){
    case "${action_select,,}" in
        backup )
            fn_RsyncOperation "${login_user_home}" "${portable_backup_full_path}"

            find "${login_user_home}" -maxdepth 1 -type d \( -name '.ssh' -o -name '.aws' \) -not -empty -print | while read -r line; do
                src_name="${line##*/}"
                # .ssh / .aws ==> _ssh / _aws
                [[ "${src_name}" =~ ^. ]] && src_name="${src_name/./_}"
                cp -R "${line}" "${portable_backup_full_path}/${src_name}"
            done
            ;;
        restore )
            find "${login_user_home}" -maxdepth 1 -type d -not -path '*\/.*' -not -path "${login_user_home}" -print | while read -r line; do
                backup_dir_path="${portable_backup_full_path}/${line##*/}"
                if [[ -d "${backup_dir_path}" ]]; then
                    rsync --bwlimit="${bandwidth_limit}" -acv --delete "${backup_dir_path}" "${login_user_home}"
                    fn_PermissionOperation "${line}"
                fi
            done

            # .ssh / .aws
            echo "${hidden_dir_list}" | sed -r -n 's@\|@\n@g;p' | while read -r line; do
                if [[ -n "${line}" ]]; then
                    dst_name=".${line##*.}"
                    src_name="_${line##*.}"
                    src_portable_name="${portable_backup_full_path}/${src_name}"
                    if [[ -d "${src_portable_name}" ]]; then
                        dst_dir_path="${login_user_home}/${dst_name}"
                        [[ -d "${dst_dir_path}${bak_suffix}" ]] && rm -rf "${dst_dir_path}${bak_suffix}"
                        [[ -d "${dst_dir_path}" ]] && mv "${dst_dir_path}" "${dst_dir_path}${bak_suffix}"
                        cp -R "${src_portable_name}" "${dst_dir_path}"
                        fn_PermissionOperation "${dst_dir_path}"

                        find "${dst_dir_path}" -type f -name '*.pub' -print | sed -n 's@.pub$@@g;p' | while read -r priv_key; do
                            chmod 400 "${priv_key}"
                        done
                        if [[ "${dst_name}" =~ aws$ ]]; then
                            [[ -f "${dst_dir_path}/credentials" ]] && chmod 600 "${dst_dir_path}/credentials"
                        fi
                    fi
                fi
            done
            ;;
    esac
}


# - Operation Procedure
fn_LoginUserInfoDetection
fn_DiskDriveTypeDetection
fn_PortableDriveDiskDetection
fn_ActionSelectionMenu
fn_MainOperation

# Script End
