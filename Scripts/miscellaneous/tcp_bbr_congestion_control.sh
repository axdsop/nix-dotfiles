#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails
# IFS=$'\n\t' #used in loop,  Internal Field Separator

# Official Site:
# https://cloudplatform.googleblog.com/2017/07/TCP-BBR-congestion-control-comes-to-GCP-your-Internet-just-got-faster.html
# https://tools.ietf.org/html/rfc2581

# Target: TCP Congestion Control Method Change On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:15 Thu ET - initialization check method update
# - Sep 24, 2019 16:35 Tue ET - change to new base functions, add support for Arch Linux
# - Jan 01, 2019 20:16 Tue ET - code reconfiguration, include base funcions from other file


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
method_choose=${method_choose:-'bbr'}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

TCP Congestion Control Method Change On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -m method    --choose congestion control method (bbr|cubic|hybla|), default is 'bbr', if kernel version < 4.9, then default choose 'cubic'
    -h    --help, show help info
\e[0m"
}

while getopts "hm:" option "$@"; do
    case "$option" in
        m ) method_choose="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'sed'
    # fnBase_CommandExistCheckPhase 'gawk'
}

fn_GetOptsVariableConfiguration(){
    # Method Choose
    case "${method_choose,,}" in
        b|bbr ) method_choose='bbr' ;;
        h|hybla ) method_choose='hybla' ;;
        c|cubic|* ) method_choose='cubic' ;;
    esac

    if [[ "${method_choose}" == 'bbr' ]]; then
        local l_kernel_version=${l_kernel_version:-}
        l_kernel_version=$(uname -r | sed -r -n 's@^([[:digit:]]+.[[:digit:]]+)..*$@\1@g;p')
        local l_kernel_major=${l_kernel_version%%.*}
        local l_kernel_minor=${l_kernel_version##*.}

        if [[ "${l_kernel_major}" -gt 4 ]]; then
            method_choose='bbr'
        elif [[ "${l_kernel_major}" -eq 4 && "${l_kernel_minor}" -ge 9 ]]; then
            method_choose='bbr'
        else
            method_choose='cubic'
        fi
    fi

    if [[ "${method_choose}" == 'bbr' ]]; then
        local l_required_module=${l_required_module:-}
        l_required_module=$(sed -r -n '/(CONFIG_TCP_CONG_BBR|CONFIG_NET_SCH_FQ)/p' /boot/config-"$(uname -r)" 2> /dev/null)

        if [[ -n "${l_required_module}" || -n $(modinfo -n tcp_bbr 2> /dev/null) ]]; then
            # printf "${c_bold}${c_blue}%s${c_normal}\n" 'Required Linux Kernel Module Check'
            # echo -e "${l_required_module}\n"
            method_choose='bbr'
        else
            method_choose='cubic'
        fi
    fi
}

#########  2. Operation Procedure  #########
# egrep 'CONFIG_TCP_CONG_BBR|CONFIG_NET_SCH_FQ' /boot/config-$(uname -r)
# /etc/sysctl.conf
# net.core.default_qdisc=fq
# net.ipv4.tcp_congestion_control=bbr

# tcp_congestion_control: for high-latency network, use 'hybla', for low-latency network, use 'cubic' instead. If kernel version >= 4.9, use 'bbr'

fn_SysctlDirectiveSetting(){
    local l_path=${1:-} # /etc/sysctl.conf /etc/sysctl.d/*.conf
    local l_item=${2:-}
    local l_val=${3:-}
    local l_action=${4:-}

    if [[ -f "${l_path}" && -n "${l_item}" && -f "/proc/sys/${l_item//./\/}" && -n "${l_val}" ]]; then
        if [[ -n $(sed -r -n '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{p}' "${l_path}") ]]; then
            case "${l_action,,}" in
                d|delete )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/d' "${l_path}" 2> /dev/null
                    ;;
                c|comment )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*(.*)$@# \1@g}' "${l_path}" 2> /dev/null
                    ;;
                * )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*([^=]+=[[:space:]]*).*@\1'"${l_val}"'@g}' "${l_path}" 2> /dev/null
                    ;;
            esac
        else
            [[ -z "${l_action}" ]] && sed -r -i '$a '"${l_item}"' = '"${l_val}"'' "${l_path}" 2> /dev/null
        fi
    fi
}

fn_OperationProcedure(){
    local l_sysctl_path=${l_sysctl_path:-'/etc/sysctl.conf'}

    # Arch Linux just has directory /etc/sysctl.d/
    # https://wiki.archlinux.org/index.php/Sysctl#Enable_BBR
    # https://bbs.archlinux.org/viewtopic.php?id=223879#p1863975
    if [[ ! -f "${l_sysctl_path}" && -d /etc/sysctl.d/ ]]; then
        l_sysctl_path='/etc/sysctl.d/bbr.conf'
        [[ -f "${l_sysctl_path}" ]] || echo '# TCP Congestion Contrel' > "${l_sysctl_path}"
        # Load the BBR kernel module.
        [[ -d /etc/modules-load.d/ ]] && echo "tcp_bbr" > /etc/modules-load.d/bbr.conf

       # Set the default congestion algorithm to BBR.
       # echo "net.core.default_qdisc=fq" > /etc/sysctl.d/bbr.conf
       # echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.d/bbr.conf
    fi

    local l_tcp_congestion_control_directive=${l_tcp_congestion_control_directive:-'net.ipv4.tcp_congestion_control'}
    # /proc/sys/net/ipv4/tcp_congestion_control
    fn_SysctlDirectiveSetting "${l_sysctl_path}" "${l_tcp_congestion_control_directive}" "${method_choose}"

    if [[ $(cat "/proc/sys/${l_tcp_congestion_control_directive//./\/}" 2> /dev/null) == 'bbr' ]]; then
        fn_SysctlDirectiveSetting "${l_sysctl_path}" 'net.core.default_qdisc' 'fq'
    else
        fn_SysctlDirectiveSetting "${l_sysctl_path}" 'net.core.default_qdisc' 'fq' 'c'
    fi

    fnBase_CommandExistIfCheck 'sysctl' && sysctl --system &> /dev/null

    echo "Configuration File ${c_yellow}${l_sysctl_path}${c_normal}."
    printf "\n${c_bold}${c_blue}%s${c_normal}\ntcp_congestion_control = ${c_red}%s${c_normal}\n" 'Current TCP Congestion Control Method' "${method_choose}"
}



#########  3. Executing Process  #########
fn_InitializationCheck
fn_GetOptsVariableConfiguration
fn_OperationProcedure


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset method_choose
}

trap fn_TrapEXIT EXIT

# Script End
