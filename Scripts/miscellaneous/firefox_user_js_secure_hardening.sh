#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Generating custom user profile for Mozilla Firefox and its variants On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Nov 02, 2019 21:45 Sat ET - add custom user profile generation, add user.js to harden firefox from GitHub
# - Mar 29, 2020 09:21 Sun ET - code reconfiguratio
# - Jul 28, 2020 08:06 Tue ET - add WebRender enable/disable in custom user profile
# - Dec 08, 2020 09:31 Tue ET - disable dynamically resizes the inner window by applying margins ('privacy.resistFingerprinting.letterboxing'), in i3 environment it will show margins in inner window.


# 1. check firefox is installed
# 2. check config directory ~/.mozilla/firefox is existed
# 2.1 generate new
# 3. install/update harden js


#########  0-1. Variables Setting  #########
firefox_bin_path=${firefox_bin_path:-}
user_name_specify=${user_name_specify:-}
user_name_specify_home_path=${user_name_specify_home_path:-}
silent_output=${silent_output:-0}
prompt_action=${prompt_action:-0}
base_function_path=${base_function_path:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Generating Custom User Profile To Harden Mozilla Firefox Web Browser On GNU/Linux!

[available option]
    -h    --help, show help info
    -b firefox_bin_path    --specify firefox bin dir path if path not in \$PATH
    -u user_name_specify    --specify user name listed in /etc/passwd, default is current normal login user '$USER'
    -s    --silent output, default output operation info
    -p    --enable prompt info while using base functions, default is diable (please don't use parameter)
    -f base_function_path    --specify bash function path, along with parameter '-p'
\e[0m"
}

while getopts "hb:u:spf:" option "$@"; do
    case "$option" in
        b ) firefox_bin_path="$OPTARG" ;;
        u ) user_name_specify="$OPTARG" ;;
        s ) silent_output=1 ;;
        p ) prompt_action=1 ;;
        f ) base_function_path="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Custom Functions  #########

fn_ExitStatement(){
    local l_str="$*"
    [[ -n "${l_str}" && "${silent_output}" -eq 0 ]] && echo -e "${l_str}\n"
    exit
}

fn_OutputStatemnet(){
    local l_str="$*"
    [[ "${silent_output}" -ne 0 ]] || echo "${l_str}"
}

fn_UserJSConfiguration(){
    local l_path="${1:-}"
    local l_key="${2:-}"
    local l_val="${3:-}"
    if [[ -f "${l_path}" && -n "${l_key}" ]]; then
        if [[ -n $(sed -r -n '/user_pref.*'"${l_key}"'"/{p}' "${l_path}" 2> /dev/null) ]]; then
            if [[ -n "${l_val}" ]]; then
                # sed -r -i '/^user_pref.*'"${l_key}"'/{s@(.*?",[[:space:]]+)[^\)]+(.*)$@\1'"${l_val}"'\2@g;}' "${l_path}" 2> /dev/null
                sed -r -i '/^[[:space:]]*\/*[[:space:]]*user_pref.*'"${l_key}"'"/{s@^[[:space:]]*\/*[[:space:]]*(.*?",[[:space:]]+)[^\)]+(.*)$@\1'"${l_val}"'\2@g;}' "${l_path}" 2> /dev/null
            else
                sed -r -i '/^[[:space:]]*\/*[[:space:]]*user_pref.*'"${l_key}"'"/{s@[[:space:]]*\/*[[:space:]]*(.*?",[[:space:]]+)[^\)]+(.*)$@\1""\2@g;}' "${l_path}" 2> /dev/null
            fi   
        else
            sed -r -i '$a user_pref("'"${l_key}"'", '"${l_val}"');' "${l_path}" 2> /dev/null # append
        fi


        # old method
        # if [[ -n "${l_val}" ]]; then
        #     # sed -r -i '/^user_pref.*'"${l_key}"'/{s@(.*?",[[:space:]]+)[^\)]+(.*)$@\1'"${l_val}"'\2@g;}' "${l_path}" 2> /dev/null
        #     sed -r -i '/^[[:space:]]*\/*[[:space:]]*user_pref.*'"${l_key}"'/{s@^[[:space:]]*\/*[[:space:]]*(.*?",[[:space:]]+)[^\)]+(.*)$@\1'"${l_val}"'\2@g;}' "${l_path}" 2> /dev/null
        # else
        #     sed -r -i '/^[[:space:]]*\/*[[:space:]]*user_pref.*'"${l_key}"'/{s@[[:space:]]*\/*[[:space:]]*(.*?",[[:space:]]+)[^\)]+(.*)$@\1""\2@g;}' "${l_path}" 2> /dev/null
        # fi
    fi
}



#########  2-1 Variables Initialization  #########
fn_Variables_Initialization(){
    [[ "$UID" -eq 0 && "${prompt_action}" -eq 0 ]] && fn_ExitStatement "Sorry: this script doesn't require superuser privileges (eg. root, su)."

    [[ "${prompt_action}" -ne 0 && -n "${base_function_path}" && -f "${base_function_path}" ]] && . "${base_function_path}"

    # - Firefox existed check
    local l_bin_path=${1:-"${firefox_bin_path}"}

    if [[ -n "${l_bin_path}" ]]; then
        [[ -d "${l_bin_path}" ]] && l_bin_path="${l_bin_path%/}/firefox"
    else
        l_bin_path=$(which firefox 2> /dev/null || command -v firefox 2> /dev/null)

        if [[ ! -n "${l_bin_path}" ]]; then
            l_bin_path='/opt/MozillaFirefox/'
            [[ -d "${l_bin_path}" ]] && l_bin_path="${l_bin_path%/}/firefox"
        fi
    fi

    [[ -f "${l_bin_path}" ]] || fn_ExitStatement "Fail to find Firefox bin in ${l_bin_path}"
    
    if [[ -f "${l_bin_path}" && -n $($l_bin_path --version 2> /dev/null) ]]; then
        fn_OutputStatemnet "Detected Firefox bin path: ${l_bin_path}"
    else
        fn_ExitStatement "Fail to detect Firefox bin path."
    fi

    firefox_bin_path="${l_bin_path}"

    # - User name specify check
    if [[ -n "${user_name_specify}" ]]; then
        [[ -n $(sed -r -n '/^'"${user_name_specify}"':/{p}' /etc/passwd 2> /dev/null) ]] || fn_ExitStatement "Fail to find name ${user_name_specify} in /etc/passwd"
        login_normal_user="${user_name_specify}"
    else
        # $EUID is 0 if run as root, default is user id, use command `who` to detect login user name
        # $USER exist && $SUDO_USER not exist, then use $USER
        [[ -n "${USER:-}" && -z "${SUDO_USER:-}" ]] && login_normal_user="${USER:-}" || login_normal_user="${SUDO_USER:-}"
        [[ -z "${login_normal_user}" ]] && login_normal_user=$(logname 2> /dev/null)
        # [[ -z "${login_normal_user}" ]] && login_normal_user=$(who 2> /dev/null | cut -d' ' -f 1)
        # not work properly while using sudo
        [[ -z "${login_normal_user}" ]] && login_normal_user=$(id -u -n 2> /dev/null)
        [[ -z "${login_normal_user}" ]] && login_normal_user=$(ps -eo 'uname,comm,cmd' | sed -r -n '/xinit[[:space:]]+\/(root|home)\//{s@^([^[:space:]]+).*@\1@g;p;q}')
    
        user_name_specify="${login_normal_user}"
    fi

    # awk -F: 'match($1,/^'"${user_name_specify}"'$/){print $6}' /etc/passwd
    # sed -n '/^'"${user_name_specify}"':/{p}' /etc/passwd | cut -d: -f6
    [[ "${user_name_specify}" == 'root' ]] && user_name_specify_home_path='/root' || user_name_specify_home_path="/home/${user_name_specify}"
    user_name_specify_home_path="${user_name_specify_home_path%/}"

    fn_OutputStatemnet "User name specify: ${user_name_specify}"
}


#########  2-2 User Profiles Configuration  #########
fn_UserProfileConfiguration(){
    local l_config_dir=${l_config_dir:-"${user_name_specify_home_path}/.mozilla/firefox"}
    local l_profiles_ini=${l_profiles_ini:-"${l_config_dir}/profiles.ini"}
    local l_profile_name=${l_profile_name:-"${user_name_specify}"}

    # for freshly installed firefox
    if [[ ! -f "${l_profiles_ini}" ]]; then
        [[ "${prompt_action}" -eq 0 ]] || fnBase_OperationProcedureStatement 'User profile generating'
        # ~/.mozilla/firefox/installs.ini  ~/.mozilla/firefox/profiles.ini
        # - generate custom profile (name same to login user name) create file profiles.ini
        # https://developer.mozilla.org/en-US/docs/Mozilla/Command_Line_Options#-CreateProfile_profile_name
        # sudo -u "${user_name_specify}" "${firefox_bin_path}" -CreateProfile "${l_profile_name}" 2> /dev/null

        if [[ "${user_name_specify}" == "${login_normal_user}" ]]; then
            "${firefox_bin_path}" -CreateProfile "${l_profile_name}"
        else
            # Sorry, user flying is not allowed to execute '/usr/bin/firefox -CreateProfile flying' as flying on BMXP.
            sudo -u "${user_name_specify}" "${firefox_bin_path}" -CreateProfile "${l_profile_name}"
        fi
        
        # - find custom profile directory
        local l_user_profile_dir=''
        l_user_profile_dir=$(find "${l_config_dir}"  -type d -name "*${l_profile_name}" -print 2> /dev/null)

        if [[ -d "${l_user_profile_dir}" ]]; then
            # - generate default profile created by web browser, create file installs.ini
            fn_OutputStatemnet "Generating profile installs.ini"

            if [[ "${user_name_specify}" == "${login_normal_user}" ]]; then
                nohup=$(nohup "${firefox_bin_path}" --headless >/dev/null 2>&1 &)
            else
                # Sorry, user flying is not allowed to execute '/usr/bin/firefox --headless' as flying on BMXP.
                nohup=$(nohup sudo -u "${user_name_specify}" "${firefox_bin_path}" --headless >/dev/null 2>&1 &)
            fi

            while true; do
                nohup_pid=$(sudo ps -e -o pid,args 2> /dev/null | sed -r -n '/--headless/{/sed/d;s@^[[:space:]]*([[:digit:]]+).*$@\1@g;p;q}')
                if [[ -n "${nohup_pid}" ]]; then
                    sleep 1
                    sudo kill -9 "${nohup_pid}" 2> /dev/null
                else
                    break
                fi
            done
        fi

        # local l_installs_uuid=''
        # local l_installs_ini="${l_user_profile_dir%/*}/installs.ini"
        # [[ -f "${l_installs_ini}" ]] && l_installs_uuid=$(sed -r -n '/^\[/{s@[[:punct:]]*@@g;p}' "${l_installs_ini}") # A518266653472A7A

        # - update default profile setting in file profiles.ini
        sed -r -i '/\[Install/,/\[/{/Default=/{s@^([^=]+=).*@\1'"${l_user_profile_dir##*/}"'@g;}}' "${l_profiles_ini}" 2> /dev/null
        [[ "${prompt_action}" -eq 0 ]] || fnBase_OperationProcedureResult "${l_user_profile_dir}/"
    fi


    # - add custom user.js to hardening firefox
    # l_profiles_ini=$(find "${l_config_dir}" -type f -name 'profiles.ini' -print 2> /dev/null)
    if [[ -f "${l_profiles_ini}" ]]; then
        local l_default_profile_name
        l_default_profile_name=$(sed -r -n '/\[Install/,/\[/{/Default=/{s@^[^=]+=@@g;p}}' "${l_profiles_ini}")

        l_default_user_profile_dir=$(find "${l_config_dir}"  -type d -name "*${l_default_profile_name}" -print 2> /dev/null)

        if [[ -d "${l_default_user_profile_dir}" ]]; then
            [[ "${prompt_action}" -eq 0 ]] || fnBase_OperationProcedureStatement 'user.js updating'
            # - download method
            download_tool='wget -qO-'
            [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_tool='curl -fsL'

            # https://github.com/pyllyukko/user.js/
            # https://github.com/ghacksuserjs/ghacks-user.js --> https://github.com/arkenfox/user.js
            # https://ownyourbits.com/2018/09/08/customize-firefox-for-privacy-and-security-with-a-custom-user-js/
            # https://askubuntu.com/questions/313483/how-do-i-change-firefoxs-aboutconfig-from-a-shell-script
            local l_user_js_path="${l_default_user_profile_dir}/user.js"
            local l_action_type=${l_action_type:-'install'}
            [[ -f "${l_user_js_path}" ]] && l_action_type='update'
            $download_tool https://raw.githubusercontent.com/arkenfox/user.js/master/user.js > "${l_user_js_path}"
            
            if [[ -f "${l_user_js_path}" ]]; then
                chown "${user_name_specify}" "${l_user_js_path}"
                chgrp "${user_name_specify}" "${l_user_js_path}" 2> /dev/null
                chmod 640 "${l_user_js_path}"
            fi

            # enable location bar using search
            fn_UserJSConfiguration "${l_user_js_path}" 'keyword.enabled' 'true'

            # DNS-over-HTTPS (DoH)
            # https://support.mozilla.org/en-US/kb/firefox-dns-over-https
            # https://www.zdnet.com/article/how-to-enable-dns-over-https-doh-in-firefox/

            # DoH publicly available servers
            # https://github.com/curl/curl/wiki/DNS-over-HTTPS
            # https://kb.adguard.com/en/general/dns-providers

            # DoH removed https://github.com/ghacksuserjs/ghacks-user.js/commit/16756646bbd803e772d9796a20d400d2e2ee18c7
            # // user_pref("network.trr.mode", 0);
            # // user_pref("network.trr.bootstrapAddress", "");
            # // user_pref("network.trr.uri", "");
            # network.trr.resolvers  value  [{ "name": "Cloudflare", "url": "https://mozilla.cloudflare-dns.com/dns-query" },{ "name": "NextDNS", "url": "https://firefox.dns.nextdns.io/" }]

            # network.trr.mode
            # 0 - Default value in standard Firefox installations (currently is 5, which means DoH is disabled)
            # 1 - DoH is enabled, but Firefox picks if it uses DoH or regular DNS based on which returns faster query responses
            # 2 - DoH is enabled, and regular DNS works as a backup
            # 3 - DoH is enabled, and regular DNS is disabled
            # 5 - DoH is disabled

            # fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.mode' '5'

            fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.mode' '2'
            fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.uri' '"https://dns9.quad9.net/dns-query"'  # also set network.trr.custom_uri
            fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.bootstrapAddress' '"9.9.9.9"'

            # enable MPRIS support
            # https://superuser.com/questions/1350908/use-mpris-dbus-media-commands-within-firefox-on-linux#1549344
            # https://superuser.com/questions/948192/use-media-keys-for-soundcloud-youtube-etc-in-firefox/1547822#1547822
            # https://work.lisk.in/2020/05/06/linux-media-control.html
            fn_UserJSConfiguration "${l_user_js_path}" 'media.hardwaremediakeys.enabled' 'true'
            fn_UserJSConfiguration "${l_user_js_path}" 'dom.media.mediasession.enabled' 'true'
            fn_UserJSConfiguration "${l_user_js_path}" 'media.mediacontrol.stopcontrol.timer.ms' '86400000'  # ddefault is 60000 (60s)

            # enable/disable WebRender
            # https://www.omgubuntu.co.uk/2020/07/firefox-enable-webrender-linux
            # https://wiki.mozilla.org/Platform/GFX/Quantum_Render
            fn_UserJSConfiguration "${l_user_js_path}" 'gfx.webrender.all' 'false'

            # disable dynamically resizes the inner window by applying margins. In i3 environment, it will show margins in inner window.
            fn_UserJSConfiguration "${l_user_js_path}" 'privacy.resistFingerprinting.letterboxing' 'false'

            fn_OutputStatemnet "${l_action_type^^} user js: ${l_user_js_path}"
            [[ "${prompt_action}" -eq 0 ]] || fnBase_OperationProcedureResult "${l_user_js_path}"
        fi
    fi
}


#########  3. Executing Process  #########
fn_Variables_Initialization
fn_UserProfileConfiguration


# Script End