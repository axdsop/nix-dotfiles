# USB Auto Mount On GNU/Linux

To auto-mount [USB storage devices](https://wiki.archlinux.org/index.php/USB_storage_devices), one solution is via [Udisks](https://wiki.archlinux.org/index.php/Udisks), but it need to install a lot of dependency packages (16+1).

Another solution is via [udev](https://wiki.archlinux.org/index.php/Udev#Udisks).

>udev is a userspace system that enables the operating system administrator to register userspace handlers for events.

Arch Linux wiki [Mounting drives in rules](https://wiki.archlinux.org/index.php/Udev#Mounting_drives_in_rules) listed project [udev-media-automount][udev-media-automount].


## TOC

1. [Features](#features)  
2. [Prerequisites](#prerequisites)  
3. [Deployment](#deployment)  
3.1 [Download (Optional)](#download-optional)  
3.2 [Install](#install)  
3.3 [Uninstall](#uninstall)  
4. [Change Log](#change-log)  


## Features

This project is based on project [udev-media-automount][udev-media-automount], I rewrite and add some extra functions:

* add notify function via notification daemon [Dunst][dunst];
* add menu choose via [Rofi][rofi];
* integrate in status bar [Polybar][polybar];
* ~~unmount script also supports remount operation~~;
* merge (auto)mount/unmount/remount into one script;

Directory [archive](./archive) listed old scripts which (auto)mount operation and unmount/remount operaton are saved in two seperate scrips.


## Prerequisites

To make these scripts work properly, you may need to install some dependencies packages.

For Arch Linux

```bash
# - Essential
sudo pacman -S --noconfirm rofi
# https://aur.archlinux.org/packages/nerd-fonts-source-code-pro/
# yay -S --noconfirm nerd-fonts-source-code-pro
sudo pacman -S --noconfirm ttf-sourcecodepro-nerd

# Dunst (https://dunst-project.org/) is a lightweight replacement for the notification-daemons provided by most desktop environments.
sudo pacman -R --noconfirm dunst

# - Optional
# Icon theme dir /usr/share/icons/
# https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
sudo pacman -S papirus-icon-theme

# https://wiki.archlinux.org/index.php/Xdg-utils#xdg-open
pacman -S --noconfirm xdg-utils
```


## Deployment

### Download (Optional)

```bash
# download usb-automount, default save path /tmp/usb-automount

fn_Script_Download(){
  local l_save_dir=${1:-'/tmp'}
  tmp_download_dir="$(mktemp -d -t XXXXXXXX)"
  cd "${tmp_download_dir}"

  # https://en.terminalroot.com.br/how-to-clone-only-a-subdirectory-with-git-or-svn/
  git_subdir_string='Scripts/usb-automount/usb-automount'
  git init
  git remote add -f origin https://gitlab.com/axdsop/nix-dotfiles/
  git config core.sparseCheckout true
  echo "${git_subdir_string}" >> .git/info/sparse-checkout
  git pull origin master

  [[ -d "${l_save_dir}" ]] || mkdir -p "${l_save_dir}"
  cp -R "${git_subdir_string}" "${l_save_dir}"
  cd "${l_save_dir}/${git_subdir_string##*/}"
  [[ -d "${tmp_download_dir}" ]] && rm -rf "${tmp_download_dir}"
  ls -lh
}

fn_Script_Download  # /tmp/usb-automount
# fn_Script_Download "$HOME/Documents" # $HOME/Documents/usb-automount
```

### Install

```bash
sudo install -m644 usb-automount.rules /usr/lib/udev/rules.d/99-usb-automount.rules
#　https://www.freedesktop.org/software/systemd/man/systemd.unit.html
sudo install -m644 usb-automount@.service /usr/lib/systemd/system/usb-automount@.service
sudo install -m755 usb-automount /usr/bin/usb-automount
```

### Uninstall

```bash
sudo rm -f /usr/bin/usb-automount
sudo rm -f /usr/bin/usb-unmount # if existed
sudo rm -f /usr/lib/udev/rules.d/99-usb-automount.rules
sudo rm -rf /usr/lib/systemd/system/usb-automount@.service
sudo systemctl daemon-reload
```


## Bibliography

* [Automount USB devices on Linux using UDEV and Systemd](https://www.andreafortuna.org/2019/06/26/automount-usb-devices-on-linux-using-udev-and-systemd/)


## Change Log

* May 17, 2020 Sun 22:00 ET
  * first draft
* May 18, 2020 Mon 08:55 ET
  * unmount script add remount operation
* May 18, 2020 Mon 17:15 ET
  * merge (auto)mount/unmount/remount operation into one script
* Sep 27, 2020 Sun 09:10 ET
  * migrate to project **nix-dotfiles**


[dunst]:https://dunst-project.org/ "Dunst is a lightweight replacement for the notification daemons provided by most desktop environments."
[polybar]:https://polybar.github.io/ "Polybar - A fast and easy-to-use tool for creating status bars"
[rofi]: https://github.com/davatorium/rofi "Rofi: A window switcher, application launcher and dmenu replacement"
[udev-media-automount]:https://github.com/Ferk/udev-media-automount

<!-- End -->
